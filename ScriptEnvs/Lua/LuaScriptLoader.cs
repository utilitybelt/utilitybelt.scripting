﻿using WattleScript.Interpreter.Loaders;
using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.ScriptEnvs.Lua {
    public class LuaScriptLoader : ScriptLoaderBase {
        private readonly UBScript _script;

        public LuaScriptLoader(UBScript script) : base() {
            _script = script;
        }

        private string GetFileName(string file) {
            var path = Path.GetFullPath(Path.Combine(_script.Directory, file));
            //UBService.UBService.WriteLog($"Full File Path: {path}");

            return path;
        }

        public override bool ScriptFileExists(string name) {
            return File.Exists(GetFileName(name));
        }

        public override object LoadFile(string file, Table globalContext) {
            return File.OpenRead(GetFileName(file));
        }
    }
}
