﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UtilityBelt.Common.Messages.Types;

namespace UtilityBelt.Scripting.ScriptEnvs.Lua {
    public static class LuaObjectExtensions {
        /// <summary>
        /// Returns a list of strings of all public property / field names. Useful for reflection on UserData in lua scripts
        /// </summary>
        /// <param name="source">object instance to inspect</param>
        /// <returns></returns>
        public static IList<string> GetPropertyKeys(this object source) {
            try {
                if (source == null || source.GetType().IsEnum || source.GetType().IsPrimitive || source is DateTime || source.GetType().GetGenericArguments().Length > 0)
                    return new List<string>();

                var propKeys = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(p => p.Name).ToList();
                propKeys.AddRange(source.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).Select(f => f.Name));
                var keys = propKeys.Where(k => !string.IsNullOrEmpty(k)).ToList();


                //UBService.UBService.WriteLog($"GetPropertyKeys: {source} // {string.Join(", ", keys.ToArray())}");
                return keys;

            }
            catch {
                return new List<string>();
            }
        }
    }
}
