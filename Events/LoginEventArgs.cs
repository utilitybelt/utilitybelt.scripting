﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class LoginEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the character logging in
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// The account name of the character logging in
        /// </summary>
        public string Account { get; set; }

        public LoginEventArgs(uint id, string account) {
            Id = id;
            Account = account;
        }
    }
}
