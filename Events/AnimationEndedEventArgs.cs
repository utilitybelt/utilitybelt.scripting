﻿using System;
using System.Collections.Generic;
using System.Text;
using static UtilityBelt.Scripting.Interop.AnimTracker;

namespace UtilityBelt.Scripting.Events {
    public class AnimationEndedEventArgs : EventArgs {
        public Animation Animation { get; }
        public AnimationEndedEventArgs(Animation animation) {
            Animation = animation;
        }
    }
}
