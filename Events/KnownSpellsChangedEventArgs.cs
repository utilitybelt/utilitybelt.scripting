﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    public class KnownSpellsChangedEventArgs : System.EventArgs {
        public AddRemoveEventType Type { get; }
        public int SpellId { get; }
    }
}
