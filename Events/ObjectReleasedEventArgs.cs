﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// ObjectReleasedEventArgs
    /// </summary>
    public class ObjectReleasedEventArgs : System.EventArgs {
        /// <summary>
        /// The object id that was released
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="objectId"></param>
        public ObjectReleasedEventArgs(uint objectId) {
            ObjectId = objectId;
        }
    }
}
