﻿using System;

namespace UtilityBelt.Scripting.Events {
    public class LevelChangedEventArgs : EventArgs {
        /// <summary>
        /// Your character's current level
        /// </summary>
        public int Level;

        /// <summary>
        /// Your character's old level
        /// </summary>
        public int OldLevel;

        internal LevelChangedEventArgs(int level, int oldLevel) {
            Level = level;
            OldLevel = oldLevel;
        }
    }
}