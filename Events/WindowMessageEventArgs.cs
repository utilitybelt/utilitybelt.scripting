﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Window message event args
    /// </summary>
    public class WindowMessageEventArgs : EventArgs {

        /// <summary>
        /// The window handle
        /// </summary>
        public int HWND { get; }

        /// <summary>
        /// The message
        /// </summary>
        public short Msg { get; }

        /// <summary>
        /// The WParam
        /// </summary>
        public int WParam { get; }

        /// <summary>
        /// The LParam
        /// </summary>
        public int LParam { get; }

        /// <summary>
        /// Set this to true to prevent the message from reaching imgui / game handlers.
        /// </summary>
        public bool Eat { get; set; } = false;


        public WindowMessageEventArgs(int hwnd, short msg, int wParam, int lParam) {
            HWND = hwnd;
            Msg = msg;
            WParam = wParam;
            LParam = lParam;
        }
    }
}
