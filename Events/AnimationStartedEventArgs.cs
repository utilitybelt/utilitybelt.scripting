﻿using System;
using System.Collections.Generic;
using System.Text;
using static UtilityBelt.Scripting.Interop.AnimTracker;

namespace UtilityBelt.Scripting.Events {
    public class AnimationStartedEventArgs : EventArgs {
        public Animation Animation { get; }
        public AnimationStartedEventArgs(Animation animation) {
            Animation = animation;
        }
    }
}
