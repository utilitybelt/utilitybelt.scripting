﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Events {
    public class EnchantmentsChangedEventArgs : System.EventArgs {

        /// <summary>
        /// Wether the enchantment was added or removed
        /// </summary>
        public AddRemoveEventType Type { get; }

        /// <summary>
        /// The layered id of the spell for this enchantment
        /// </summary>
        public LayeredSpellId LayeredSpellId { get; }

        /// <summary>
        /// Enchantment information
        /// </summary>
        public Interop.Enchantment Enchantment { get; }

        public EnchantmentsChangedEventArgs(AddRemoveEventType type, Interop.Enchantment enchantment) {
            Type = type;
            LayeredSpellId = enchantment.LayeredId;
            Enchantment = enchantment;
        }
    }
}
