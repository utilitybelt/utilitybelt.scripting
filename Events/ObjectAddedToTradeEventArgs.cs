﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Scripting.Events {
    public class ObjectAddedToTradeEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the object added to the trade window
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The side of the trade window the object was added on
        /// </summary>
        public TradeSide Side { get; }

        public ObjectAddedToTradeEventArgs(uint objectId, TradeSide side) {
            ObjectId = objectId;
            Side = side;
        }
    }
}
