﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class TradeDeclinedEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the person who declined the trade
        /// </summary>
        public uint DeclinerId { get; }

        public TradeDeclinedEventArgs(uint declinerId) {
            DeclinerId = declinerId;
        }
    }
}
