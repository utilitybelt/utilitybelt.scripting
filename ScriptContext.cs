﻿using WattleScript.Interpreter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common;
using UtilityBelt.Common.Messages;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.ScriptEnvs.Lua;
using UtilityBelt.Scripting.Enums;
using System.Reflection;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using UtilityBelt.Scripting.Interop.Modules;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Threading;
using WattleScript.Interpreter.Interop;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace UtilityBelt.Scripting
{
    public class ScriptContext : IDisposable {
        private readonly ScriptManager _manager;

        private List<object> _instances = new List<object>();
        internal static Dictionary<string, DynValue> _moduleRetValues { get; } = new Dictionary<string, DynValue>();
        private static readonly Dictionary<string, Dictionary<string, DynValue>> _cachedGlobals = new Dictionary<string, Dictionary<string, DynValue>>();
        internal Dictionary<string, DynValue> _scriptModuleRetValues { get; } = new Dictionary<string, DynValue>();

        /// <summary>
        /// Holds all globals exposed to the script.
        /// </summary>
        public Dictionary<string, DynValue> Globals { get; } = new Dictionary<string, DynValue>();

        /// <summary>
        /// The name of the script.  If IsOnDisk is true, this is also the name of the directory the script lives in.
        /// </summary>
        public string ScriptName { get; }

        /// <summary>
        /// The startup type of the script.  Scripts can be started manually, automatically on all clients (global), automatically per account, or automatically per character.
        /// </summary>
        public ScriptStartupType StartupType { get; }
        public UBScript Script { get; internal set; }

        /// <summary>
        /// The directory this script lives in.  Only valid if IsOnDisk is set to true
        /// </summary>
        public string ScriptDirectory { get; }

        /// <summary>
        /// The data directory for this script.
        /// </summary>
        public string ScriptDataDirectory { get; }

        /// <summary>
        /// Wether this script exists on disk or is temporary
        /// </summary>
        public bool IsOnDisk { get; }

        public ScriptContext(ScriptManager manager, string name, ScriptStartupType startupType, string scriptDataDirectory, bool isOnDisk = true, string directory = null) {
            ScriptName = name;
            StartupType = startupType;
            IsOnDisk = isOnDisk;
            _manager = manager;
            ScriptDirectory = directory;
            ScriptDataDirectory = scriptDataDirectory;
        }

        public DynValue Sleep(int milliseconds) {
            return DynValue.NewYieldReq(new DynValue[] {
                DynValue.FromObject(Script.WattleScript, GetGame().Actions.Sleep((uint)milliseconds))
            });
        }

        public DynValue Await(QueueAction action) {
            return DynValue.NewYieldReq(new DynValue[] {
                DynValue.FromObject(Script.WattleScript, action)
            });
        }

        public Game GetGame() {
            if (Globals.TryGetValue("game", out var game)) {
               return (game.UserData?.Object as Game);
            }
            return null;
        }

        internal void PopulateDefaultGlobals() {
            // load base module
            var baseModule = ScriptableTypes.GetModule("base");

            Script.WattleScript.Globals["await"] = Await;
            Script.WattleScript.Globals["sleep"] = Sleep;


            Globals.Add("DateTime", UserData.CreateStatic(new LuaDescriptor(typeof(DateTime))));
            Globals.Add("TimeSpan", UserData.CreateStatic(new LuaDescriptor(typeof(TimeSpan))));
            Globals.Add("Enum", UserData.CreateStatic(new LuaDescriptor(typeof(Enum))));
            Globals.Add("Regex", UserData.CreateStatic(new LuaDescriptor(typeof(System.Text.RegularExpressions.Regex))));
            Globals.Add("RegexOptions", UserData.CreateStatic(new LuaEnumDescriptor(typeof(System.Text.RegularExpressions.RegexOptions))));

            foreach (var kv in baseModule.GetInstances()) {
                var instance = CreateInstance(kv.Value);
                _instances.Add(instance);
                Globals.Add(kv.Key, UserData.Create(instance, new LuaDescriptor(kv.Value)));
            }

            // we should probably do this better...
            Globals.Add("require", DynValue.NewCallback((ctx, args) => {
                try {
                    var moduleName = args[0].String?.ToLower() ?? "";
                    if (string.IsNullOrEmpty(moduleName)) {
                        throw new Exception($"require() expects the first argument to be a non-empty string");
                    }

                    var module = ScriptableTypes.GetModule(moduleName);


                    if (_scriptModuleRetValues.TryGetValue(moduleName, out DynValue scriptModule)) {
                        ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Loading {moduleName} from cache");
                        return scriptModule;
                    }

                    var shouldCache = moduleName != "filesystem" && moduleName != "utilitybelt.views";
                    _manager.Resolve<ILogger>()?.LogTrace($"Loaded module {moduleName}: {(module?.ToString() ?? "null")} ShouldCache:{shouldCache}");

                    if (!shouldCache || !_moduleRetValues.TryGetValue(moduleName, out DynValue cachedRetValue)) {
                        if (module == null) {
                            var moduleRet = TryLoadScript(moduleName);
                            _scriptModuleRetValues.Add(moduleName, moduleRet);
                            return moduleRet;
                        }
                        var globals = GetGlobals(moduleName);

                        var table = new Table(Script.WattleScript);

                        foreach (var global in globals) {
                            table[global.Key] = global.Value;
                        }

                        foreach (var kv in module.GetInstances()) {
                            var instance = CreateInstance(kv.Value);
                            _instances.Add(instance);
                            table[kv.Key] = UserData.Create(instance, new LuaDescriptor(kv.Value));
                        }

                        DynValue retVal;

                        if (module.ModuleReturnType != null) {
                            if (module.ModuleReturnType.IsClass && module.ModuleReturnType.IsAbstract && module.ModuleReturnType.IsSealed) {
                                retVal = UserData.CreateStatic(new LuaDescriptor(module.ModuleReturnType));
                            }
                            else {
                                retVal = UserData.Create(CreateInstance(module.ModuleReturnType), new LuaDescriptor(module.ModuleReturnType));
                            }
                        }
                        else {
                            retVal = DynValue.NewTable(table);
                        }

                        if (shouldCache) {
                            _moduleRetValues.Add(moduleName, retVal);
                        }

                        return retVal;
                    }
                    else {
                        return cachedRetValue;
                    }
                }
                catch (Exception ex) {
                    _manager.Resolve<ILogger>()?.LogError(ex.ToString());
                }

                return DynValue.Nil;
            }));

            foreach (var kv in GetGlobals("base")) {
                Globals.Add(kv.Key, kv.Value);
            }
        }

        private DynValue TryLoadScript(string moduleName) {
            string error = null;
            if (!IsValidFilePath(moduleName, ref error)) {
                Script?.ThrowError(error, new Exception());
                return DynValue.Nil;
            }

            var resolvedPath = ResolvePath(moduleName);
            if (System.IO.File.Exists(resolvedPath)) {
                return Script.RequireFile(moduleName);
            }

            if (System.IO.File.Exists(resolvedPath + ".lua")) {
                return Script.RequireFile(moduleName + ".lua");
            }

            return DynValue.Nil;
        }

        internal string ResolvePath(string filename) {
            if (filename.StartsWith(System.IO.Path.DirectorySeparatorChar.ToString())) {
                filename = filename.Substring(1);
            }
            return System.IO.Path.GetFullPath(System.IO.Path.Combine(ScriptDirectory, filename));
        }

        /// <summary>
        /// Check if a file path is valid. This checks that the path isn't trying to escape the sandbox directory
        /// and that it doesn't contain any invalid characters.
        /// </summary>
        /// <param name="filename">The filename to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns></returns>
        internal bool IsValidFilePath(string filename, ref string error) {
            var resolvedPath = ResolvePath(filename);
            var invalidPathChars = System.IO.Path.GetInvalidFileNameChars().Where(c => System.IO.Path.GetFileName(resolvedPath).Contains(c));
            if (invalidPathChars.Count() > 0) {
                error = $"Filename contains invalid characters: {string.Join("", invalidPathChars)}";
                return false;
            }

            if (!resolvedPath.StartsWith(ScriptDirectory)) {
                error = "Filename is invalid. Tried to escape sandbox.";
                return false;
            }

            if (System.IO.Directory.Exists(resolvedPath)) {
                error = "Filename is a directory.";
                return false;
            }

            return true;
        }

        private Dictionary<string, DynValue> GetGlobals(string moduleName) {
            if (_cachedGlobals.TryGetValue(moduleName, out var _cached)) {
                return _cached;
            }

            var globals = new Dictionary<string, DynValue>();
            var module = ScriptableTypes.GetModule(moduleName);
            var retValue = module != null ? DynValue.True : DynValue.False;

            if (module != null) {
                foreach (var global in module.GetGlobals()) {
                    if (global.Value.IsEnum) {
                        globals.Add(global.Key, UserData.CreateStatic(new LuaEnumDescriptor(global.Value)));
                    }
                    else {
                        globals.Add(global.Key, UserData.CreateStatic(new LuaDescriptor(global.Value)));
                    }
                }
            }

            _cachedGlobals.Add(moduleName, globals);

            return globals;
        }

        private object CreateInstance(Type type) {
            try {
                object instance = Activator.CreateInstance(type, true);

                var propBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

                var scriptNameProp = instance.GetType().GetProperty("ScriptName", propBindingFlags);
                if (scriptNameProp != null && scriptNameProp.PropertyType == typeof(string)) {
                    scriptNameProp.SetValue(instance, ScriptName);
                }

                var scriptManagerProp = instance.GetType().GetProperty("ScriptManager", propBindingFlags);
                if (scriptManagerProp != null && scriptManagerProp.PropertyType == typeof(ScriptManager)) {
                    scriptManagerProp.SetValue(instance, _manager);
                }

                var scriptDirectoryProp = instance.GetType().GetProperty("ScriptDirectory", propBindingFlags);
                if (scriptDirectoryProp != null && scriptDirectoryProp.PropertyType == typeof(string)) {
                    scriptDirectoryProp.SetValue(instance, ScriptDirectory);
                }

                var scriptDataDirectoryProp = instance.GetType().GetProperty("ScriptDataDirectory", propBindingFlags);
                if (scriptDataDirectoryProp != null && scriptDataDirectoryProp.PropertyType == typeof(string)) {
                    scriptDataDirectoryProp.SetValue(instance, ScriptDataDirectory);
                }

                return instance;
            }
            catch (Exception ex) {
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
            }

            return null;
        }

        public void Dispose() {
            if (Globals.TryGetValue("game", out var game)) {
                (game.UserData?.Object as Game)?.Dispose();
            }

            foreach (var module in _moduleRetValues) {
                if (module.Value.UserData?.Object is IDisposable iDisp) {
                    iDisp.Dispose();
                }
            }

            LuaEventFacade.RemoveScript(ScriptName);
            foreach (var instance in _instances) {
                if (instance == null)
                    continue;

                var disposeMethod = instance.GetType().GetMethod("Dispose", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, new Type[] { }, new ParameterModifier[] { });

                if (disposeMethod != null) {
                    try {
                        disposeMethod.Invoke(instance, null);
                    }
                    catch (Exception ex) {
                        _manager.Resolve<ILogger>()?.LogError(ex.ToString());
                    }
                }
            }
            _instances.Clear();
        }
    }
}
