﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// Errors from actions
    /// </summary>
    public enum ActionError {
        /// <summary>
        /// No error.
        /// </summary>
        None,

        /// <summary>
        /// Request to the server timed out.
        /// </summary>
        TimedOut,

        /// <summary> 
        /// Server complained. See [ActionResult.ErrorString](xref:UBScript.Interop.QueueAction#UBScript_Interop_QueueAction_ErrorDetails) for the reason.
        /// </summary>
        ServerError,

        /// <summary>
        /// Must be logged in
        /// </summary>
        NotLoggedIn,

        /// <summary>
        /// Tried to spend more experience than you have available.
        /// </summary>
        NotEnoughUnassignedExperience,

        /// <summary>
        /// Attribute / skill can't be raised any further.
        /// </summary>
        AlreadyMaxed,

        /// <summary>
        /// Spending this much experience would "overflow" the attribute / skill.  You need to cap
        /// spendExperience at the total amount left to max the attribute.
        /// </summary>
        TooMuchSpendExperience,

        /// <summary>
        /// Tried to perform an action that is only valid on your character, on another character
        /// </summary>
        NotYourCharacter,

        /// <summary>
        /// You are not trained in this skill
        /// </summary>
        NotTrained,
        YouDoNotPassCraftingRequirements,
        ItemBeingTraded,
        YouHaveBeenInPKBattleTooRecently,
        CantUseOnItself,
        TargetItemNotInInventoryOrLandscape,
        SourceItemNotInInventory,
        SourceItemNotInInventoryOrLandscape,
        InvalidTargetItem,
        InvalidSourceObject,
        ItemAlreadyWielded,
        TooEncumbered,
        MustBeInPeaceMode,
        YouCantPickThatUp,
        InvalidInventoryLocation,
        YouDontKnowThatSpell,
        MissingComponents,
        UnknownSpellId,
        SpellFizzled,
        AlreadyInFellow,
        MustBeInFellow,
        NotTheLeader,
        NotInFellow,
        MemberDoesntExist,
        FellowshipIsFull,
        FellowshipClosed,
        AlreadyRecruited,
        FellowshipRecruitBusy,
        FellowshipLocked,
        InvalidTargetObject,
        UnknownCommand,
        CharacterDoesntExist,
        CharacterPendingDelete,
        NotAtCharacterSelect,
        PreconditionFailed,
        NoUst,
        UnsalvageableItem,
        NoTradePartner,
        Exception,
        AlreadyTrained,
        CantAdvanceSkill,
        NotEnoughAvailableCredits,
        SkillDoesntExist,
        NotInAnAllegiance,
        AlreadyInAnAllegiance,
        SourceObjectTooFar,
        NoActions,
        Cancelled,
        NPCDoesntKnowWhatToDoWithThat,
        TooBusy,
        VendorNotOpen,
        VendorDoesntHaveThisItem,
        VendorDoesntHaveEnoughOfThisItem,
        CantSplitThisObject,
        ObjectSplitAmountTooBig,
        InvalidSpellLevel,
        InvalidCombatMode,
        OnCooldown
    }
}
