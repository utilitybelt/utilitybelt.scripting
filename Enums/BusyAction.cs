﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// The different actions a character can be busy doing. Only one action is allowed at a time.
    /// </summary>
    [Flags]
    public enum BusyAction {
        /// <summary>
        /// Not busy
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// Crafting (using one item on another)
        /// </summary>
        Crafting = 0x00000001,

        /// <summary>
        /// Wielding / unwielding a piece of equipment
        /// </summary>
        Wielding = 0x00000002,

        /// <summary>
        /// Casting a spell
        /// </summary>
        SpellCasting = 0x00000004,

        /// <summary>
        /// Opening trade with someone (double clicked / used them)
        /// </summary>
        OpeningTrade = 0x00000008,

        /// <summary>
        /// Dropping an object on the ground
        /// </summary>
        DroppingWeenie = 0x00000010,

        /// <summary>
        /// Giving an object
        /// </summary>
        GivingWeenie = 0x00000020,

        /// <summary>
        /// Logging out
        /// </summary>
        LoggingOut = 0x00000040,

        /// <summary>
        /// Moving an object
        /// </summary>
        MovingWeenie = 0x00000080,

        /// <summary>
        /// Using an item
        /// </summary>
        UsingWeenie = 0x00000100,

        /// <summary>
        /// Moving a weenie to a container
        /// </summary>
        MovingWeenieToContainer = 0x00000200,

        /// <summary>
        /// Recalling to lifestone with /lifestone command
        /// </summary>
        TeleToLifestone = 0x00000400,

        /// <summary>
        /// Character in portal space
        /// </summary>
        Portalling = 0x00000800,

        /// <summary>
        /// Character is playing an animation
        /// </summary>
        Animating = 0x00001000,

        /// <summary>
        /// Changing combat mode
        /// </summary>
        ChangeCombatMode = 0x00002000,

        /// <summary>
        /// Recalling to marketplace, with /mp command
        /// </summary>
        TeleToMarketplace = 0x00004000,

        /// <summary>
        /// Recalling to pk arena, with /pkarena command
        /// </summary>
        TeleToPKArena = 0x00008000,

        /// <summary>
        /// Being teleported by advocate?
        /// </summary>
        AdvocateTeleport = 0x00010000,

        /// <summary>
        /// Teleporting to your house, with /hr command
        /// </summary>
        TeleToHouse = 0x00020000,

        /// <summary>
        /// Teleporting to allegiance mansion, with /hom command
        /// </summary>
        TeleToMansion = 0x00040000,

        /// <summary>
        /// Teleporting to allegiance hometown, with /ah command
        /// </summary>
        TeleToAllegianceHometown = 0x00080000,


        /// <summary>
        /// All recall commands
        /// </summary>
        RecallCommand = TeleToLifestone | TeleToMarketplace | TeleToPKArena | AdvocateTeleport | TeleToHouse | TeleToMansion | TeleToAllegianceHometown,
    }
}
