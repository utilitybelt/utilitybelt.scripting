﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// Action Types, for use in the Queue system. These are ordered by default priority
    /// with higher numbers having higher priority.
    /// </summary>
    public enum ActionType : int {
        /// <summary>
        /// This is a special action type that gets executed immediately, bypassing the queue completely
        /// </summary>
        Immediate = int.MaxValue,

        /// <summary>
        /// Wielding an item. This is highest default priority because we want to switch to the correct
        /// weapon before switching combat modes / casting spells / etc.
        /// </summary>
        Wield = 20000,

        /// <summary>
        /// Restoring health (spells / kits / pots)
        /// </summary>
        RestoreHealth = 19000,

        /// <summary>
        /// Restoring stamina (spells / kits / pots)
        /// </summary>
        RestoreStamina = 18000,

        /// <summary>
        /// Restoring mana (spells / kits / pots)
        /// </summary>
        RestoreMana = 17000,

        /// <summary>
        /// Logging in / out
        /// </summary>
        LoginLogoff = 16000,

        /// <summary>
        /// Vendoring
        /// </summary>
        Vendor = 15500,

        /// <summary>
        /// Trading with other players
        /// </summary>
        Trade = 15000,

        /// <summary>
        /// Combat
        /// </summary>
        Combat = 14000,

        /// <summary>
        /// Casting spells
        /// </summary>
        CastSpell = 13000,

        /// <summary>
        /// Navigating
        /// </summary>
        Navigation = 12000,

        /// <summary>
        /// Managing fellowships
        /// </summary>
        Fellow = 11000,

        /// <summary>
        /// Allegiance actions
        /// </summary>
        Allegiance = 10000,

        /// <summary>
        /// Inventory management
        /// </summary>
        Inventory = 9000,

        /// <summary>
        /// Misc...
        /// </summary>
        Misc = 8000,
    }
}
