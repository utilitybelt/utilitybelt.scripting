﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    public enum ClientConfirmationType {
        UNDEF,
        ALLEGIANCE_SWEAR,
        ALTER_SKILL,
        ALTER_ATTRIBUTE,
        FELLOWSHIP_RECRUIT,
        CRAFT_INTERACTION,
        USE_AUGMENTATION,
        YESNO
    }
}
