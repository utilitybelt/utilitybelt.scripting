﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting {
    internal class ScriptComponent {
        public Type RegisteredType { get; set; }
        public Type ImplementedType { get; set; }
        public object Instance { get; set; }
        public bool IsSingleton { get; set; }
    }
}
