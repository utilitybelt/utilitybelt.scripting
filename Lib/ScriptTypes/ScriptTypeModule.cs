﻿using WattleScript.Interpreter;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration.Assemblies;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using UtilityBelt.Scripting.Interop;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Lib.ScriptTypes {
    public class ScriptTypeModule {
        /// <summary>
        /// The name of the module this package belongs to.
        /// </summary>
        public string Name { get; private set; }

        internal Dictionary<Type, ScriptType> _scriptTypes = new Dictionary<Type, ScriptType>();
        internal List<Type> _types = new List<Type>();
        internal List<Type> _actions = new List<Type>();
        internal List<Type> _extensions = new List<Type>();
        internal Dictionary<string, Type> _globals = new Dictionary<string, Type>();
        internal Dictionary<string, Type> _instances = new Dictionary<string, Type>();
        internal Dictionary<Type, string> _typemap = new Dictionary<Type, string>();

        public Type ModuleReturnType { get; private set; }

        public string Description { get; private set; }
        public Assembly Assembly { get; internal set; }

        public ScriptTypeModule(string moduleName) {
            Name = moduleName;
        }

        public IEnumerable<Type> GetRegisteredTypes() {
            return _types;
        }

        public IEnumerable<Type> GetExtensions() {
            return _extensions;
        }

        public Dictionary<string, Type> GetGlobals() {
            return _globals;
        }

        public Dictionary<string, Type> GetInstances() {
            return _instances;
        }

        public IEnumerable<Type> GetActions() {
            return _actions;
        }

        public IEnumerable<ScriptType> GetScriptTypes() {
            return _scriptTypes.Values.ToList();
        }

        public bool TryGetScriptType(Type type, out ScriptType scriptType) {
            return _scriptTypes.TryGetValue(type, out scriptType);
        }

        internal void LoadPackageData(JObject packageData, Assembly assembly) {
            try {
                Console.WriteLine($"Loading package data: {packageData.ToString()}");

                var assemblyName = packageData.Property("assembly")?.Value?.Value<string>();
                var extensions = packageData.Property("extensions")?.Values()?.Select(j => j?.Value<string>()) ?? new List<string>();
                var includes = packageData.Property("includes")?.Values()?.Select(j => j?.Value<string>()) ?? new List<string>();
                var actions = packageData.Property("actions")?.Values()?.Select(j => j?.Value<string>()) ?? new List<string>();
                var excludes = packageData.Property("excludes")?.Values()?.Select(j => j?.Value<string>()) ?? new List<string>();
                var globals = packageData.Property("globals")?.Values()?.Select(j => j?.Value<string>()) ?? new List<string>();
                var returnVal = packageData.Property("return")?.Value?.Value<string>();
                if (string.IsNullOrEmpty(Description)) {
                    Description = packageData.Property("description")?.Value?.Value<string>();
                }

                assembly = assembly ?? AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a?.GetName()?.Name?.Equals(assemblyName) == true);

                if (packageData.Property("instances") != null) {
                    foreach (var prop in (packageData.Property("instances")?.Value as JObject)?.Properties()) {
                        if (!string.IsNullOrEmpty(prop?.Value?.Value<string>())) {
                            var type = assembly.GetType(prop?.Value?.Value<string>());
                            if (GlobalNameExists(prop?.Value?.Value<string>(), type, out string definer)) {
                                ScriptManager.Instance?.Resolve<ILogger>()?.LogError($"Tried to add instance '{prop.Name}' ({type.FullName}) from assembly {assembly?.GetName()?.Name ?? "System"}, but it was already defined by {definer}");
                                continue;
                            }

                            _instances.Add(prop.Name, type);
                        }
                    }
                }
                if (packageData.Property("typemap") != null) {
                    foreach (var prop in (packageData.Property("typemap")?.Value as JObject)?.Properties()) {
                        if (!string.IsNullOrEmpty(prop?.Value?.Value<string>())) {
                            _typemap.Add(assembly.GetType(prop.Name), prop?.Value?.Value<string>());
                        }
                    }
                }

                var extensionRes = extensions.Select(x => new Regex(x));
                var includeRes = includes.Select(x => new Regex(x));
                var actionRes = includes.Select(x => new Regex(x));
                var excludeRes = excludes.Select(x => new Regex(x));
                var globalRes = globals.Select(x => new Regex(x));

                var newTypes = new List<Type>();

                newTypes.AddRange(_instances.Values.Select(v => v));

                // register types / extensions / statics
                foreach (var type in assembly.GetExportedTypes()) {
                    if (!IsValidType(type) || excludeRes.Any(r => r.IsMatch(type.FullName))) {
                        continue;
                    }

                    if (includeRes.Any(r => r.IsMatch(type.FullName))) {
                        if (!newTypes.Contains(type)) {
                            newTypes.Add(type);
                        }
                    }

                    if (extensionRes.Any(r => r.IsMatch(type.FullName))) {
                        _extensions.Add(type);
                        if (!newTypes.Contains(type)) {
                            newTypes.Add(type);
                        }
                    }

                    if (actionRes.Any(r => r.IsMatch(type.FullName))) {
                        if (!_actions.Contains(type)) {
                            _actions.Add(type);
                        }
                    }

                    if (globalRes.Any(r => r.IsMatch(type.FullName))) {
                        string name;
                        if (!_typemap.TryGetValue(type, out name)) {
                            name = type.Name;
                        }

                        if (GlobalNameExists(type.Name, type, out string definer)) {
                            ScriptManager.Instance?.Resolve<ILogger>()?.LogError($"Tried to add global '{name}' ({type.FullName}) from assembly {assembly?.GetName()?.Name ?? "System"}, but it was already defined by {definer}");
                            continue;
                        }
                        if (!newTypes.Contains(type)) {
                            newTypes.Add(type);
                        }

                        _globals.Add(name, type);
                    }
                }
                Console.WriteLine($"Add type: {newTypes.Count}");

                foreach (var type in newTypes) {
                    if (_scriptTypes.TryGetValue(type, out var existingType)) {
                        ScriptManager.Instance?.Resolve<ILogger>()?.LogError($"Tried to add script type {type.FullName} // {type.Assembly.GetName().Name} but was already added by {type.FullName} // {existingType.Type.Assembly.GetName().Name}");
                    }
                    else {
                        _scriptTypes.Add(type, new ScriptType(type.Name, type, _globals.Values.Contains(type), this));
                        _types.Add(type);
                    }
                }

                if (!string.IsNullOrEmpty(returnVal)) {
                    ModuleReturnType = assembly.GetType(returnVal);
                }
                else {
                    ModuleReturnType = null;
                }
            }
            catch (ReflectionTypeLoadException ex) {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions) {
                    sb.AppendLine(exSub.Message);
                    FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                    if (exFileNotFound != null) {
                        if (!string.IsNullOrEmpty(exFileNotFound.FusionLog)) {
                            sb.AppendLine("Fusion Log:");
                            sb.AppendLine(exFileNotFound.FusionLog);
                        }
                    }
                    sb.AppendLine();
                }
                Console.WriteLine(ex.ToString());
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
            }
            catch (Exception ex) {
                Console.WriteLine(ex.ToString());
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
            }
        }

        private bool IsValidType(Type type) {
            return !type.FullName.Contains("<");
        }

        private bool GlobalNameExists(string key, Type type, out string definer) {
            if (_instances.ContainsKey(key)) {
                definer = $"{_instances[key].Assembly?.GetName()?.Name ?? "System"}(Global)";
                return true;
            }

            if (_globals.ContainsKey(key)) {
                definer = $"{_instances[key].Assembly?.GetName()?.Name ?? "System"}(Static)";
                return true;
            }

            definer = null;
            return false;
        }
    }
}
