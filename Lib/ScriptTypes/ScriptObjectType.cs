﻿namespace UtilityBelt.Scripting.Lib.ScriptTypes
{
    public enum ScriptObjectType
    {
        Unknown,
        Enum,
        Class,
        EnumField,
        Constructor,
        Field,
        Property,
        Event,
        Method,
        Struct,
    }
}