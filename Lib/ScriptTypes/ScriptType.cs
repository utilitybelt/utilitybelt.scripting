﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using UtilityBelt.Scripting.Interop;
using Exception = System.Exception;

namespace UtilityBelt.Scripting.Lib.ScriptTypes {
    public class ScriptType {
        private bool _didLoadDocs = false;
        private static List<Assembly> _loadedDocsAssemblies = new List<Assembly>();
        private static Dictionary<string, ScriptTypeDocumentation> _docsCache = new Dictionary<string, ScriptTypeDocumentation>();

        public Type Type { get; }
        public Type ParentType { get; }

        public string Name { get; }

        public ScriptObjectType ObjectType { get; internal set; }

        public MemberInfo MemberInfo { get; }

        private string _id = null;
        public string Id {
            get {
                if (_id == null) {
                    _id = MakeId();
                }
                return _id;
            }
        }

        public bool IsGlobal { get; } = false;

        public ScriptTypeModule Module { get; }

        public List<ScriptType> Children { get; } = new List<ScriptType>();

        private ScriptTypeDocumentation _docs = null;
        public ScriptTypeDocumentation Docs {
            get {
                LoadDocumentation();

                if (!_docsCache.TryGetValue(Id, out ScriptTypeDocumentation value)) {
                    value = new ScriptTypeDocumentation() {
                        Summary = ObjectType == ScriptObjectType.Constructor ? $"Create a new {ParentType.Name} instance." : ""
                    };
                    _docsCache.Add(Id, value);
                }

                return value;
            }
        }

        public ScriptType(MemberInfo memberInfo, Type type, ScriptObjectType objectType, Type parentType, string name, bool isGlobal, ScriptTypeModule module) {
            MemberInfo = memberInfo;
            ObjectType = objectType;
            ParentType = parentType;
            Type = type;
            Name = MemberInfo?.Name ?? name ?? "";
            IsGlobal = isGlobal;
            Module = module;
        }

        public ScriptType(string name, Type type, bool isGlobal, ScriptTypeModule module) {
            Name = name;
            Type = type;
            MemberInfo = Type;
            IsGlobal = isGlobal;
            Module = module;

            if (type.IsEnum) {
                ObjectType = ScriptObjectType.Enum;
                LoadMembers();
            }
            else if (type.IsClass) {
                ObjectType = ScriptObjectType.Class;
                LoadMembers();
            }
            else if (type.IsValueType && !type.IsPrimitive) {
                ObjectType = ScriptObjectType.Struct;
                LoadMembers();
            }
        }

        private void LoadMembers() {
            var disallowedMethods = new Regex("^(Equals|ToString|GetHashCode|GetType)$");
            if (ObjectType == ScriptObjectType.Enum) {
                foreach (var name in Enum.GetNames(Type)) {
                    Children.Add(new ScriptType(null, Type, ScriptObjectType.EnumField, Type, name, IsGlobal, Module));
                }
            }
            else if (ObjectType == ScriptObjectType.Class || ObjectType == ScriptObjectType.Struct) {
                foreach (var member in Type.GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)) {
                    switch (member.MemberType) {
                        case MemberTypes.Constructor:
                            Children.Add(new ScriptType(member, (member as ConstructorInfo).DeclaringType, ScriptObjectType.Constructor, (member as ConstructorInfo).DeclaringType, "", IsGlobal, Module));
                            break;
                        case MemberTypes.Field:
                            Children.Add(new ScriptType(member, (member as FieldInfo).FieldType, ScriptObjectType.Field, (member as FieldInfo).DeclaringType, "", IsGlobal, Module));
                            break;
                        case MemberTypes.Property:
                            Children.Add(new ScriptType(member, (member as PropertyInfo).PropertyType, ScriptObjectType.Property, (member as PropertyInfo).DeclaringType, "", IsGlobal, Module));
                            break;
                        case MemberTypes.Event:
                            Children.Add(new ScriptType(member, (member as EventInfo).EventHandlerType, ScriptObjectType.Event, (member as EventInfo).DeclaringType, "", IsGlobal, Module));
                            break;
                        case MemberTypes.Method:
                            if ((member as MethodInfo).IsSpecialName)
                                continue;
                            if (member.DeclaringType.GetProperties().Any(prop => prop.GetSetMethod() == member || prop.GetGetMethod() == member))
                                continue;
                            if (disallowedMethods.IsMatch(member.Name))
                                continue;
                            Children.Add(new ScriptType(member, (member as MethodInfo).ReturnType, ScriptObjectType.Method, (member as MethodInfo).DeclaringType, "", IsGlobal, Module));
                            break;
                    }
                }
            }
        }

        private void LoadDocumentation() {
            if (_didLoadDocs || _loadedDocsAssemblies.Contains(Type.Assembly))
                return;

            _didLoadDocs = true;

            if (Type.Assembly.IsDynamic) {
                _loadedDocsAssemblies.Add(Type.Assembly);
                return;
            }

            if (string.IsNullOrEmpty(Type.Assembly.Location)) {
                _loadedDocsAssemblies.Add(Type.Assembly);
                return;
            }

            var xmlPath = Type.Assembly.Location.Replace(Path.GetExtension(Type.Assembly.Location), ".xml");
            if (!File.Exists(xmlPath)) {
                _loadedDocsAssemblies.Add(Type.Assembly);
                return;
            }

            try {
                XDocument xdoc = new XDocument();
                xdoc = XDocument.Parse(File.ReadAllText(xmlPath));

                foreach (var e in xdoc.Descendants("member")) {
                    var docs = ScriptTypeDocumentation.FromMember(e);

                    if (_docsCache.ContainsKey(e.Attribute("name").Value)) {
                        _docsCache[e.Attribute("name").Value] = docs;
                    }
                    else {
                        _docsCache.Add(e.Attribute("name").Value, docs);
                    }
                }

                foreach (var key in _docsCache.Keys) {
                    var docs = _docsCache[key];
                    if (docs?.Summary?.StartsWith("inheritdoc|") == true) {
                        var id = docs.Summary.Split('|').Last();
                        if (_docsCache.TryGetValue(id, out var inheritedDocs)) {
                            _docsCache[key].Summary = inheritedDocs.Summary;
                        }
                    }
                }

                _loadedDocsAssemblies.Add(Type.Assembly);
            }
            catch (Exception ex) {
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
                Console.WriteLine(ex.ToString());
                _loadedDocsAssemblies.Add(Type.Assembly);
            }
        }

        private string MakeId() {
            switch (ObjectType) {
                case ScriptObjectType.Struct:
                case ScriptObjectType.Class:
                case ScriptObjectType.Enum:
                    return $"T:{FormatType(Type)}";
                case ScriptObjectType.Property:
                    return $"P:{FormatType(ParentType)}.{Name}";
                case ScriptObjectType.EnumField:
                case ScriptObjectType.Field:
                    return $"F:{FormatType(ParentType)}.{Name}";
                case ScriptObjectType.Constructor:
                    return $"M:{FormatMethod(ParentType, MemberInfo as ConstructorInfo)}";
                case ScriptObjectType.Method:
                    return $"M:{FormatMethod(ParentType, MemberInfo as MethodInfo)}";
                case ScriptObjectType.Event:
                    return $"E:{FormatType(ParentType)}.{Name}";
                default:
                    return $"UnknownId:{Type.FullName}//{Name}";
            }
        }

        private object FormatMethod(Type parentType, ConstructorInfo constructorInfo) {
            return $"{parentType.Namespace}.{parentType.Name}.#ctor{FormatParameters(constructorInfo.GetParameters())}";
        }

        private object FormatMethod(Type parentType, MethodInfo methodInfo) {
            return $"{parentType.Namespace}.{parentType.Name}.{methodInfo.Name}{FormatArity(methodInfo)}{FormatParameters(methodInfo.GetParameters())}";
        }

        private object FormatArity(MemberInfo memberInfo) {
            if (memberInfo is MethodInfo methodInfo) {
                if (methodInfo.IsGenericMethodDefinition) {
                    return $"``{methodInfo.GetParameters().Count()}";
                }
            }
            else if (memberInfo is ConstructorInfo constructorInfo) {
                if (constructorInfo.IsGenericMethodDefinition) {
                    return $"``{constructorInfo.GetParameters().Count()}";
                }
            }

            return "";
        }

        private object FormatParameters(ParameterInfo[] parameterInfos) {
            if (parameterInfos.Length > 0)
                return $"({string.Join(",", parameterInfos.Select(p => $"{FormatType(p.ParameterType)}{(p.ParameterType.Name.Contains("&") ? "@" : "")}"))})";
            return "";
        }

        public static string FormatType(Type type) {
            if (type == null || type == typeof(void)) {
                return "void";
            }
            if (type.IsGenericType || type.IsGenericParameter || type.GetGenericArguments().Length > 0) {
                var typeStr = $"{type.Namespace}.{type.Name.Split('`').First()}";
                var genericTypeArgs = type.GetGenericArguments().Select(a => FormatType(a));

                return $"{typeStr}{{{string.Join(",", genericTypeArgs)}}}";
            }

            return $"{type.Namespace}.{type.Name.Split('`').First().Split('&').First()}";
        }
    }
}
