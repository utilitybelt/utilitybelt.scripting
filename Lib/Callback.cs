﻿using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Lib {
    public class Callback {
        private readonly ILogger _log;

        public Action CleanupAction { get; set; }
        public Action HandleTimeout { get; set; }

        public Action<object> CallbackFunc { get; set; }
        
        public DateTime StartedAt { get; set; }

        // todo: whats a good default timeout?
        public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(5);

        public bool WasCalled { get; private set; } = false;

        private bool needsCleanup = true;
        public DateTime InvokeAt { get; private set; } = DateTime.MaxValue;
        public object InvokeWith { get; private set; } = null;
        public bool IsScheduled { get; private set; } = false;

        public Callback(ILogger logger, ScriptManager manager) {
            _log = logger;
            StartedAt = DateTime.UtcNow;
        }

        public void Invoke(object arg) {
            try {
                if (WasCalled)
                    return;

                var cbFunc = CallbackFunc;
                if (cbFunc != null) {
                    try {
                        //_log.Log($"Invoking cb with: {arg}");
                        cbFunc.Invoke(arg);
                    }
                    catch (Exception ex) { _log.LogError(ex.ToString()); }
                }
                Cleanup();
                WasCalled = true;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void InvokeAfter(TimeSpan delay, object arg) {
            if (IsScheduled)
                return;

            InvokeWith = arg;
            InvokeAt = DateTime.UtcNow + delay;

            CleanupAction?.Invoke();
            IsScheduled = true;
        }

        private void Cleanup() {
            if (!needsCleanup)
                return;

            CleanupAction?.Invoke();
            needsCleanup = false;
        }
    }
}
