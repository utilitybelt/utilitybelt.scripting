﻿using WattleScript.Interpreter;
using WattleScript.Interpreter.Loaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.ScriptEnvs.Lua;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace UtilityBelt.Scripting {
    public class UBScript : IDisposable {

        public enum State {
            Running,
            Paused,
            Stopped
        }

        public class LogEventArgs : EventArgs {
            public string Text { get; }

            public LogEventArgs(string text) {
                Text = text;
            }
        }

        private ILogger _log;

        public string Name { get; }
        public string Directory { get; }
        public ScriptContext Context { get; }
        public Script WattleScript;
        private bool didInit;
        public List<string> Logs = new List<string>();

        public State CurrentState { get; private set; }

        internal readonly Dictionary<DynValue, DynValue> Coroutines = new Dictionary<DynValue, DynValue>();
        internal readonly Dictionary<DynValue, TaskCompletionSource<DynValue>> CoroutineTasks = new Dictionary<DynValue, TaskCompletionSource<DynValue>>();

        public event EventHandler<LogEventArgs> OnLogText;

        public UBScript(string name, string directory, ScriptContext context, ILogger logger) {
            _log = logger;
            Name = name;
            Directory = directory;
            Context = context;
        }

        private void Log(string text, LogLevel level = LogLevel.Information) {
            text = text.Replace("\t", " ");
            if (Directory != null) {
                text = text.Replace(Directory.TrimEnd('\\') + '\\', "");
            }
            _log.Log(level, $"[{Name}] {text.Replace("\t", " ")}");
            Logs.Add(text);
            OnLogText?.Invoke(this, new LogEventArgs(text.Replace("\t", " ")));
        }

        async internal void Init() {
            if (didInit)
                return;

            WattleScript = new Script(CoreModules.Preset_SoftSandbox | CoreModules.Debug);
            WattleScript.Options.DebugPrint = (s) => {
                Log(s, LogLevel.Information);
            };
            WattleScript.Options.UseLuaErrorLocations = false;
            WattleScript.Options.ScriptLoader = new LuaScriptLoader(this);
            WattleScript.Options.AutoAwait = true;
            WattleScript.Options.Syntax = ScriptSyntax.Lua;
            WattleScript.Options.CheckThreadAccess = true;
            //WattleScript.Options.InstructionLimit = 0;

            Context.PopulateDefaultGlobals();

            foreach (var kv in Context.Globals) {
                WattleScript.Globals.Set(kv.Key, kv.Value);
            }

            CurrentState = State.Running;

            LoadDirectoryScripts();
            didInit = true;
        }

        private void LoadDirectoryScripts() {
            if (Name.Equals("Global") || string.IsNullOrEmpty(Directory))
                return;

            // todo: support for projectinfo.json that allows for customized start script?
            var indexScript = "index.lua";
            var scriptPath = Path.Combine(Directory, indexScript);

            if (File.Exists(scriptPath)) {
                RunFile(scriptPath);
            }
            else {
                throw new Exception($"Could not find script: {scriptPath}");
            }
        }

        public void Pause() {
            CurrentState = State.Paused;
        }

        public void Unpause() {
            CurrentState = State.Running;
        }

        internal void RunCoroutines() {
            var cos = Coroutines.Keys.ToArray();
            foreach (var co in cos) {
                RunCoroutine(co);
            }
        }

        public DynValue RequireFile(string filename) {
            return RunWithCatch(() => {
                var scriptPath = Path.Combine(Directory, filename);
                return WattleScript.DoFile(scriptPath);
            });
        }

        private DynValue Run(Func<DynValue> value) {
            return value();
        }

        private DynValue RunWithCatch(Func<DynValue> value) {
            DynValue result = DynValue.Nil;
            try {
                result = value();
            }
            catch (ScriptRuntimeException ex2) {
                Log($"An error occured running script {Name}: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}", LogLevel.Error);
            }
            catch (SyntaxErrorException ex3) {
                Log($"A syntax error occured running script {Name}: {ex3.DecoratedMessage}", LogLevel.Error);
            }
            catch (Exception arg) {
                Log($"Error running  script {Name}: {arg}", LogLevel.Error);
            }

            return result;
        }

        public DynValue AddAndRunCoroutine(DynValue co, object o01 = null) {
            if (co.Type != DataType.Thread) {
                throw new Exception($"Tried to add non-coroutine DynValue: ({co.Type}) {co}");
            }

            return RunCoroutine(co, o01);
        }

        private DynValue RunCoroutine(DynValue co, object o01 = null) {
            DynValue result = DynValue.Nil;
            Exception rex = null;
            try {
                if (Coroutines.TryGetValue(co, out var lastRes)) {
                    if (lastRes.Type == DataType.UserData && lastRes.UserData.Object is QueueAction action) {
                        if (action.IsFinished) {
                            result = co.Coroutine.Resume(lastRes);
                        }
                        else {
                            result = lastRes;
                        }
                    }
                    else {
                        result = co.Coroutine.Resume(lastRes);
                    }
                }
                else {
                    result = co.Coroutine.Resume(o01);
                }
            }
            catch (ScriptRuntimeException ex2) {
                Log($"An error occured running script {Name}: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}", LogLevel.Error);
            }
            catch (SyntaxErrorException ex3) {
                Log($"A syntax error occured running script {Name}: {ex3.DecoratedMessage}", LogLevel.Error);
            }
            catch (Exception arg) {
                Log($"Error running  script {Name}: {arg}", LogLevel.Error);
            }

            if (rex is not null) {
                Coroutines.Remove(co);
                if (CoroutineTasks.TryGetValue(co, out var task)) {
                    task.SetException(rex);
                }

                return DynValue.Nil;
            }

            switch (co.Coroutine.State) {
                case CoroutineState.Suspended:
                case CoroutineState.ForceSuspended:
                    ScriptManager.Instance.Resolve<ILogger>().LogTrace($"Running coroutine ({Name}): {co}");
                    if (Coroutines.ContainsKey(co)) {
                        Coroutines[co] = result;
                    }
                    else {
                        Coroutines.Add(co, result);
                    }
                    break;
                case CoroutineState.Dead:
                    ScriptManager.Instance.Resolve<ILogger>().LogTrace($"Finished running coroutine ({Name}): {co}");
                    if (Coroutines.TryGetValue(co, out var lastRes)) {
                        Coroutines.Remove(co);
                        if (CoroutineTasks.TryGetValue(co, out var task)) {
                            task.SetResult(lastRes);
                        }
                    }
                    else if (CoroutineTasks.TryGetValue(co, out var task)) {
                        task.SetResult(DynValue.Nil);
                    }
                    break;
            }

            return result;
        }

        public DynValue RunFile(string filename) {
            return RunWithCatch(() => {
                var scriptPath = Path.Combine(Directory, filename);

                DynValue func = WattleScript.LoadFile(scriptPath);
                var co = WattleScript.CreateCoroutine(func);
                return AddAndRunCoroutine(co);
            });
        }

        public DynValue RunText(string scriptCode) {
            return RunWithCatch(() => {
                DynValue func = WattleScript.DoString(scriptCode);
                var co = WattleScript.CreateCoroutine(func);
                return AddAndRunCoroutine(co);
            });
        }

        public Task<DynValue> RunFileAsync(string filename) {
            var tcs = new TaskCompletionSource<DynValue>();
            DynValue result = DynValue.Nil;
            try {
                var scriptPath = Path.Combine(Directory, filename);

                DynValue func = WattleScript.LoadFile(scriptPath);
                var co = WattleScript.CreateCoroutine(func);
                CoroutineTasks.Add(co, tcs);
                result = AddAndRunCoroutine(co);
            }
            catch (ScriptRuntimeException ex2) {
                Log($"An error occured running script {Name}: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}", LogLevel.Error);
                tcs.SetException(ex2);
            }
            catch (SyntaxErrorException ex3) {
                Log($"A syntax error occured running script {Name}: {ex3.DecoratedMessage}", LogLevel.Error);
                tcs.SetException(ex3);
            }
            catch (Exception arg) {
                Log($"Error running  script {Name}: {arg}", LogLevel.Error);
                tcs.SetException(arg);
            }

            return tcs.Task;
        }

        public Task<DynValue> RunTextAsync(string scriptCode) {
            var tcs = new TaskCompletionSource<DynValue>();
            DynValue result = DynValue.Nil;
            try {
                DynValue func = WattleScript.DoString(scriptCode);
                if (func.Type == DataType.Function) {
                    var co = WattleScript.CreateCoroutine(func);
                    CoroutineTasks.Add(co, tcs);
                    result = AddAndRunCoroutine(co);
                }
                else {
                    tcs.SetResult(func);
                }
            }
            catch (ScriptRuntimeException ex2) {
                Log($"An error occured running script {Name}: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}", LogLevel.Error);
                tcs.SetException(ex2);
            }
            catch (SyntaxErrorException ex3) {
                Log($"A syntax error occured running script {Name}: {ex3.DecoratedMessage}", LogLevel.Error);
                tcs.SetException(ex3);
            }
            catch (Exception arg) {
                Log($"Error running  script {Name}: {arg}", LogLevel.Error);
                tcs.SetException(arg);
            }

            return tcs.Task;
        }

        public object RunTextNoCatch(string scriptCode) {
            return FixResult(WattleScript.DoString(scriptCode));
        }

        private object FixResult(DynValue result) {
            if (result.IsNil()) {
                return null;
            }

            switch (result.Type) {
                case DataType.Boolean:
                    return result.Boolean;
                case DataType.ClrFunction:
                    return result.Callback;
                case DataType.Function:
                    return result.Function;
                case DataType.Nil:
                    return null;
                case DataType.Number:
                    return result.Number;
                case DataType.String:
                    return result.String;
                case DataType.Table:
                    return result.Table;
                case DataType.TailCallRequest:
                    return result.TailCallData;
                case DataType.Thread:
                    return result.Coroutine;
                case DataType.Tuple:
                    return result.Tuple.Select(r => FixResult(r)).ToList();
                case DataType.UserData:
                    return result.UserData.Object;
                case DataType.Void:
                    return null;
                case DataType.YieldRequest:
                    return result.YieldRequest;
                default:
                    return result;
            }
        }

        public void Dispose() {
            try {
                CurrentState = State.Stopped;
                WattleScript.Options.InstructionLimit = 1;
                Context.Dispose();
            }
            catch (Exception ex) { ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString()); }
        }

        internal void ThrowError(string text, Exception ex) {
            if (ex is ScriptRuntimeException scriptRuntimeEx) {
                Log($"{text}: {scriptRuntimeEx.DecoratedMessage}", LogLevel.Error);
            }
            else if (ex is SyntaxErrorException syntaxErrorEx) {
                Log($"{text}: {syntaxErrorEx.DecoratedMessage}", LogLevel.Error);
            }
            else {
                Log($"{text}: {ex.Message}", LogLevel.Error);
            }
        }
    }
}
