﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectDropAction : QueueAction {
        /// <summary>
        /// The id of the object that was dropped
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Inventory;

        public ObjectDropAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);

            // item must exist
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // item must be in inventory / equipped
            if (!(Manager.GameState.Character.Weenie.AllItemIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.EquipmentIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.ContainerIds.Contains(ObjectId))) {
                SetPermanentResult(ActionError.SourceItemNotInInventory);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_ServerSaysMoveItem += Incoming_Item_ServerSaysMoveItem;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().DropObject(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_ServerSaysMoveItem -= Incoming_Item_ServerSaysMoveItem;
        }

        private void Incoming_Item_ServerSaysMoveItem(object sender, UtilityBelt.Common.Messages.Events.Item_ServerSaysMoveItem_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
