﻿using System;
using System.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Actions {
    public class CastEquippedWandSpellAction : QueueAction {
        private bool gotError = false;
        /// <summary>
        /// The target object id that was attempted being cast on
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.CastSpell;

        public CastEquippedWandSpellAction(uint targetId = 0, ActionOptions options = null) : base(options) {
            TargetId = targetId;
        }

        protected override void UpdateDefaultOptions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_WeenieError += Incoming_Communication_WeenieError;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // make sure the equipped wand has a spell
            var wand = Manager.GameState.Character.Weenie.Equipment.FirstOrDefault((wo) => {
                return (EquipMask)wo.Value(IntId.CurrentWieldedLocation) == EquipMask.Wand;
            });

            if (wand == null || wand.SpellId == 0) {
                SetPermanentResult(ActionError.InvalidSourceObject, "You must have a wand with a useable spell equipped.");
                return false;
            }

            // todo: make sure specified target (if applicable) is a player and matches our pk status?

            return true;
        }

        protected override void UpdatePreconditions() {
            // must be in magic mode
            if (Manager.GameState.Character.Weenie.CombatMode != CombatMode.Magic) {
                Preconditions.Enqueue(new SetCombatModeAction(CombatMode.Magic));
                return;
            }
        }

        protected override bool Execute() {
            gotError = false;
            Manager.Resolve<IClientActionsRaw>().CastEquippedWandSpell(TargetId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_WeenieError -= Incoming_Communication_WeenieError;
        }

        private void Incoming_Communication_WeenieError(object sender, UtilityBelt.Common.Messages.Events.Communication_WeenieError_S2C_EventArgs e) {
            SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
            gotError = true;
        }

        private void Incoming_Item_UseDone(object sender, UtilityBelt.Common.Messages.Events.Item_UseDone_S2C_EventArgs e) {
            if (gotError)
                return;
            switch (e.Data.Type) {
                case StatusMessage.None:
                    SetPermanentResult(ActionError.None);
                    break;

                case StatusMessage.YourSpellFizzled:
                    SetTemporaryResult(ActionError.SpellFizzled);
                    break;

                default:
                    SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
                    break;
            }
        }
    }
}
