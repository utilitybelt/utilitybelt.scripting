﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

// /ub lexec return game.actions.castSpell(2, game.characterId).finally(function(res) print(tostring(res)) end)
// /ub lexec return game.actions.castSpell(4325, game.characterId).finally(function(res) print(tostring(res)) end)

namespace UtilityBelt.Scripting.Actions {
    public  class CastSpellAction : QueueAction {
        private bool gotError = false;

        /// <summary>
        /// The spell id that was attempted being cast
        /// </summary>
        public uint SpellId { get; }

        /// <summary>
        /// The target object id that was attempted being cast on
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType {
            get {
                switch (Manager.GameState.Character?.SpellBook?[SpellId]?.Category) {
                    case SpellCategory.HealthRestoring:
                        return Enums.ActionType.RestoreHealth;
                    case SpellCategory.ManaRestoring:
                        return Enums.ActionType.RestoreMana;
                    case SpellCategory.StaminaRestoring:
                    case SpellCategory.StaminaRestoring2:
                        return Enums.ActionType.RestoreStamina;
                    default:
                        return Enums.ActionType.CastSpell;
                }
            }
        }

        public CastSpellAction(uint spellId, uint targetId = 0, ActionOptions options = null) : base(options) {
            SpellId = spellId;
            TargetId = targetId;
        }

        protected override void UpdateDefaultOptions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_WeenieError += Incoming_Communication_WeenieError;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // make sure this spell is valid // known // we have components
            if (Manager.GameState.Character.SpellBook.TryGet(SpellId, out Spell spell)) {
                if (!spell.IsKnown()) {
                    SetPermanentResult(ActionError.YouDontKnowThatSpell);
                    return false;
                }
                if (!spell.HasComponents(out IEnumerable<SpellComponent> missingComponents)) {
                    SetPermanentResult(ActionError.MissingComponents, $"Missing Components: {string.Join(", ", missingComponents.Select(c => c.Name).ToArray())}");
                    return false;
                }

                // todo: skill check?
            }
            else {
                SetPermanentResult(ActionError.UnknownSpellId);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            // must be in magic mode
            if (Manager.GameState.Character.Weenie.CombatMode != CombatMode.Magic) {
                Preconditions.Enqueue(new SetCombatModeAction(CombatMode.Magic));
                return;
            }
        }

        protected override bool Execute() {
            gotError = false;
            Manager.Resolve<IClientActionsRaw>().CastSpell(SpellId, TargetId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_WeenieError -= Incoming_Communication_WeenieError;
        }

        private void Incoming_Communication_WeenieError(object sender, UtilityBelt.Common.Messages.Events.Communication_WeenieError_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.YourSpellFizzled:
                    SetTemporaryResult(ActionError.SpellFizzled);
                    gotError = true;
                    break;

                default:
                    SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
                    gotError = true;
                    break;
            }
        }

        private void Incoming_Item_UseDone(object sender, UtilityBelt.Common.Messages.Events.Item_UseDone_S2C_EventArgs e) {
            if (gotError)
                return;
            switch (e.Data.Type) {
                case StatusMessage.None:
                    SetPermanentResult(ActionError.None);
                    break;

                case StatusMessage.YourSpellFizzled:
                    SetTemporaryResult(ActionError.SpellFizzled);
                    break;

                default:
                    SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
                    break;
            }
        }
    }
}
