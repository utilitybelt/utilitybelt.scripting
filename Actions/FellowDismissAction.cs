﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class FellowDismissAction : QueueAction {
        /// <summary>
        /// The objectId of the member you tried to dismiss
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Fellow;

        public FellowDismissAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // check if we are in a fellowship
            if (!Manager.GameState.Character.Fellowship.Exists) {
                SetPermanentResult(ActionError.NotInFellow);
                return false;
            }

            // check if we are the fellowship leader
            if (Manager.GameState.Character.Fellowship.LeaderId != Manager.GameState.CharacterId) {
                SetPermanentResult(ActionError.NotTheLeader);
                return false;
            }

            // check if the fellow member exists
            if (!Manager.GameState.Character.Fellowship.Members.TryGetValue(ObjectId, out FellowshipMember member)) {
                SetPermanentResult(ActionError.MemberDoesntExist);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Fellowship_Dismiss += Incoming_Fellowship_Dismiss;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().FellowshipDismiss(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Fellowship_Dismiss -= Incoming_Fellowship_Dismiss;
        }

        private void Incoming_Fellowship_Dismiss(object sender, UtilityBelt.Common.Messages.Events.Fellowship_Dismiss_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
