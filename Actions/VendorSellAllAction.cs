﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class VendorSellAllAction : QueueAction {
        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Vendor;

        public VendorSellAllAction(ActionOptions options = null) : base(options) {

        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have vendor open
            if (!Manager.GameState.WorldState.Vendor.IsOpen) {
                SetPermanentResult(ActionError.VendorNotOpen);
                return false;
            }

            // TODO: verify 

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {

        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().VendorSellAll();

            // TODO: verify this...
            SetPermanentResultAfter(TimeSpan.FromMilliseconds(500), ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
