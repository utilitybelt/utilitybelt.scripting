﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectInscribeAction : QueueAction {
        /// <summary>
        /// The id of the object being inscribed.
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The inscription to set, or a blank string if removing.
        /// </summary>
        public string Inscription { get; private set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public ObjectInscribeAction(uint objectId, string inscription, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            Inscription = inscription;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 100;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must be in inventory / equipped
            if (!(Manager.GameState.Character.Weenie.AllItemIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.EquipmentIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.ContainerIds.Contains(ObjectId))) {
                SetPermanentResult(ActionError.SourceItemNotInInventory);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {

        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().InscribeWeenie(ObjectId, Inscription);

            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
