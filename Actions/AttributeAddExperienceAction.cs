﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class AttributeAddExperienceAction : QueueAction {
        /// <summary>
        /// The attribute id that your are attempting to put experience into
        /// </summary>
        public AttributeId Attribute { get; }

        /// <summary>
        /// The amount of experience you were attempting to spend
        /// </summary>
        public uint ExperienceToSpend { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set, and the queue it will be entered into.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public AttributeAddExperienceAction(AttributeId attributeId, uint experienceToSpend, ActionOptions options = null) : base(options) {
            Attribute = attributeId;
            ExperienceToSpend = experienceToSpend;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var attribute = Manager.GameState.Character.Weenie.Attributes[Attribute];
            var attributeXPTable = Manager.Resolve<PortalDatDatabase>().XpTable.AttributeXpList;

            // make sure we have enough unassigned experience
            if (ExperienceToSpend > Manager.GameState.Character.Weenie.Value(Int64Id.AvailableExperience)) {
                SetPermanentResult(ActionError.NotEnoughUnassignedExperience);
                return false;
            }

            // make sure this attribute is not maxed out
            if (attribute.PointsRaised >= attributeXPTable.Count) {
                SetPermanentResult(ActionError.AlreadyMaxed);
                return false;
            }

            // make sure we aren't trying to spend more exp than the attribute can take
            var experienceLeft = attributeXPTable[attributeXPTable.Count - 1] - attribute.Experience;
            if (ExperienceToSpend > experienceLeft) {
                SetPermanentResult(ActionError.TooMuchSpendExperience);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute += Incoming_Qualities_PrivateUpdateAttribute;
            Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().AttributeAddExperience(Attribute, ExperienceToSpend);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute -= Incoming_Qualities_PrivateUpdateAttribute;
            Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
        }

        private void Incoming_Communication_TextboxString(object sender, Communication_TextboxString_S2C_EventArgs e) {
            if (e.Data.Type == ChatMessageType.Default && Regex.IsMatch(e.Data.Text, $"^Your attempt to raise .* has failed")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
            }
        }

        private void Incoming_Qualities_PrivateUpdateAttribute(object sender, Qualities_PrivateUpdateAttribute_S2C_EventArgs e) {
            if (e.Data.Key == Attribute) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
