﻿using ACE.Entity;
using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using static System.Collections.Specialized.BitVector32;

namespace UtilityBelt.Scripting.Actions {
    public class VendorBuyAction : QueueAction {
        private bool _addedActionsToPreconditions;

        /// <summary>
        /// The id of the object that was being added to the vendor buy list
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The amount of items to add
        /// </summary>
        public uint Amount { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Vendor;

        public VendorBuyAction(uint objectId, uint amount = 1, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            Amount = amount;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have vendor open
            if (!Manager.GameState.WorldState.Vendor.IsOpen) {
                SetPermanentResult(ActionError.VendorNotOpen);
                return false;
            }

            // vendor must have this item
            if (!Manager.GameState.WorldState.Vendor.Items.Any(i => i.ObjectID == ObjectId)) {
                SetPermanentResult(ActionError.VendorDoesntHaveThisItem);
                return false;
            }

            // vendor must have enough of this item
            if (!Manager.GameState.WorldState.Vendor.Items.Any(i => i.ObjectID == ObjectId && i.Amount >= Amount)) {
                SetPermanentResult(ActionError.VendorDoesntHaveEnoughOfThisItem);
                return false;
            }
            return true;
        }

        protected override void UpdatePreconditions() {
            if (!_addedActionsToPreconditions) {
                Preconditions.Enqueue(new VendorClearBuyListAction());
                Preconditions.Enqueue(new VendorAddToBuyListAction(ObjectId, Amount));
                Preconditions.Enqueue(new VendorBuyAllAction());
                _addedActionsToPreconditions = true;
            }
        }

        protected override void Start() {

        }

        protected override bool Execute() {
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
