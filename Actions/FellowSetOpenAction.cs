﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class FellowSetOpenAction : QueueAction {
        /// <summary>
        /// The openness you attempted to set on the fellow
        /// </summary>
        public bool Open { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Fellow;

        public FellowSetOpenAction(bool open, ActionOptions options = null) : base(options) {
            Open = open;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // check if we are in a fellowship
            if (!Manager.GameState.Character.Fellowship.Exists) {
                SetPermanentResult(ActionError.NotInFellow);
                return false;
            }

            // check if we are the fellowship leader
            if (Manager.GameState.Character.Fellowship.LeaderId != Manager.GameState.CharacterId) {
                SetPermanentResult(ActionError.NotTheLeader);
                return false;
            }

            // check if fellowship is locked
            if (Manager.GameState.Character.Fellowship.Locked) {
                SetPermanentResult(ActionError.FellowshipLocked);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString += Incoming_Communication_WeenieErrorWithString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().FellowshipSetOpen(Open);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString -= Incoming_Communication_WeenieErrorWithString;
        }

        private void Incoming_Communication_WeenieErrorWithString(object sender, UtilityBelt.Common.Messages.Events.Communication_WeenieErrorWithString_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.IsNowOpenFellowship:
                    if (Open) {
                        SetPermanentResult(ActionError.None);
                    }
                    break;
                case StatusMessage.IsNowClosedFellowship:
                    if (!Open) {
                        SetPermanentResult(ActionError.None);
                    }
                    break;
            }
        }
    }
}
