﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectSelectAction : QueueAction {
        /// <summary>
        /// The id of the selected object
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public ObjectSelectAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 100;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must exist
            if (!Manager.GameState.WorldState.WeenieExists(ObjectId)) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {

        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SelectWeenie(ObjectId);

            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
