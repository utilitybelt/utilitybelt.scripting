﻿using System.Linq;
using System;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Lib;
using ACE.Entity;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Common.Messages.Events;

namespace UtilityBelt.Scripting.Actions {
    public class SetCombatModeAction : QueueAction {
        /// <summary>
        /// The combat mode being switch to
        /// </summary>
        public CombatMode CombatMode { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Combat;

        public SetCombatModeAction(CombatMode mode, ActionOptions options = null) : base(options) {
            CombatMode = mode;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            return true;
        }

        private void TryEquipAnyWeapon(ObjectType objectType) {
            var equipped = Manager.GameState.Character.Weenie.Equipment.Where(w => w.ObjectType == objectType).FirstOrDefault();
            if (equipped == null) {
                // todo: make sure we can equip this item...
                var bestWeapon = Manager.GameState.Character.Weenie.AllItems.FirstOrDefault(w => w.ObjectType == objectType);
                // if we dont find a weapon, we pass 0 causing this precondition to fail (and this action to fail)
                Preconditions.Enqueue(new ObjectWieldAction(bestWeapon == null ? 0 : bestWeapon.Id));
            }
        }

        protected override void UpdatePreconditions() {
            switch (CombatMode) {
                case CombatMode.Magic:
                    TryEquipAnyWeapon(ObjectType.Caster);
                    break;
                case CombatMode.Missile:
                    TryEquipAnyWeapon(ObjectType.MissileWeapon);
                    break;
                case CombatMode.Melee:
                    TryEquipAnyWeapon(ObjectType.MeleeWeapon);
                    break;
            }
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Movement_SetObjectMovement += Incoming_Movement_SetObjectMovement;
        }

        protected override bool Execute() {
            // check if we are already in the requested combat mode
            if (Manager.GameState.Character.Weenie.CombatMode == CombatMode) {
                SetPermanentResult(ActionError.None);
                return false;
            }

            Manager.Resolve<IClientActionsRaw>().SetCombatMode(CombatMode);
            return true;
        }

        private void Incoming_Movement_SetObjectMovement(object sender, Movement_SetObjectMovement_S2C_EventArgs e) {
            if (e.Data.ObjectId == Manager.GameState.CharacterId) {
                bool confirmed = false;
                switch (e.Data.MovementData.Stance) {
                    case MotionStance.NonCombat:
                        confirmed = CombatMode == CombatMode.NonCombat;
                        break;
                    case MotionStance.HandCombat:
                    case MotionStance.SwordCombat:
                    case MotionStance.SwordShieldCombat:
                    case MotionStance.TwoHandedSwordCombat:
                    case MotionStance.TwoHandedStaffCombat:
                    case MotionStance.DualWieldCombat:
                        confirmed = CombatMode == CombatMode.Melee;
                        break;
                    case MotionStance.BowNoAmmo:
                    case MotionStance.BowCombat:
                    case MotionStance.CrossbowCombat:
                    case MotionStance.SlingCombat:
                    case MotionStance.ThrownShieldCombat:
                    case MotionStance.ThrownWeaponCombat:
                    case MotionStance.CrossBowNoAmmo:
                        confirmed = CombatMode == CombatMode.Missile;
                        break;
                    case MotionStance.Magic:
                        confirmed = CombatMode == CombatMode.Magic;
                        break;
                }

                if (confirmed) {
                    SetPermanentResult(ActionError.None);
                }
            }
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Movement_SetObjectMovement -= Incoming_Movement_SetObjectMovement;
        }
    }
}