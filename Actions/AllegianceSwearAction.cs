﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class AllegianceSwearAction : QueueAction {
        private const float MAX_DISTANCE = 60;

        /// <summary>
        /// The object id of the player to swear to
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Allegiance;

        public AllegianceSwearAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
            // 30 seconds because we need to wait for other person to accept popup, potentially
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 30000;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            if (Manager.GameState.Character.Allegiance.Exists) {
                SetPermanentResult(ActionError.AlreadyInAnAllegiance);
                return false;
            }

            var target = Manager.GameState.WorldState.GetWeenie(ObjectId);

            // target must exist
            if (target == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target must be within range
            if (Manager.GameState.Character.Weenie.DistanceTo3D(target) > MAX_DISTANCE) {
                SetPermanentResult(ActionError.SourceObjectTooFar);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate += Incoming_Allegiance_AllegianceUpdate;
        }

        private void Incoming_Allegiance_AllegianceUpdate(object sender, UtilityBelt.Common.Messages.Events.Allegiance_AllegianceUpdate_S2C_EventArgs e) {
            if (e.Data.Profile?.Hierarchy?.Records?.Any(r => r.AllegianceData.ObjectId == Manager.GameState.CharacterId && r.TreeParent == ObjectId) == true) {
                SetPermanentResult(ActionError.None);
            }
        }

        protected override bool Execute() {
            var target = Manager.GameState.WorldState.GetWeenie(ObjectId);

            // target must exist
            if (target == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target must be within range
            if (Manager.GameState.Character.Weenie.DistanceTo3D(target) > MAX_DISTANCE) {
                SetTemporaryResult(ActionError.SourceObjectTooFar);
                return false;
            }

            Manager.Resolve<IClientActionsRaw>().AllegianceSwear(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate -= Incoming_Allegiance_AllegianceUpdate;
        }
    }
}
