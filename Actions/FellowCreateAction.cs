﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class FellowCreateAction : QueueAction {
        /// <summary>
        /// The name of the fellowship you were attempting to create
        /// </summary>
        public string FellowshipName { get; }

        /// <summary>
        /// Wether or not experience was set to be shared
        /// </summary>
        public bool ShareExperience { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Fellow;

        public FellowCreateAction(string name, bool shareExperience = true, ActionOptions options = null) : base(options) {
            ShareExperience = shareExperience;
            FellowshipName = name;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // check if we are already in a fellowship
            if (Manager.GameState.Character.Fellowship.Exists) {
                SetPermanentResult(ActionError.AlreadyInFellow);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Fellowship_FullUpdate += Incoming_Fellowship_FullUpdate;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().FellowshipCreate(FellowshipName, ShareExperience);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Fellowship_FullUpdate -= Incoming_Fellowship_FullUpdate;
        }

        private void Incoming_Fellowship_FullUpdate(object sender, UtilityBelt.Common.Messages.Events.Fellowship_FullUpdate_S2C_EventArgs e) {
            if (e.Data.Fellowship.Name.Equals(FellowshipName)) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
