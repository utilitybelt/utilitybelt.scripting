﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using static System.Net.Mime.MediaTypeNames;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectGiveAction : QueueAction {
        private bool _didHandToNPC = false;

        /// <summary>
        /// The id of the item attempting to be given
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The id of the target that the item is being given to
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Inventory;

        public ObjectGiveAction(uint objectId, uint targetId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            TargetId = targetId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must exist
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target must exist
            var target = Manager.GameState.WorldState.GetWeenie(TargetId);
            if (target == null) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }

            // item must be in inventory / equipped
            if (!(Manager.GameState.Character.Weenie.AllItemIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.EquipmentIds.Contains(ObjectId) ||
                Manager.GameState.Character.Weenie.ContainerIds.Contains(ObjectId))) {
                SetPermanentResult(ActionError.SourceItemNotInInventory);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_ServerSaysContainID += Incoming_Item_ServerSaysContainID;
            Manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed += Incoming_Character_ServerSaysAttemptFailed;
            Manager.GameState.WorldState.OnChatText += WorldState_OnChatText;
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString += Incoming_Communication_WeenieErrorWithString;
            Manager.MessageHandler.Incoming.Communication_WeenieError += Incoming_Communication_WeenieError;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().GiveWeenie(ObjectId, TargetId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_ServerSaysContainID -= Incoming_Item_ServerSaysContainID;
            Manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed -= Incoming_Character_ServerSaysAttemptFailed;
            Manager.GameState.WorldState.OnChatText -= WorldState_OnChatText;
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString -= Incoming_Communication_WeenieErrorWithString;
            Manager.MessageHandler.Incoming.Communication_WeenieError -= Incoming_Communication_WeenieError;
        }

        private void Incoming_Communication_WeenieError(object sender, Communication_WeenieError_S2C_EventArgs e) {
            if (e.Data.Type == Common.Enums.StatusMessage.YoureTooBusy) {
                SetTemporaryResult(ActionError.TooBusy);
            }
        }

        private void Incoming_Communication_WeenieErrorWithString(object sender, Communication_WeenieErrorWithString_S2C_EventArgs e) {
            if (e.Data.Type == Common.Enums.StatusMessage.TradeAiDoesntWant) {
                SetPermanentResult(ActionError.NPCDoesntKnowWhatToDoWithThat);
            }
        }

        private void WorldState_OnChatText(object sender, ChatEventArgs e) {
            // this is used for some npcs, like rare exchanger
            if (Manager.GameState.WorldState?.TryGetWeenie(TargetId, out var target) == true && target.ObjectClass == ObjectClass.Npc) {
                if (e.Message.StartsWith("You hand over ") || e.Message.StartsWith("You allow ") || e.Message.StartsWith("You give ")) {
                    _didHandToNPC = true;
                }
            }
        }

        private void Incoming_Character_ServerSaysAttemptFailed(object sender, Character_ServerSaysAttemptFailed_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                if (_didHandToNPC) {
                    SetPermanentResult(ActionError.None);
                }
                else {
                    SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Reason}");
                }
            }
        }

        private void Incoming_Item_ServerSaysContainID(object sender, Item_ServerSaysContainID_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId && e.Data.ContainerId == TargetId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
