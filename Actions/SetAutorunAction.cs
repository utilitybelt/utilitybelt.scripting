﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SetAutorunAction : QueueAction {
        /// <summary>
        /// Wether autorun was set to enabled or not
        /// </summary>
        public bool Enabled { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Navigation;

        public SetAutorunAction(bool enabled, ActionOptions options = null) : base(options) {
            Enabled = enabled;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Movement_SetObjectMovement += Incoming_Movement_SetObjectMovement;
            Manager.MessageHandler.Incoming.Movement_PositionAndMovementEvent += Incoming_Movement_PositionAndMovementEvent;
        }

        protected override bool Execute() {
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Movement_SetObjectMovement -= Incoming_Movement_SetObjectMovement;
            Manager.MessageHandler.Incoming.Movement_PositionAndMovementEvent -= Incoming_Movement_PositionAndMovementEvent;
        }

        private void Incoming_Movement_PositionAndMovementEvent(object sender, UtilityBelt.Common.Messages.Events.Movement_PositionAndMovementEvent_S2C_EventArgs e) {
            if (e.Data.ObjectId == Manager.GameState.CharacterId && e.Data.MovementData.State.ForwardCommand == MotionCommand.RunForward) {
                SetPermanentResult(ActionError.None);
            }
        }

        private void Incoming_Movement_SetObjectMovement(object sender, UtilityBelt.Common.Messages.Events.Movement_SetObjectMovement_S2C_EventArgs e) {
            if (e.Data.ObjectId == Manager.GameState.CharacterId && e.Data.MovementData.State.ForwardCommand == MotionCommand.RunForward) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
