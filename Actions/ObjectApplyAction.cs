﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectApplyAction : QueueAction {
        /// <summary>
        /// The id of the item that was used.
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The id of the item that was the target.
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Inventory;

        public ObjectApplyAction(uint objectId, uint targetId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            TargetId = targetId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // can't use item on itself
            if (ObjectId == TargetId) {
                SetPermanentResult(ActionError.CantUseOnItself);
                return false;
            }

            // source item must exist
            var sourceWeenie = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (sourceWeenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target item must exist
            var targetWeenie = Manager.GameState.WorldState.GetWeenie(TargetId);
            if (targetWeenie == null) {
                SetPermanentResult(ActionError.InvalidTargetItem);
                return false;
            }

            // todo: check item use types so we can short circuit before we call to server if they are not compatible

            var inventoryIds = Manager.GameState.Character.Weenie.AllItemIds;

            // sourceItem must be in inventory
            var foundSourceInInventory = inventoryIds.Contains(ObjectId);

            if (!foundSourceInInventory) {
                SetPermanentResult(ActionError.SourceItemNotInInventory);
                return false;
            }

            // target must be in inventory / landscape
            bool foundTargetInInventoryOrLandscape = inventoryIds.Contains(TargetId) || Manager.GameState.WorldState.LandscapeWeenies.Any(w => w.Id == TargetId);

            if (!foundTargetInInventoryOrLandscape) {
                SetPermanentResult(ActionError.TargetItemNotInInventoryOrLandscape);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
            Manager.MessageHandler.Incoming.Communication_TransientString += Incoming_Communication_TransientString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().ApplyWeenie(ObjectId, TargetId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
            Manager.MessageHandler.Incoming.Communication_TransientString -= Incoming_Communication_TransientString;
        }

        private void Incoming_Communication_TransientString(object sender, UtilityBelt.Common.Messages.Events.Communication_TransientString_S2C_EventArgs e) {
            if (Regex.IsMatch(e.Data.Message, "Cannot use the .* with the .*")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Message}");
            }
        }

        private void Incoming_Communication_TextboxString(object sender, UtilityBelt.Common.Messages.Events.Communication_TextboxString_S2C_EventArgs e) {
            if (e.Data.Type == ChatMessageType.Default && Regex.IsMatch(e.Data.Text, $"You must (wield|contain) the .* to use it\\.")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
            }
            else if (e.Data.Type == ChatMessageType.Craft && Regex.IsMatch(e.Data.Text, $"The .* cannot be combined with itself\\.")) {
                SetPermanentResult(ActionError.CantUseOnItself, $"Server said: {e.Data.Text}");
            }
            else if (e.Data.Type == ChatMessageType.Craft && Regex.IsMatch(e.Data.Text, $"The .* cannot be used on the .*\\.")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
            }
        }

        private void Incoming_Item_UseDone(object sender, UtilityBelt.Common.Messages.Events.Item_UseDone_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.None:
                    SetPermanentResult(ActionError.None);
                    break;

                case StatusMessage.YouHaveBeenInPKBattleTooRecently:
                    SetTemporaryResult(ActionError.YouHaveBeenInPKBattleTooRecently);
                    break;

                case StatusMessage.TradeItemBeingTraded:
                    SetPermanentResult(ActionError.ItemBeingTraded);
                    break;

                case StatusMessage.YouDoNotPassCraftingRequirements:
                    SetPermanentResult(ActionError.YouDoNotPassCraftingRequirements);
                    break;

                default:
                    SetTemporaryResult(ActionError.ServerError);
                    break;
            }
        }
    }
}
