﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectAppraiseAction : QueueAction {
        /// <summary>
        /// The objectId being assessed
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public ObjectAppraiseAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 10000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 1;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);

            // item must exist
            if (weenie == null && !Manager.GameState.WorldState.Vendor.Items.Where(i => i.ObjectID == ObjectId).Any()) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_SetAppraiseInfo += Incoming_Item_SetAppraiseInfo;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().Appraise(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_SetAppraiseInfo -= Incoming_Item_SetAppraiseInfo;
        }

        private void Incoming_Item_SetAppraiseInfo(object sender, UtilityBelt.Common.Messages.Events.Item_SetAppraiseInfo_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                if (e.Data.Success) {
                    SetPermanentResult(ActionError.None);
                }
                else {
                    SetPermanentResult(ActionError.ServerError);
                }
            }
        }
    }
}
