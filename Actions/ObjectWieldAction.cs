﻿using ACE.Entity.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectWieldAction : QueueAction {
        /// <summary>
        /// The id of the object to wield
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The slot you are attempting to wield this to. Leave as EquipMask.None for auto
        /// </summary>
        public EquipMask Slot { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Wield;

        public ObjectWieldAction(uint objectId, EquipMask slot = EquipMask.None, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            Slot = slot;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);

            // object must exist
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // objectId must be in inventory or landscape
            bool foundInInventory = Manager.GameState.Character.Weenie.AllItemIds.Contains(ObjectId);
            bool foundInLandscape = Manager.GameState.WorldState.LandscapeWeenies.Any(w => w.Id == ObjectId);

            if (!foundInInventory && !foundInLandscape) {
                SetPermanentResult(ActionError.SourceItemNotInInventoryOrLandscape);
                return false;
            }

            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            bool foundInLandscape = false; //  Manager.GameState.WorldState.LandscapeWeenies.Any(w => w.Id == ObjectId);

            // must be in peace mode if object is in landscape
            if (foundInLandscape) {
                Preconditions.Enqueue(new SetCombatModeAction(CombatMode.NonCombat));
                return;
            }
        }

        protected override bool Execute() {
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);

            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }


            // check if item is already wielded
            if (weenie.Value(IntId.CurrentWieldedLocation) != 0) {
                SetPermanentResult(ActionError.ItemAlreadyWielded);
                return false;
            }

            if (weenie.ObjectType == ObjectType.Caster || weenie.ObjectType == ObjectType.MeleeWeapon || weenie.ObjectType == ObjectType.MissileWeapon) {
                Manager.Resolve<IClientActionsRaw>().UseWeenie(ObjectId);
            }
            else {
                EquipMask slot = 0;
                if (Slot == EquipMask.None) {
                    // find correct auto slot
                    if (weenie.ValidWieldedLocations.HasFlag(EquipMask.LeftBracelet)) {
                        if (Manager.GameState.Character.Weenie.Equipment.Any(e => e.Value(IntId.CurrentWieldedLocation) == (int)EquipMask.LeftBracelet)) {
                            slot = EquipMask.RightBracelet;
                        }
                        else if (Manager.GameState.Character.Weenie.Equipment.Any(e => e.Value(IntId.CurrentWieldedLocation) == (int)EquipMask.RightBracelet)) {
                            slot = EquipMask.LeftBracelet;
                        }
                    }
                    else if (weenie.ValidWieldedLocations.HasFlag(EquipMask.LeftRing)) {
                        if (Manager.GameState.Character.Weenie.Equipment.Any(e => e.Value(IntId.CurrentWieldedLocation) == (int)EquipMask.LeftRing)) {
                            slot = EquipMask.RightRing;
                        }
                        else if (Manager.GameState.Character.Weenie.Equipment.Any(e => e.Value(IntId.CurrentWieldedLocation) == (int)EquipMask.RightRing)) {
                            slot = EquipMask.LeftRing;
                        }
                    }
                }
                else {
                    slot = Slot;
                }

                if (slot == EquipMask.None) {
                    slot = weenie.ValidWieldedLocations;
                    EquipMask slots = weenie.ValidWieldedLocations;
                    foreach (var e in Enum.GetValues(typeof(EquipMask)).Cast<EquipMask>()) {
                        if (e == EquipMask.None)
                            continue;
                        if (slots.HasFlag(e)) {
                            slot = e;
                            break;
                        }
                    }
                }

                Manager.Resolve<IClientActionsRaw>().AutoWield(ObjectId, slot);
            }
            return true;
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_TransientString += Incoming_Communication_TransientString; ;
            Manager.MessageHandler.Incoming.Item_WearItem += Incoming_Item_WearItem;
        }

        private void Incoming_Item_WearItem(object sender, UtilityBelt.Common.Messages.Events.Item_WearItem_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResultAfter(TimeSpan.FromMilliseconds(50), ActionError.None);
            }
        }

        private void Incoming_Communication_TransientString(object sender, UtilityBelt.Common.Messages.Events.Communication_TransientString_S2C_EventArgs e) {
            if (e.Data.Message.Equals("Item not found!")) {
                SetPermanentResult(ActionError.InvalidSourceObject, $"Server said: {e.Data.Message}");
            }
            else if (e.Data.Message.Equals("You are too encumbered to carry that!")) {
                SetPermanentResult(ActionError.TooEncumbered, $"Server said: {e.Data.Message}");
            }
            else if (e.Data.Message.Equals("Cannot pick that up and wield it while not at peace!")) {
                SetTemporaryResult(ActionError.MustBeInPeaceMode, $"Server said: {e.Data.Message}");
                Preconditions.Enqueue(new SetCombatModeAction(CombatMode.NonCombat));
            }
        }

        private void Incoming_Item_UseDone(object sender, UtilityBelt.Common.Messages.Events.Item_UseDone_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.None:
                    SetPermanentResult(ActionError.None);
                    break;

                case StatusMessage.Stuck:
                    SetPermanentResult(ActionError.YouCantPickThatUp);
                    break;

                case StatusMessage.InvalidInventoryLocation:
                    SetPermanentResult(ActionError.InvalidInventoryLocation);
                    break;

                default:
                    SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
                    break;
            }
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_TransientString -= Incoming_Communication_TransientString; ;
            Manager.MessageHandler.Incoming.Item_WearItem -= Incoming_Item_WearItem;
        }
    }
}
