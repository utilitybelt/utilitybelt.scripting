﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SalvageAddAction : QueueAction {
        /// <summary>
        /// The id of the object that was being added to the salvage panel
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public SalvageAddAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have ust
            var ust = Manager.GameState.Character.Weenie.AllItems.Where(w => w.ClassId == 20646).FirstOrDefault();
            if (ust == null) {
                SetPermanentResult(ActionError.NoUst);
                return false;
            }

            // item must exist in inventory
            var weenie = Manager.GameState.Character.Weenie.AllItems.Where(w => w.Id == ObjectId).FirstOrDefault();
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // item must be salvageable
            if (weenie.Material == 0) {
                SetPermanentResult(ActionError.UnsalvageableItem);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {

        }

        protected override bool Execute() {

            Manager.Resolve<IClientActionsRaw>().SalvagePanelAdd(ObjectId);

            // we dont really verify this, since its client side only...
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
