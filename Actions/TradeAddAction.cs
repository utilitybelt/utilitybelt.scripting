﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class TradeAddAction : QueueAction {
        /// <summary>
        /// The id of the object being added to the trade window
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Trade;

        public TradeAddAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must be trading
            if (!Manager.GameState.Character.Trade.IsOpen) {
                SetPermanentResult(ActionError.NoTradePartner);
                return false;
            }

            // item must exist
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Trade_AddToTrade += Incoming_Trade_AddToTrade;
            Manager.MessageHandler.Incoming.Trade_TradeFailure += Incoming_Trade_TradeFailure;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().TradeAdd(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Trade_AddToTrade -= Incoming_Trade_AddToTrade;
            Manager.MessageHandler.Incoming.Trade_TradeFailure -= Incoming_Trade_TradeFailure;
        }

        private void Incoming_Trade_TradeFailure(object sender, UtilityBelt.Common.Messages.Events.Trade_TradeFailure_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetTemporaryResult(ActionError.ServerError, $"Server said: {e.Data.Reason}");
            }
        }

        private void Incoming_Trade_AddToTrade(object sender, UtilityBelt.Common.Messages.Events.Trade_AddToTrade_S2C_EventArgs e) {
            if (e.Data.Side == UtilityBelt.Common.Enums.TradeSide.Self && e.Data.ObjectId == ObjectId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
