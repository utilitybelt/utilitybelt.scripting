﻿using ACE.Entity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Interface for performing client actions with async callbacks / promises. These actions do thier best to
    /// check for validity before sending the action to the server, and throwing specific error messages when 
    /// something goes wrong.
    /// </summary>
    public class ClientActions {
        private ILogger _log;
        private readonly ScriptManager _manager;

        internal ClientActions() {
            _log = ScriptManager.Instance.Resolve<ILogger>();
            _manager = ScriptManager.Instance;
        }

        public void Dispose() {

        }

        public T MakeAndEnqueuePromise<T>(T action, Action<T> callback, object[] args = null) where T : QueueAction {
            var promise = new Promise<T>(_log, _manager);
            promise.Name = MakePromiseDescription(promise, args);
            TiePromiseToActionAndEnqueue(promise, action, callback);

            return action;
        }

        internal bool EnqueueAction<T>(QueueAction action, Action<T> callback) {
            var isValid = action.Options.SkipChecks ? true : action.IsValid();

            if (isValid) {
                EventHandler<EventArgs> onFinished = null;

                onFinished = (s, e) => {
                    callback?.Invoke((T)Convert.ChangeType(action, typeof(T)));
                    action.OnFinished -= onFinished;
                };

                action.OnFinished += onFinished;

                _manager.GameState.ActionQueue.Add(action);
            }
            else {
                callback?.Invoke((T)Convert.ChangeType(action, typeof(T)));
            }

            return isValid;
        }

        internal void TiePromiseToActionAndEnqueue<T>(Promise<T> promise, QueueAction action, Action<T> callback) where T : QueueAction {
            EnqueueAction(action, (T res) => {
                try {
                    if (res.Success)
                        promise.Resolve(res);
                    else
                        promise.Reject(res);

                    if (callback != null)
                        callback(res);
                }
                catch (Exception ex) { _log.LogError(ex, "Unable to tie promise to action and enqueue."); }
            });
        }

        internal string MakePromiseDescription<T>(Promise<T> promise, object[] args) {
            var name = new StringBuilder();
            name.Append(typeof(T).Name);

            if (args != null) {
                foreach (var arg in args) {
                    if (arg == null) {
                        name.Append(":null");
                    }
                    else if (arg.GetType() == typeof(uint)) {
                        name.Append($":0x{arg:X8}");
                    }
                    else {
                        name.Append($":{arg}");
                    }
                }
            }

            return name.ToString();
        }

        /// <summary>
        /// Run a series of queue actions in order. This ignores the individual priorities and instead uses the priority of RunAllOrderedAction
        /// </summary>
        /// <param name="actions">the queue actions to run</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public RunAllOrderedAction RunAllOrdered(IEnumerable<QueueAction> actions, ActionOptions options, Action<RunAllOrderedAction> callback) {
            return MakeAndEnqueuePromise(new RunAllOrderedAction(actions, options), callback);
        }

        /// <summary>
        /// A blank action that always immediately returns success
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public BlankAction Blank(ActionOptions options = null, Action<BlankAction> callback = null) {
            return MakeAndEnqueuePromise(new BlankAction(options), callback);
        }

        /// <summary>
        /// Attempt to log in the specified character
        /// </summary>
        /// <param name="objectId">Character id to log in as</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public LoginAction Login(uint objectId, ActionOptions options = null, Action<LoginAction> callback = null) {
            return MakeAndEnqueuePromise(new LoginAction(objectId, options), callback);
        }

        /// <summary>
        /// Adds experience towards raising an attribute.
        /// </summary>
        /// <param name="attributeId">The attribute id to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public AttributeAddExperienceAction AttributeAddExperience(AttributeId attributeId, uint experienceToSpend, ActionOptions options = null, Action<AttributeAddExperienceAction> callback = null) {
            return MakeAndEnqueuePromise(new AttributeAddExperienceAction(attributeId, experienceToSpend, options), callback);
        }

        /// <summary>
        /// Adds experience towards raising a skill.
        /// </summary>
        /// <param name="skillId">The skill to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SkillAddExperienceAction SkillAddExperience(SkillId skillId, uint experienceToSpend, ActionOptions options = null, Action<SkillAddExperienceAction> callback = null) {
            return MakeAndEnqueuePromise(new SkillAddExperienceAction(skillId, experienceToSpend, options), callback);
        }

        /// <summary>
        /// Adds experience towards raising a vital.
        /// </summary>
        /// <param name="vitalId">The vital to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VitalAddExperienceAction VitalAddExperience(VitalId vitalId, uint experienceToSpend, ActionOptions options = null, Action<VitalAddExperienceAction> callback = null) {
            return MakeAndEnqueuePromise(new VitalAddExperienceAction(vitalId, experienceToSpend, options), callback);
        }

        /// <summary>
        /// Attempt to wield an object (weapons / armor). The item must be in your inventory or the landscape.
        /// </summary>
        /// <param name="objectId">the object id to wield</param>
        /// <param name="slot">The slot to place this item in. EquipMask.None for auto</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectWieldAction ObjectWield(uint objectId, EquipMask slot = EquipMask.None, ActionOptions options = null, Action<ObjectWieldAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectWieldAction(objectId, slot, options), callback);
        }

        /// <summary>
        /// Attempt to cast a spell by id. If this is an untargeted spell, set targetId to 0.
        /// </summary>
        /// <param name="spellId">SpellId to cast</param>
        /// <param name="targetId">TargetId to cast on (if none use 0)</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public CastSpellAction CastSpell(uint spellId, uint targetId = 0, ActionOptions options = null, Action<CastSpellAction> callback = null) {
            return MakeAndEnqueuePromise(new CastSpellAction(spellId, targetId, options), callback);
        }

        /// <summary>
        /// Try to switch to the specified combat mode. You must have the appropriate weapon type equipped first.
        /// </summary>
        /// <param name="combatMode">CombatMode to switch to</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SetCombatModeAction SetCombatMode(CombatMode combatMode, ActionOptions options = null, Action<SetCombatModeAction> callback = null) {
            return MakeAndEnqueuePromise(new SetCombatModeAction(combatMode, options), callback);
        }

        /// <summary>
        /// Try and drop the specified object id. It must be in your inventory.
        /// </summary>
        /// <param name="objectId">The object id to attempt to drop</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectDropAction ObjectDrop(uint objectId, ActionOptions options = null, Action<ObjectDropAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectDropAction(objectId, options), callback);
        }

        /// <summary>
        /// Attempt to create a fellowship.
        /// </summary>
        /// <param name="name">Name of the fellowship</param>
        /// <param name="shareExperience">Wether or not to share experience</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowCreateAction FellowCreate(string name, bool shareExperience = true, ActionOptions options = null, Action<FellowCreateAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowCreateAction(name, shareExperience, options), callback);
        }

        /// <summary>
        /// Attempt to disband your current fellowship. You must be the leader.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowDisbandAction FellowDisband(ActionOptions options = null, Action<FellowDisbandAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowDisbandAction(options), callback);
        }

        /// <summary>
        /// Attempt to dismiss a member from the fellowship. You must be the leader
        /// </summary>
        /// <param name="objectId">The object id of the member you want to dismiss</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowDismissAction FellowDismiss(uint objectId, ActionOptions options = null, Action<FellowDismissAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowDismissAction(objectId, options), callback);
        }

        /// <summary>
        /// Attempt to give leadership to another membership of the fellow. You must be the leader.
        /// </summary>
        /// <param name="objectId">The object id of the member you want to give leadership to.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowSetLeaderAction FellowSetLeader(uint objectId, ActionOptions options = null, Action<FellowSetLeaderAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowSetLeaderAction(objectId, options), callback);
        }

        /// <summary>
        /// Attempt to quit your current fellowship.
        /// </summary>
        /// <param name="disband">Wether or not to disband the fellowship while quitting. If you specify true you must be the leader</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowQuitAction FellowQuit(bool disband, ActionOptions options = null, Action<FellowQuitAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowQuitAction(disband, options), callback);
        }

        /// <summary>
        /// Attempt to recruit a member to the fellowship. The fellowship must be open (or you must be the leader).
        /// </summary>
        /// <param name="objectId">The object id of the player to recruit</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowRecruitAction FellowRecruit(uint objectId, ActionOptions options = null, Action<FellowRecruitAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowRecruitAction(objectId, options), callback);
        }

        /// <summary>
        /// Attempt to set the openness of the fellowship. Must be the leader.
        /// </summary>
        /// <param name="open">The openness to set the fellowship to</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public FellowSetOpenAction FellowSetOpen(bool open, ActionOptions options = null, Action<FellowSetOpenAction> callback = null) {
            return MakeAndEnqueuePromise(new FellowSetOpenAction(open, options), callback);
        }

        /// <summary>
        /// Attempt to give an object to another player/npc
        /// </summary>
        /// <param name="objectId">The object id of the item to give</param>
        /// <param name="targetId">The object id of the target</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectGiveAction ObjectGive(uint objectId, uint targetId, ActionOptions options = null, Action<ObjectGiveAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectGiveAction(objectId, targetId, options), callback);
        }

        /// <summary>
        /// Attempt to send a command to the chat window parser. (As if you were typing into the chat box in a client)
        /// </summary>
        /// <param name="text">The text to send to the chat parser</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public InvokeChatAction InvokeChat(string text, ActionOptions options = null, Action<InvokeChatAction> callback = null) {
            return MakeAndEnqueuePromise(new InvokeChatAction(text, options), callback);
        }

        /// <summary>
        /// Attempt to log out the currently logged in character
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public LogoutAction Logout(ActionOptions options = null, Action<LogoutAction> callback = null) {
            return MakeAndEnqueuePromise(new LogoutAction(options), callback);
        }

        /// <summary>
        /// Attempt to move a weenie to a weenie container.
        /// </summary>
        /// <param name="objectId">The id of the item to move</param>
        /// <param name="targetId">The id of the container to move the item to</param>
        /// <param name="slot">The slot in the container to place the item. Slot 0 is the first slot.</param>
        /// <param name="stack">Wether or not to attempt to merge a stacked item with an existing stack in the container</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectMoveAction ObjectMove(uint objectId, uint targetId, uint slot = 0, bool stack = true, ActionOptions options = null, Action<ObjectMoveAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectMoveAction(objectId, targetId, slot, stack, options), callback);
        }

        /// <summary>
        /// Attempt to split a stack into a smaller one
        /// </summary>
        /// <param name="objectId">The id of the item to move</param>
        /// <param name="targetId">The id of the container to move the item to</param>
        /// <param name="newStackSize">The new stack size</param>
        /// <param name="slot">The slot in the container to place the item. Slot 0 is the first slot.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectSplitAction ObjectSplit(uint objectId, uint targetId, uint newStackSize, uint slot = 0, ActionOptions options = null, Action<ObjectSplitAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectSplitAction(objectId, targetId, newStackSize, slot, options), callback);
        }

        /// <summary>
        /// Request assessment data for a n object
        /// </summary>
        /// <param name="objectId">The id of the object to request assessment data for</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectAppraiseAction ObjectAppraise(uint objectId, ActionOptions options = null, Action<ObjectAppraiseAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectAppraiseAction(objectId, options), callback);
        }

        /// <summary>
        /// Adds a salvageable weenie to the salvage panel.
        /// </summary>
        /// <param name="objectId">The id of the item to add to the salvage panel</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SalvageAddAction SalvageAdd(uint objectId, ActionOptions options = null, Action<SalvageAddAction> callback = null) {
            return MakeAndEnqueuePromise(new SalvageAddAction(objectId, options), callback);
        }

        /// <summary>
        /// Salvages the objects in the salvage panel
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SalvageAction Salvage(ActionOptions options = null, Action<SalvageAction> callback = null) {
            return MakeAndEnqueuePromise(new SalvageAction(options), callback);
        }

        /// <summary>
        /// Select a weenie. This call is client side, nothing gets sent to the server.
        /// </summary>
        /// <param name="objectId">The id of the object to select</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectSelectAction ObjectSelect(uint objectId, ActionOptions options = null, Action<ObjectSelectAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectSelectAction(objectId, options), callback);
        }

        /// <summary>
        /// Attempt to use the specified object. Optionally on a target. Set target to 0 to use an
        /// object by itself.
        /// </summary>
        /// <param name="objectId">The id of the object to use</param>
        /// <param name="targetId">The id of the object to use this object on</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ObjectUseAction ObjectUse(uint objectId, uint targetId = 0, ActionOptions options = null, Action<ObjectUseAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectUseAction(objectId, targetId, options), callback);
        }

        /// <summary>
        /// Enable / disable autorunning
        /// </summary>
        /// <param name="enabled">Wether to enable or not</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SetAutorunAction SetAutorun(bool enabled, ActionOptions options = null, Action<SetAutorunAction> callback = null) {
            return MakeAndEnqueuePromise(new SetAutorunAction(enabled, options), callback);
        }

        /// <summary>
        /// Mark the current trade as accepted.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public TradeAcceptAction TradeAccept(ActionOptions options = null, Action<TradeAcceptAction> callback = null) {
            return MakeAndEnqueuePromise(new TradeAcceptAction(options), callback);
        }

        /// <summary>
        /// Adds the specified object id to your side of the trade window
        /// </summary>
        /// <param name="objectId">The id of the object to add</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public TradeAddAction TradeAdd(uint objectId, ActionOptions options = null, Action<TradeAddAction> callback = null) {
            return MakeAndEnqueuePromise(new TradeAddAction(objectId, options), callback);
        }

        /// <summary>
        /// Decline the current trade. This does not end the trade, just removes acceptance.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public TradeDeclineAction TradeDecline(ActionOptions options = null, Action<TradeDeclineAction> callback = null) {
            return MakeAndEnqueuePromise(new TradeDeclineAction(options), callback);
        }

        /// <summary>
        /// Ends the current trade.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public TradeEndAction TradeEnd(ActionOptions options = null, Action<TradeEndAction> callback = null) {
            return MakeAndEnqueuePromise(new TradeEndAction(options), callback);
        }

        /// <summary>
        /// Reset the current trade window. Clears all trade items and acceptance on both sides.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public TradeResetAction TradeReset(ActionOptions options = null, Action<TradeResetAction> callback = null) {
            return MakeAndEnqueuePromise(new TradeResetAction(options), callback);
        }

        /// <summary>
        /// Advance skill training.
        /// </summary>
        /// <param name="skill">The skill to advance the training of</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SkillAdvanceAction SkillAdvance(SkillId skill, ActionOptions options = null, Action<SkillAdvanceAction> callback = null) {
            return MakeAndEnqueuePromise(new SkillAdvanceAction(skill, options), callback);
        }

        /// <summary>
        /// Swear allegiance to another player
        /// </summary>
        /// <param name="objectId">The object id of the player to swear to</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public AllegianceSwearAction AllegianceSwear(uint objectId, ActionOptions options = null, Action<AllegianceSwearAction> callback = null) {
            return MakeAndEnqueuePromise(new AllegianceSwearAction(objectId, options), callback);
        }

        /// <summary>
        /// Break allegiance from your current patron
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public AllegianceBreakAction AllegianceBreak(ActionOptions options = null, Action<AllegianceBreakAction> callback = null) {
            return MakeAndEnqueuePromise(new AllegianceBreakAction(options), callback);
        }

        /// <summary>
        /// Add a vendor object / template id to the buy list.
        /// </summary>
        /// <param name="objectId">The id of the object / template to add to the vendor buy list</param>
        /// <param name="amount">The amount to add</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorAddToBuyListAction VendorAddToBuyList(uint objectId, uint amount = 1, ActionOptions options = null, Action<VendorAddToBuyListAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorAddToBuyListAction(objectId, amount, options), callback);
        }

        /// <summary>
        /// Add an object to the vendor sell list.
        /// </summary>
        /// <param name="objectId">The id of the object to add to the vendor sell list</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorAddToSellListAction VendorAddToSellList(uint objectId, ActionOptions options = null, Action<VendorAddToSellListAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorAddToSellListAction(objectId, options), callback);
        }

        /// <summary>
        /// Buys all the items in the vendor buy list
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorBuyAllAction VendorBuyAll(ActionOptions options = null, Action<VendorBuyAllAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorBuyAllAction(options), callback);
        }

        /// <summary>
        /// Sells all the items in the vendor sell list
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorSellAllAction VendorSellAll(ActionOptions options = null, Action<VendorSellAllAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorSellAllAction(options), callback);
        }

        /// <summary>
        /// Clears out the vendor buy list
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorClearBuyListAction VendorClearBuyList(ActionOptions options = null, Action<VendorClearBuyListAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorClearBuyListAction(options), callback);
        }

        /// <summary>
        /// Clears out the vendor sell list
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public VendorClearSellListAction VendorClearSellList(ActionOptions options = null, Action<VendorClearSellListAction> callback = null) {
            return MakeAndEnqueuePromise(new VendorClearSellListAction(options), callback);
        }

        /// <summary>
        /// Send a tell to an object id. This is used for Spell Professors.
        /// </summary>
        /// <param name="objectId">The id of the object to send a tell to</param>
        /// <param name="message">The message to send</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SendTellByIdAction SendTellById(uint objectId, string message, ActionOptions options = null, Action<SendTellByIdAction> callback = null) {
            return MakeAndEnqueuePromise(new SendTellByIdAction(objectId, message, options), callback);
        }

        /// <summary>
        /// Sleep for the specified amount of milliseconds. There are 1000 milliseconds in 1 second. Timing will not be perfect.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep for.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public SleepAction Sleep(uint milliseconds, ActionOptions options = null, Action<SleepAction> callback = null) {
            return MakeAndEnqueuePromise(new SleepAction(milliseconds, options), callback);
        }

        /// <summary>
        /// Cast the spell on your current equipped wand, with an optional target.
        /// </summary>
        /// <param name="targetId">The id of the object to target, set to 0 if none.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns></returns>
        public CastEquippedWandSpellAction CastEquippedWandSpell(uint targetId = 0, ActionOptions options = null, Action<CastEquippedWandSpellAction> callback = null) {
            return MakeAndEnqueuePromise(new CastEquippedWandSpellAction(targetId, options), callback);
        }

        /// <summary>
        /// Inscribe the specified object. Leave inscription text blank to remove an inscription.
        /// </summary>
        /// <param name="objectId">The id of the object to inscribe</param>
        /// <param name="inscription">The text to inscribe, or empty if removing an inscription</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns></returns>
        public ObjectInscribeAction Inscribe(uint objectId, string inscription, ActionOptions options = null, Action<ObjectInscribeAction> callback = null) {
            return MakeAndEnqueuePromise(new ObjectInscribeAction(objectId, inscription, options), callback);
        }
    }
}
