﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about an allegiance member
    /// </summary>
    public class AllegianceMember {
        /// <summary>
        /// Object id of this allegiance member
        /// </summary>
        public uint Id { get; internal set; }

        /// <summary>
        /// Character name of this allegiance member
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// The object id of the parent of this allegiance member
        /// </summary>
        public uint ParentId { get; internal set; }

        /// <summary>
        /// The level of this character
        /// </summary>
        public uint Level { get; internal set; }

        /// <summary>
        /// The rank of this character
        /// </summary>
        public uint Rank { get; internal set; }

        /// <summary>
        /// The gender of this character
        /// </summary>
        public Gender Gender { get; internal set; }

        /// <summary>
        /// The heritage of this character
        /// </summary>
        public HeritageGroup Heritage { get; internal set; }

        /// <summary>
        /// The loyalty skill of this character
        /// </summary>
        public uint Loyalty { get; internal set; }

        /// <summary>
        /// The leadership skill of this character
        /// </summary>
        public uint Leadership { get; internal set; }

        /// <summary>
        /// The amount of experience passed up by this character
        /// </summary>
        public long Experience { get; internal set; }

        public AllegianceRecordFlags Flags { get; internal set; }

        internal AllegianceMember() {
        
        }

        internal static AllegianceMember From(uint parentId, UtilityBelt.Common.Messages.Types.AllegianceData data) {
            return new AllegianceMember() {
                Experience = data.ExperienceTithed,
                Flags = data.Bitfield,
                Gender = data.Gender,
                Heritage = data.Heritage,
                Id = data.ObjectId,
                Leadership = data.Leadership,
                Level = data.Level,
                Loyalty = data.Loyalty,
                Name = data.Name,
                ParentId = parentId,
                Rank = data.Rank
            };
        }
    }
}
