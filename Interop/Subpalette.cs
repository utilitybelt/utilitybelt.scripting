﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Interop {
    public struct Subpalette {
        /// <summary>
        /// The palette this subpallet is using.
        /// </summary>
        public uint Palette { get; internal set; }

        /// <summary>
        /// The offset to use when reading the palette
        /// </summary>
        public byte Offset { get; internal set; }

        /// <summary>
        /// The number of colors to pull from the palette.
        /// </summary>
        public byte NumColors { get; internal set; }
    }
}
