﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Lib;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Provides information about the character's busy status. This includes things like cast spells, moving items, playing animations, etc.
    /// </summary>
    public class BusyTracker : IDisposable {
        private AnimTracker _animTracker;
        private static ScriptManager _manager;
        private const int DEFAULT_TIMEOUT_MS = 5000;
        private const int DEFAULT_SLASH_RECALL_TIMEOUT_MS = 15000;

        /// <summary>
        /// Details about a specific action that is making the character busy.
        /// </summary>
        public class BusyActionDetails {
            internal DateTime StartedAt;

            public BusyAction BusyAction { get; }

            /// <summary>
            /// The id of the weenie (if any) that you are interacting with.
            /// If spellcasting, this is you.
            /// </summary>
            public uint SourceWeenieId { get; internal set; }
            public WorldObject SourceWeenie => _manager.GameState.WorldState.GetWeenie(SourceWeenieId);

            /// <summary>
            /// The id of the weenie target (if any) that you are interacting with.
            /// For example when moving an item to a container, this is the container.
            /// giving an item to another player, this is the player. If spell casting,
            /// this is the target (if applicable).
            /// </summary>
            public uint TargetWeenieId { get; internal set; }
            public WorldObject TargetWeenie => _manager.GameState.WorldState.GetWeenie(TargetWeenieId);

            /// <summary>
            /// When spellcasting, this is the spell you are currently casting.
            /// </summary>
            public uint SpellId { get; internal set; }
            public Spell Spell => _manager.GameState.Character.SpellBook[SpellId];

            /// <summary>
            /// String description, if any, that describes this action.
            /// For animations this is the name of the Stance/Movement
            /// </summary>
            public string Description { get; internal set; }

            /// <summary>
            /// The combat mode being switched to, if any
            /// </summary>
            public CombatMode CombatMode { get; internal set; } = CombatMode.Invalid;

            internal TimeSpan Timeout { get; set; } = TimeSpan.Zero;

            internal BusyActionDetails(BusyAction busyAction, uint sourceWeenieId = 0, uint targetWeenieId = 0, uint spellId = 0) {
                BusyAction = busyAction;
                SourceWeenieId = sourceWeenieId;
                TargetWeenieId = targetWeenieId;
                SpellId = spellId;
            }

            internal BusyActionDetails(BusyAction busyAction, string description) {
                BusyAction = busyAction;
                Description = description;
            }

            public override string ToString() {
                var str = new StringBuilder($"{BusyAction}");
                if (Spell != null)
                    str.Append($" / Spell: {Spell}");
                if (SourceWeenie != null)
                    str.Append($" / Source: {SourceWeenie}");
                if (TargetWeenie != null)
                    str.Append($" / Target: {TargetWeenie}");
                if (!string.IsNullOrEmpty(Description))
                    str.Append($" / {Description}");
                if (CombatMode != CombatMode.Invalid)
                    str.Append($" / {CombatMode}");

                return str.ToString();
            }
        }

        private BusyActionDetails _noAction = new BusyActionDetails(BusyAction.None);
        private Dictionary<string, BusyActionDetails> _animActionCache = new Dictionary<string, BusyActionDetails>();

        /// <summary>
        /// The action you are busy performing
        /// </summary>
        public BusyActionDetails CurrentAction {
            get {
                var currentAnim = _animTracker.AnimationQueue.FirstOrDefault();
                if (Actions.Count > 0) {
                    return Actions.First();
                }
                else if (currentAnim != null && currentAnim.DurationSeconds > 0) {
                    if (!_animActionCache.ContainsKey(currentAnim.Name)) {
                        _animActionCache.Add(currentAnim.Name, new BusyActionDetails(BusyAction.Animating, currentAnim.ToString()));
                    }
                    return _animActionCache[currentAnim.Name];
                }
                else {
                    return _noAction;
                }
            }
        }

        public List<BusyActionDetails> Actions { get; } = new List<BusyActionDetails>();

        /// <summary>
        /// The datetime the last action was started at
        /// </summary>
        public DateTime LastActionStartedAt { get; internal set; }


        public BusyTracker(AnimTracker animTracker) {
            _animTracker = animTracker;
            _manager = ScriptManager.Instance;

            // actions the client sends to cause busy state to change
            _manager.MessageHandler.Outgoing.Inventory_DropItem += Out_Inventory_DropItem;
            _manager.MessageHandler.Outgoing.Inventory_GetAndWieldItem += Out_Inventory_GetAndWieldItem;
            _manager.MessageHandler.Outgoing.Inventory_GiveObjectRequest += Out_Inventory_GiveObjectRequest;
            _manager.MessageHandler.Outgoing.Inventory_PutItemInContainer += Out_Inventory_PutItemInContainer;
            _manager.MessageHandler.Outgoing.Inventory_StackableMerge += Out_Inventory_StackableMerge;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitTo3D += Out_Inventory_StackableSplitTo3D;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitToContainer += Out_Inventory_StackableSplitToContainer;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitToWield += Out_Inventory_StackableSplitToWield;
            _manager.MessageHandler.Outgoing.Inventory_UseEvent += Out_Inventory_UseEvent;
            _manager.MessageHandler.Outgoing.Inventory_UseWithTargetEvent += Out_Inventory_UseWithTargetEvent;
            _manager.MessageHandler.Outgoing.Magic_CastTargetedSpell += Outgoing_Magic_CastTargetedSpell;
            _manager.MessageHandler.Outgoing.Magic_CastUntargetedSpell += Outgoing_Magic_CastUntargetedSpell;
            _manager.MessageHandler.Outgoing.Character_TeleToLifestone += Outgoing_Character_TeleToLifestone;
            _manager.MessageHandler.Outgoing.Character_LoginCompleteNotification += Outgoing_Character_LoginCompleteNotification;
            _manager.MessageHandler.Outgoing.Combat_ChangeCombatMode += Outgoing_Combat_ChangeCombatMode;
            _manager.MessageHandler.Outgoing.Character_TeleToMarketplace += Outgoing_Character_TeleToMarketplace;
            _manager.MessageHandler.Outgoing.Character_TeleToPKArena += Outgoing_Character_TeleToPKArena;
            _manager.MessageHandler.Outgoing.Character_TeleToPKLArena += Outgoing_Character_TeleToPKLArena;
            _manager.MessageHandler.Outgoing.Advocate_Teleport += Outgoing_Advocate_Teleport;
            _manager.MessageHandler.Outgoing.House_TeleToHouse += Outgoing_House_TeleToHouse;
            _manager.MessageHandler.Outgoing.House_TeleToMansion += Outgoing_House_TeleToMansion;
            _manager.MessageHandler.Outgoing.Allegiance_RecallAllegianceHometown += Outgoing_Allegiance_RecallAllegianceHometown;
            _manager.MessageHandler.Outgoing.Movement_Jump += Outgoing_Movement_Jump;
            _manager.MessageHandler.Outgoing.Trade_OpenTradeNegotiations += Outgoing_Trade_OpenTradeNegotiations;

            // monitor for server to say busy state ended 
            _manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed += In_Character_ServerSaysAttemptFailed;
            _manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            _manager.MessageHandler.Incoming.Item_ServerSaysMoveItem += Incoming_Item_ServerSaysMoveItem;
            _manager.MessageHandler.Incoming.Item_ServerSaysContainID += Incoming_Item_ServerSaysContainID;
            _manager.MessageHandler.Incoming.Item_WearItem += Incoming_Item_WearItem;
            _manager.MessageHandler.Incoming.Effects_PlayerTeleport += Incoming_Effects_PlayerTeleport;
            _manager.MessageHandler.Incoming.Movement_SetObjectMovement += Incoming_Movement_SetObjectMovement;
            _manager.MessageHandler.Incoming.Item_UpdateStackSize += Incoming_Item_UpdateStackSize;
            _manager.MessageHandler.Incoming.Communication_WeenieError += Incoming_Communication_WeenieError;
            _manager.MessageHandler.Incoming.Trade_RegisterTrade += Incoming_Trade_RegisterTrade;
        }

        internal void Tick() {
            var actions = Actions.ToArray();
            foreach (var action in actions) {
                if (DateTime.UtcNow - action.StartedAt > action.Timeout) {
                    Actions.Remove(action);
                }
            }
        }

        private void Incoming_Trade_RegisterTrade(object sender, Trade_RegisterTrade_S2C_EventArgs e) {
            ClearBusy("Incoming_Trade_RegisterTrade", a => a.BusyAction == BusyAction.OpeningTrade);
        }

        private void Outgoing_Trade_OpenTradeNegotiations(object sender, Trade_OpenTradeNegotiations_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.OpeningTrade, _manager.GameState.CharacterId, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Incoming_Communication_WeenieError(object sender, Communication_WeenieError_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.YouHaveMovedTooFar:
                    ClearBusy($"Incoming_Communication_WeenieError: {e.Data.Type}");
                    break;
                case StatusMessage.TradeIgnoringRequests:
                case StatusMessage.TradeMaxDistanceExceeded:
                case StatusMessage.TradeClosed:
                    ClearBusy($"Incoming_Communication_WeenieError: {e.Data.Type}", a => a.BusyAction == BusyAction.OpeningTrade);
                    break;
            }
        }

        private void Outgoing_Movement_Jump(object sender, Movement_Jump_C2S_EventArgs e) {
            // jump events clear animations
            var removed = _animTracker.AnimationQueue.RemoveAll(a => true);
        }

        private void Outgoing_Allegiance_RecallAllegianceHometown(object sender, Allegiance_RecallAllegianceHometown_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToAllegianceHometown, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Outgoing_House_TeleToMansion(object sender, House_TeleToMansion_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToMansion, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Outgoing_House_TeleToHouse(object sender, House_TeleToHouse_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToHouse, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Outgoing_Advocate_Teleport(object sender, Advocate_Teleport_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.AdvocateTeleport, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Outgoing_Character_TeleToPKLArena(object sender, Character_TeleToPKLArena_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToPKArena, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Outgoing_Character_TeleToPKArena(object sender, Character_TeleToPKArena_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToPKArena, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Outgoing_Character_TeleToMarketplace(object sender, Character_TeleToMarketplace_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToMarketplace, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_SLASH_RECALL_TIMEOUT_MS));
        }

        private void Incoming_Item_UpdateStackSize(object sender, Item_UpdateStackSize_S2C_EventArgs e) {
            ClearBusy("Incoming_Item_UpdateStackSize", a => e.Data.ObjectId == a.SourceWeenieId || e.Data.ObjectId == a.TargetWeenieId);
        }

        private void Incoming_Movement_SetObjectMovement(object sender, Movement_SetObjectMovement_S2C_EventArgs e) {
            if (e.Data.ObjectId != _manager.GameState.CharacterId)
                return;
            CombatMode mode = CombatMode.Invalid;

            switch (e.Data.MovementData.Stance) {
                case MotionStance.HandCombat:
                case MotionStance.DualWieldCombat:
                case MotionStance.SwordCombat:
                case MotionStance.SwordShieldCombat:
                case MotionStance.TwoHandedStaffCombat:
                case MotionStance.TwoHandedSwordCombat:
                    mode = CombatMode.Melee;
                    break;
                case MotionStance.BowCombat:
                case MotionStance.AtlatlCombat:
                case MotionStance.CrossbowCombat:
                case MotionStance.CrossBowNoAmmo:
                case MotionStance.ThrownShieldCombat:
                case MotionStance.ThrownWeaponCombat:
                    mode = CombatMode.Missile;
                    break;
                case MotionStance.Magic:
                    mode = CombatMode.Magic;
                    break;
                default:
                    mode = CombatMode.NonCombat;
                    break;
            }

            if (mode != CombatMode.Invalid && Actions.Any(a => a.BusyAction == BusyAction.ChangeCombatMode)) {
                var currentAnim = _animTracker.AnimationQueue.FirstOrDefault();
                if (currentAnim != null) {
                    currentAnim.OnFinished += (s, ev) => {
                        ClearBusy("Incoming_Movement_SetObjectMovement_Anim_OnFinished", a => a.BusyAction == BusyAction.ChangeCombatMode);
                    };
                }
                else {
                    ClearBusy("Incoming_Movement_SetObjectMovement", a => a.BusyAction == BusyAction.ChangeCombatMode);
                }
            }
        }

        private void Outgoing_Combat_ChangeCombatMode(object sender, Combat_ChangeCombatMode_C2S_EventArgs e) {
            ClearBusy("Outgoing_Combat_ChangeCombatMode", a => a.BusyAction == BusyAction.ChangeCombatMode);
            SetBusy(new BusyActionDetails(BusyAction.ChangeCombatMode) {
                CombatMode = e.Data.Mode
            }, TimeSpan.FromMilliseconds(1000));
        }

        private void Outgoing_Character_LoginCompleteNotification(object sender, Character_LoginCompleteNotification_C2S_EventArgs e) {
            ClearBusy("Outgoing_Character_LoginCompleteNotification", a => true);
        }

        private void Incoming_Effects_PlayerTeleport(object sender, Effects_PlayerTeleport_S2C_EventArgs e) {
            ClearBusy("Incoming_Effects_PlayerTeleport", a => true);
            SetBusy(new BusyActionDetails(BusyAction.Portalling), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Outgoing_Character_TeleToLifestone(object sender, Character_TeleToLifestone_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.TeleToLifestone, _manager.GameState.CharacterId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Incoming_Item_WearItem(object sender, Item_WearItem_S2C_EventArgs e) {
            ClearBusy("Incoming_Item_WearItem", a => e.Data.ObjectId == a.SourceWeenieId);
        }

        private void Outgoing_Magic_CastUntargetedSpell(object sender, Magic_CastUntargetedSpell_C2S_EventArgs e) {
            ClearBusy("Outgoing_Magic_CastUntargetedSpell", a => a.BusyAction == BusyAction.ChangeCombatMode);
            SetBusy(new BusyActionDetails(BusyAction.SpellCasting, _manager.GameState.CharacterId, 0, e.Data.SpellId.Id), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Outgoing_Magic_CastTargetedSpell(object sender, Magic_CastTargetedSpell_C2S_EventArgs e) {
            ClearBusy("Outgoing_Magic_CastTargetedSpell", a => a.BusyAction == BusyAction.ChangeCombatMode);
            SetBusy(new BusyActionDetails(BusyAction.SpellCasting, _manager.GameState.CharacterId, e.Data.ObjectId, e.Data.SpellId.Id), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Incoming_Item_ServerSaysContainID(object sender, Item_ServerSaysContainID_S2C_EventArgs e) {
            ClearBusy("Incoming_Item_ServerSaysContainID", a => e.Data.ObjectId == a.SourceWeenieId);
        }

        private void Incoming_Item_ServerSaysMoveItem(object sender, Item_ServerSaysMoveItem_S2C_EventArgs e) {
            ClearBusy("Incoming_Item_ServerSaysMoveItem", a => e.Data.ObjectId == a.SourceWeenieId);
        }

        private void Incoming_Item_UseDone(object sender, Item_UseDone_S2C_EventArgs e) {
            if (e.Data.Type == StatusMessage.None) {
                ClearBusy($"Incoming_Item_UseDone: {e.Data.Type}", 0);
            }
            else {
                ClearBusy($"Incoming_Item_UseDone: {e.Data.Type}", Actions.Count - 1);
            }
        }

        private void In_Character_ServerSaysAttemptFailed(object sender, Character_ServerSaysAttemptFailed_S2C_EventArgs e) {
            ClearBusy("In_Character_ServerSaysAttemptFailed", a => a.SourceWeenieId == e.Data.ObjectId);
        }

        private void SetBusy(BusyActionDetails details, TimeSpan timeout) {
            _manager.Resolve<ILogger>().LogDebug($"SetBusy: {details} (timeout: {timeout})");
            details.Timeout = timeout;
            details.StartedAt = DateTime.UtcNow;
            Actions.Add(details);
        }

        private void ClearBusy(string reason = "", int index = -1) {
            if (Actions.Count > 0 && (index < 0 || index <= Actions.Count - 1)) {
                _manager.Resolve<ILogger>().LogDebug($"ClearBusy: {CurrentAction} Reason: {reason}");
                Actions.RemoveAt(index < 0 ? Actions.Count - 1 : index);
            }
        }

        private void ClearBusy(string reason, Predicate<BusyActionDetails> value) {
            _manager.Resolve<ILogger>().LogDebug($"ClearBusy: {CurrentAction} Reason: {reason}");
            var removed = Actions.RemoveAll(value);
        }

        #region event handlers
        private void Out_Inventory_UseWithTargetEvent(object sender, Inventory_UseWithTargetEvent_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.UsingWeenie, e.Data.ObjectId, e.Data.TargetId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_UseEvent(object sender, Inventory_UseEvent_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.UsingWeenie, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_StackableSplitToWield(object sender, Inventory_StackableSplitToWield_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.Wielding, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_StackableSplitToContainer(object sender, Inventory_StackableSplitToContainer_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenieToContainer, e.Data.ObjectId, e.Data.ContainerId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_StackableSplitTo3D(object sender, Inventory_StackableSplitTo3D_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenieToContainer, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_StackableMerge(object sender, Inventory_StackableMerge_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenie, e.Data.ObjectId, e.Data.TargetId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_PutItemInContainer(object sender, Inventory_PutItemInContainer_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenieToContainer, e.Data.ObjectId, e.Data.ContainerId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_GiveObjectRequest(object sender, Inventory_GiveObjectRequest_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenieToContainer, e.Data.ObjectID, e.Data.TargetId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_GetAndWieldItem(object sender, Inventory_GetAndWieldItem_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.Wielding, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }

        private void Out_Inventory_DropItem(object sender, Inventory_DropItem_C2S_EventArgs e) {
            SetBusy(new BusyActionDetails(BusyAction.MovingWeenieToContainer, e.Data.ObjectId), TimeSpan.FromMilliseconds(DEFAULT_TIMEOUT_MS));
        }
        #endregion // event handlers

        public void Dispose() {
            // actions the client sends to cause busy state to change
            _manager.MessageHandler.Outgoing.Inventory_DropItem -= Out_Inventory_DropItem;
            _manager.MessageHandler.Outgoing.Inventory_GetAndWieldItem -= Out_Inventory_GetAndWieldItem;
            _manager.MessageHandler.Outgoing.Inventory_GiveObjectRequest -= Out_Inventory_GiveObjectRequest;
            _manager.MessageHandler.Outgoing.Inventory_PutItemInContainer -= Out_Inventory_PutItemInContainer;
            _manager.MessageHandler.Outgoing.Inventory_StackableMerge -= Out_Inventory_StackableMerge;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitTo3D -= Out_Inventory_StackableSplitTo3D;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitToContainer -= Out_Inventory_StackableSplitToContainer;
            _manager.MessageHandler.Outgoing.Inventory_StackableSplitToWield -= Out_Inventory_StackableSplitToWield;
            _manager.MessageHandler.Outgoing.Inventory_UseEvent -= Out_Inventory_UseEvent;
            _manager.MessageHandler.Outgoing.Inventory_UseWithTargetEvent -= Out_Inventory_UseWithTargetEvent;
            _manager.MessageHandler.Outgoing.Magic_CastTargetedSpell -= Outgoing_Magic_CastTargetedSpell;
            _manager.MessageHandler.Outgoing.Magic_CastUntargetedSpell -= Outgoing_Magic_CastUntargetedSpell;
            _manager.MessageHandler.Outgoing.Character_TeleToLifestone -= Outgoing_Character_TeleToLifestone;
            _manager.MessageHandler.Outgoing.Character_LoginCompleteNotification -= Outgoing_Character_LoginCompleteNotification;
            _manager.MessageHandler.Outgoing.Combat_ChangeCombatMode -= Outgoing_Combat_ChangeCombatMode;
            _manager.MessageHandler.Outgoing.Character_TeleToMarketplace -= Outgoing_Character_TeleToMarketplace;
            _manager.MessageHandler.Outgoing.Character_TeleToPKArena -= Outgoing_Character_TeleToPKArena;
            _manager.MessageHandler.Outgoing.Character_TeleToPKLArena -= Outgoing_Character_TeleToPKLArena;
            _manager.MessageHandler.Outgoing.Advocate_Teleport -= Outgoing_Advocate_Teleport;
            _manager.MessageHandler.Outgoing.House_TeleToHouse -= Outgoing_House_TeleToHouse;
            _manager.MessageHandler.Outgoing.House_TeleToMansion -= Outgoing_House_TeleToMansion;
            _manager.MessageHandler.Outgoing.Allegiance_RecallAllegianceHometown -= Outgoing_Allegiance_RecallAllegianceHometown;
            _manager.MessageHandler.Outgoing.Movement_Jump -= Outgoing_Movement_Jump;
            _manager.MessageHandler.Outgoing.Trade_OpenTradeNegotiations -= Outgoing_Trade_OpenTradeNegotiations;

            // monitor for server to say busy state ended
            _manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed -= In_Character_ServerSaysAttemptFailed;
            _manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            _manager.MessageHandler.Incoming.Item_ServerSaysMoveItem -= Incoming_Item_ServerSaysMoveItem;
            _manager.MessageHandler.Incoming.Item_ServerSaysContainID -= Incoming_Item_ServerSaysContainID;
            _manager.MessageHandler.Incoming.Item_WearItem -= Incoming_Item_WearItem;
            _manager.MessageHandler.Incoming.Effects_PlayerTeleport -= Incoming_Effects_PlayerTeleport;
            _manager.MessageHandler.Incoming.Movement_SetObjectMovement -= Incoming_Movement_SetObjectMovement;
            _manager.MessageHandler.Incoming.Item_UpdateStackSize -= Incoming_Item_UpdateStackSize;
            _manager.MessageHandler.Incoming.Communication_WeenieError -= Incoming_Communication_WeenieError;
            _manager.MessageHandler.Incoming.Trade_RegisterTrade -= Incoming_Trade_RegisterTrade;
        }
    }
}