﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Types;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Represents an enchantment on a player
    /// </summary>
    public class Enchantment {
        /// <summary>
        /// The full layered id of this enchantment
        /// </summary>
        public LayeredSpellId LayeredId { get; }

        /// <summary>
        /// Spell id of this enchantment
        /// </summary>
        public uint SpellId => LayeredId.Id;

        /// <summary>
        /// Layer of this enchantment. Higher layer values take priority.
        /// </summary>
        public ushort Layer => LayeredId.Layer;

        /// <summary>
        /// Tru if this enchantment has a spell set associated with it
        /// </summary>
        public bool HasSpellSetId { get; internal set; }

        /// <summary>
        /// The category of this enchantment
        /// </summary>
        public SpellCategory Category { get; internal set; }

        /// <summary>
        /// The spell power / difficulty of this enchantment spell
        /// </summary>
        public uint Power { get; internal set; }

        /// <summary>
        /// The time in seconds that the enchantment has been active before we heard about
        /// it from the server.
        /// </summary>
        public double StartTime { get; internal set; }

        /// <summary>
        /// The total duration in seconds this enchantment should last for
        /// </summary>
        public double Duration { get; internal set; }

        /// <summary>
        /// The id of the object who cast this enchantment (player / equipment / etc)
        /// </summary>
        public uint CasterId { get; internal set; }

        /// <summary>
        /// ?
        /// </summary>
        public float DegradeModifier { get; internal set; }

        /// <summary>
        /// ?
        /// </summary>
        public float DegradeLimit { get; internal set; }

        /// <summary>
        /// ?
        /// </summary>
        public double LastTimeDegraded { get; internal set; }

        /// <summary>
        /// Enchantment flags
        /// </summary>
        public EnchantmentFlags Flags { get; internal set; }

        /// <summary>
        /// The stat key this enchantment affects. Could be of type AttributeID, VitalID, or SkillID depending on Flags
        /// </summary>
        public uint StatKey { get; internal set; }

        /// <summary>
        /// The stat value modifier of this enchantment
        /// </summary>
        public float StatValue { get; internal set; }

        /// <summary>
        /// The spell set id this enchantment comes from.  Only valid if HasSpellSetId is true.
        /// </summary>
        public uint SpellSetId { get; internal set; }

        /// <summary>
        /// The spell flags for this enchantment
        /// </summary>
        public SpellFlags Effect { get; internal set; }

        /// <summary>
        /// The datetime this enchantment will expire. All times are in UTC.
        /// Formula is: (Duration &lt; 0) ? DateTime.MaxValue : ClientReceivedAt + TimeSpan.FromSeconds(Duration) - TimeSpan.FromSeconds(StartTime)
        /// </summary>
        public DateTime ExpiresAt => (Duration < 0) ? DateTime.MaxValue : ClientReceivedAt + TimeSpan.FromSeconds(Duration) - TimeSpan.FromSeconds(StartTime);

        /// <summary>
        /// The datetime that the client recieved this enchantment from the server.
        /// </summary>
        public DateTime ClientReceivedAt { get; internal set; }

        internal Enchantment(LayeredSpellId layeredId) {
            LayeredId = layeredId;
            ClientReceivedAt = DateTime.UtcNow;
        }

        internal static Enchantment FromMessage(UtilityBelt.Common.Messages.Types.Enchantment enchantmentData) {
            var enchantment = new Enchantment(enchantmentData.Id);

            enchantment.StartTime = enchantmentData.StartTime;
            enchantment.Power = enchantmentData.PowerLevel;
            enchantment.CasterId = enchantmentData.Caster;
            enchantment.DegradeLimit = enchantmentData.DegradeLimit;
            enchantment.DegradeModifier = enchantmentData.DegradeModifier;
            enchantment.Duration = enchantmentData.Duration;
            enchantment.HasSpellSetId = enchantmentData.HasSpellSetID > 0;
            enchantment.LastTimeDegraded = enchantmentData.LastTimeDegraded;
            enchantment.Power = enchantmentData.PowerLevel;
            enchantment.Category = (SpellCategory)(uint)enchantmentData.Category;
            enchantment.StartTime = enchantmentData.StartTime;
            enchantment.Flags = enchantmentData.StatMod.Flags;
            enchantment.StatKey = enchantmentData.StatMod.Key;
            enchantment.StatValue = enchantmentData.StatMod.Value;

            return enchantment;
        }

        public override string ToString() {
            return $"Enchantment(Spell: {SpellId}, Set: {SpellSetId}, Category: {Category}, Caster: {CasterId}, Power: {Power}, StartTime: {StartTime}, Duration: {Duration}, Flags: {Flags}, StatKey: {StatKey}, StatValue: {StatValue})";
        }
    }
}
