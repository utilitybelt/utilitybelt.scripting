﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Common.Enums;
using ACE.DatLoader;
using ACE.DatLoader.Entity;
using SkillFormula = UtilityBelt.Scripting.Interop.SkillFormula;
using UtilityBelt.Scripting.Lib;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Interop {
    public class Vital {
        private ILogger _log;

        private WorldObject _weenie;
        private int _current;

        private CharacterState _characterState => ScriptManager.Instance.GameState.Character;

        /// <summary>
        /// The type of vital this represents
        /// </summary>
        public VitalId Type { get; set; }

        /// <summary>
        /// The amount of points this vital has been raised by
        /// </summary>
        public uint PointsRaised { get; set; }

        /// <summary>
        /// The initial level of this vital
        /// </summary>
        public uint InitLevel { get; set; }

        /// <summary>
        /// Total experience spent in this vital
        /// </summary>
        public uint Experience { get; set; }

        /// <summary>
        /// The formula to use when calculating this vital
        /// </summary>
        public virtual SkillFormula Formula { get; }

        /// <summary>
        /// The base value of this vital
        /// </summary>
        public virtual int Base {
            get {
                var _base = (int)(InitLevel + PointsRaised);
                // todo: health ratings from gear
                /*
                if (Formula.UseFormula) {
                    var attrBonus = _weenie.Attributes[Formula.Attribute1].Base;
                    if (Formula.Attribute1 == AttributeID.Endurance) {
                        attrBonus += 1;
                    }
                    if (Formula.Attribute2 != 0) {
                        attrBonus += _weenie.Attributes[Formula.Attribute2].Base;
                    }

                    _base += (int)Math.Round(((float)attrBonus / Formula.Divisor));
                }
                */
                return _base;
            }
        }

        /// <summary>
        /// The max value for this vital. This includes enchantments/vitae
        /// </summary>
        public virtual int Max {
            get {
                // logic from ACE
                var max = (int)(InitLevel + PointsRaised);
                if (Type == VitalId.Health) {
                    if (_weenie.Value(IntId.Enlightenment) > 0)
                        max += _weenie.Value(IntId.Enlightenment) * 2;
                    max += _weenie.GetGearMaxHealth();
                }

                if (Formula.UseFormula) {
                    var attrBonus = _weenie.Attributes[Formula.Attribute1].Current;
                    if (Formula.Attribute2 != 0) {
                        attrBonus += _weenie.Attributes[Formula.Attribute2].Current;
                    }

                    max += (int)Math.Floor(((float)attrBonus / Formula.Divisor) + 0.5f);
                }


                // todo: health ratings from gear GetGearMaxHealth()
                var multiplier = _characterState.GetEnchantmentsMultiplierModifier(Type);
                var fTotal = max * multiplier;

                if (_characterState.Vitae < 1.0f && _characterState.Vitae > 0.0f) {
                    fTotal *= _characterState.Vitae;
                }

                var additives = _characterState.GetEnchantmentsAdditiveModifier(Type);
                var iTotal = (int)Math.Floor(fTotal + (float)additives + 0.5f);
                var minVital = max >= 5 ? 5 : 1;

                iTotal = Math.Max(minVital, iTotal);

                return iTotal;
            }
        }

        /// <summary>
        /// The current value of this vital
        /// </summary>
        public virtual int Current {
            get {
                return _current;
            }

            set {
                _current = value;
            }
        }

        internal Vital(VitalId vitalId, WorldObject weenie) {
            Type = vitalId;
            _weenie = weenie;
            _log = ScriptManager.Instance.Resolve<ILogger>();

            var portalDat = ScriptManager.Instance.Resolve<PortalDatDatabase>();

            Attribute2ndBase attrBase = null;
            switch (vitalId) {
                case VitalId.Health:
                    attrBase = portalDat.SecondaryAttributeTable.MaxHealth;
                    break;
                case VitalId.Stamina:
                    attrBase = portalDat.SecondaryAttributeTable.MaxStamina;
                    break;
                case VitalId.Mana:
                    attrBase = portalDat.SecondaryAttributeTable.MaxMana;
                    break;
            }

            if (attrBase != null) {
                var formula = attrBase.Formula;
                Formula = new SkillFormula(formula.X > 0 ? true : false, formula.Z, (AttributeId)formula.Attr1, (AttributeId)formula.Attr2);
            }
            else {
                Formula = new SkillFormula(false, 1, 0, 0);
            }
        }
    }
}
