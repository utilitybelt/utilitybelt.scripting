﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Actions;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about a character's allegiance
    /// </summary>
    public class Allegiance : IDisposable {
        private readonly ScriptManager _manager;

        /// <summary>
        /// True if your character is in an allegiance
        /// </summary>
        public bool Exists { get; private set; }

        /// <summary>
        /// The name of the allegiance your character is in
        /// </summary>
        public string Name { get; private set; } = "";

        /// <summary>
        /// Total number of members in this allegiance
        /// </summary>
        public uint TotalMembers { get; private set; }

        /// <summary>
        /// The number of followers your character has
        /// </summary>
        public uint TotalVassals { get; private set; }

        /// <summary>
        /// Your allegiance rank
        /// </summary>
        public uint Rank { get; private set; }

        /// <summary>
        /// Wether or not the allegiance is locked
        /// </summary>
        public bool Locked { get; private set; }

        /// <summary>
        /// The allegiance message of the day
        /// </summary>
        public string MOTD { get; private set; } = "";

        /// <summary>
        /// Allegiance bind point
        /// </summary>
        public Position BindPoint { get; private set; } = new Position();

        /// <summary>
        /// Allegiance chat room id
        /// </summary>
        public uint ChatRoomId { get; private set; }

        /// <summary>
        /// Monarch info
        /// </summary>
        public AllegianceMember Monarch { get; private set; } = new AllegianceMember();

        /// <summary>
        /// Patron info
        /// </summary>
        public AllegianceMember Patron { get; private set; } = new AllegianceMember();

        /// <summary>
        /// Vassals info
        /// </summary>
        public List<AllegianceMember> Vassals { get; } = new List<AllegianceMember>();

        internal Allegiance() {
            _manager = ScriptManager.Instance;

            _manager.MessageHandler.Incoming.Allegiance_AllegianceInfoResponseEvent += Incoming_Allegiance_AllegianceInfoResponseEvent;
            _manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate += Incoming_Allegiance_AllegianceUpdate;
        }

        #region public api
        /// <inheritdoc cref="ClientActions.AllegianceSwear(uint, ActionOptions, Action{AllegianceSwearAction})"/>
        public AllegianceSwearAction SwearTo(uint objectId, ActionOptions options = null, Action<AllegianceSwearAction> callback = null) {
            return _manager.GameState.Actions.AllegianceSwear(objectId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.AllegianceBreak(ActionOptions, Action{AllegianceBreakAction})"/>
        public AllegianceBreakAction Break(ActionOptions options = null, Action<AllegianceBreakAction> callback = null) {
            return _manager.GameState.Actions.AllegianceBreak(options, callback);
        }
        #endregion // public api

        private void Incoming_Allegiance_AllegianceUpdate(object sender, Allegiance_AllegianceUpdate_S2C_EventArgs e) {
            if (e.Data.Profile == null)
                return;


            if (e.Data.Profile.Hierarchy == null || e.Data.Profile.Hierarchy.RecordCount == 0) {
                Exists = false;
                ClearAllegianceInfo();
                return;
            }
            else {
                Exists = true;
            }

            Name = e.Data.Profile.Hierarchy.AllegianceName;
            TotalMembers = e.Data.Profile.TotalMembers;
            TotalVassals = e.Data.Profile.TotalVassals;
            Rank = e.Data.Rank;
            Locked = e.Data.Profile.Hierarchy.Locked;
            MOTD = e.Data.Profile.Hierarchy.MOTD;
            BindPoint.Landcell = e.Data.Profile.Hierarchy.Bindpoint.Landcell;
            BindPoint.Frame.Orientation.W = e.Data.Profile.Hierarchy.Bindpoint.Frame.Orientation.W;
            BindPoint.Frame.Orientation.X = e.Data.Profile.Hierarchy.Bindpoint.Frame.Orientation.X;
            BindPoint.Frame.Orientation.Y = e.Data.Profile.Hierarchy.Bindpoint.Frame.Orientation.Y;
            BindPoint.Frame.Orientation.Z = e.Data.Profile.Hierarchy.Bindpoint.Frame.Orientation.Z;
            BindPoint.Frame.Origin = new System.Numerics.Vector3(e.Data.Profile.Hierarchy.Bindpoint.Frame.Origin.X, e.Data.Profile.Hierarchy.Bindpoint.Frame.Origin.Y, e.Data.Profile.Hierarchy.Bindpoint.Frame.Origin.Z);

            Monarch = AllegianceMember.From(0, e.Data.Profile.Hierarchy.MonarchData);

            var myEntry = e.Data.Profile.Hierarchy.Records.FirstOrDefault(r => r.AllegianceData.ObjectId == _manager.GameState.CharacterId);
            if (myEntry != null) {
                if (myEntry.TreeParent == Monarch.Id) {
                    Patron = Monarch;
                }
                else {
                    var patron = e.Data.Profile.Hierarchy.Records.FirstOrDefault(r => r.AllegianceData.ObjectId == myEntry.TreeParent);
                    if (patron != null) {
                        Patron = AllegianceMember.From(patron.TreeParent, patron.AllegianceData);
                    }
                }
            }

            Vassals.Clear();
            foreach (var entry in e.Data.Profile.Hierarchy.Records) {
                if (entry.TreeParent == _manager.GameState.CharacterId) {
                    Vassals.Add(AllegianceMember.From(entry.TreeParent, entry.AllegianceData));
                }
            }
        }

        private void ClearAllegianceInfo() {
            Name = "";
            TotalMembers = 0;
            TotalVassals = 0;
            Rank = 0;
            Locked = false;
            MOTD = "";
            BindPoint = new Position();
            Monarch = new AllegianceMember();
            Patron = new AllegianceMember();
            Vassals.Clear();
        }

        private void Incoming_Allegiance_AllegianceInfoResponseEvent(object sender, Allegiance_AllegianceInfoResponseEvent_S2C_EventArgs e) {
            //UBService.UBService.WriteLog("Incoming_Allegiance_AllegianceInfoResponseEvent");
            //UBService.UBService.WriteLog(JsonConvert.SerializeObject(e.Data, Formatting.Indented));
        }

        public void Dispose() {
            _manager.MessageHandler.Incoming.Allegiance_AllegianceInfoResponseEvent -= Incoming_Allegiance_AllegianceInfoResponseEvent;
            _manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate -= Incoming_Allegiance_AllegianceUpdate;
        }
    }
}
