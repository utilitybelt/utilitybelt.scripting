﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Options for Queue Actions
    /// </summary>
    public class ActionOptions {
        /// <summary>
        /// A friendly name for this action. Used for debugging.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Custom priority, by default actions will self-assign a priority
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// Set to true to skip any precondition checks
        /// </summary>
        public bool SkipChecks { get; set; }

        /// <summary>
        /// Maximum number of times to retry this action. -1 retries forever.
        /// </summary>
        public int? MaxRetryCount { get; set; }

        /// <summary>
        /// The timeout in milliseconds for this action to fail. When retrying
        /// an action, the Timeout is reset.
        /// </summary>
        public uint? TimeoutMilliseconds { get; set; }

        /// <summary>
        /// Create a new instance of ActionOptions
        /// </summary>
        public ActionOptions() {
        
        }

        public override string ToString() {
            return $"{{ ActionOptions: [Name: {Name}, Priority:{Priority}, MaxRetryCount:{MaxRetryCount}, TimeoutMilliseconds: {TimeoutMilliseconds}] }}";
        }
    }
}
