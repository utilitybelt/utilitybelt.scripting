﻿using System.Collections.Generic;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Common.Enums;
using System.Numerics;

namespace UtilityBelt.Scripting.Interop {
    public class PhysicsDescription {
        public uint ObjectId { get; internal set; }

        public PhysicsDescriptionFlag Flags { get; internal set; }

        public Vector3 Acceleration { get; } = new Vector3();
        public Vector3 Omega { get; } = new Vector3();
        public Vector3 Velocity { get; } = new Vector3();

        public uint DefaultScript { get; internal set; }

        public float DefaultScriptIntensity { get; internal set; }

        public ushort ObjectPositionSequence { get; internal set; }

        public ushort ObjectStateSequence { get; internal set; }

        public ushort ObjectVectorSequence { get; internal set; }

        public ushort ObjectTeleportSequence { get; internal set; }

        public ushort ObjectServerControlSequence { get; internal set; }

        public ushort ObjectForcePositionSequence { get; internal set; }

        public ushort ObjectVisualDescSequence { get; internal set; }

        public ushort ObjectInstanceSequence { get; internal set; }

        public ushort ObjectMovementSequence { get; internal set; }

        public uint PhysicsScriptTableID { get; internal set; }

        public IEnumerable<byte> Bytes { get; internal set; } = new List<byte>();

        public bool Autonomous { get; internal set; }

        public uint AnimationFrame { get; internal set; }

        public Position Position { get; internal set;  }

        public uint MotionTableID { get; internal set; }

        public uint SoundTableID { get; internal set; }

        public uint MovementByteCount { get; internal set; }

        public uint SetupTableID { get; internal set; }

        public uint LocationID { get; internal set; }

        public uint NumChildren { get; internal set; }

        //internal List<PhysicsChild> _Children { get; } = new List<PhysicsChild>();
        //public IEnumerable<IPhysicsChild> Children => (IEnumerable<IPhysicsChild>)_Children;

        public float Scale { get; internal set; }

        public float Friction { get; internal set; }

        public float Elasticity { get; internal set; }

        public float Translucency { get; internal set; }

        public uint Parent { get; internal set; }

        public PhysicsState State { get; internal set; }
    }
}