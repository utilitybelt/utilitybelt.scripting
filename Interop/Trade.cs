﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    public class Trade : IDisposable {
        private readonly ScriptManager _manager;
        private readonly ILogger _log;

        /// <summary>
        /// Trade has been reset.
        /// </summary>
        public event EventHandler<TradeResetEventArgs> OnReset;

        /// <summary>
        /// A new trade has started
        /// </summary>
        public event EventHandler<TradeStartedEventArgs> OnStarted;

        /// <summary>
        /// Trade has been declined by someone. (trade is still ongoing, this just means they unchecked the "Trade" arrow
        /// and are not willing to accept the current trade)
        /// </summary>
        public event EventHandler<TradeDeclinedEventArgs> OnDeclined;

        /// <summary>
        /// An object has been added to the trade window.
        /// </summary>
        public event EventHandler<ObjectAddedToTradeEventArgs> OnObjectAdded;

        /// <summary>
        /// The current trade has ended. (You no longer have a trade partner)
        /// </summary>
        public event EventHandler<TradeEndedEventArgs> OnEnded;

        /// <summary>
        /// The trade has been accepted by both parties and is now completed. The trade window remains open.
        /// </summary>
        public event EventHandler<TradeCompletedEventArgs> OnCompleted;

        /// <summary>
        /// The trade has been accepted by someone. (trade is still ongoing, this just means someone clicked the "Trade" arrow
        /// and is willing to accept the current trade)
        /// </summary>
        public event EventHandler<TradeAcceptedEventArgs> OnAccepted;

        /// <summary>
        /// An object you tried to add to the trade window failed to be added.
        /// </summary>
        public event EventHandler<ObjectFailedToAddToTradeEventArgs> OnFailedToAddObject;

        /// <summary>
        /// The trade failed for some unknown reason. This clears the trade items from both sides, and declines the trade on both sides.
        /// The trade window remains open.
        /// </summary>
        public event EventHandler<TradeFailedEventArgs> OnFailed;

        /// <summary>
        /// Check if a trade is currently going on.
        /// </summary>
        public bool IsOpen { get; private set; }

        /// <summary>
        /// The id of your current trade partner
        /// </summary>
        public uint PartnerId { get; private set; }

        /// <summary>
        /// The ids of the items on your side of the trade window. (these are the items you are trading away)
        /// </summary>
        public List<uint> YourItemIds { get; } = new List<uint>();

        /// <summary>
        /// The ids of the items on your partner's side of the trade window. (these are the items you will be recieving)
        /// </summary>
        public List<uint> PartnerItemIds { get; } = new List<uint>();

        /// <summary>
        /// Wether or not you have accepted the trade. This is the state of the green "Trade" button in acclient. Both
        /// parties must accept for the trade to be completed.
        /// </summary>
        public bool YouAccepted { get; private set; }

        /// <summary>
        /// Wether or not your partner has accepted the trade. This is the state of the green "Trade" button in acclient. Both
        /// parties must accept for the trade to be completed.
        /// </summary>
        public bool PartnerAccepted { get; private set; }

        internal Trade() {
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();

            _manager.MessageHandler.Incoming.Trade_AcceptTrade += MessageHandler_Trade_AcceptTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_AddToTrade += MessageHandler_Trade_AddToTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_ClearTradeAcceptance += MessageHandler_Trade_ClearTradeAcceptance_S2C;
            _manager.MessageHandler.Incoming.Trade_CloseTrade += MessageHandler_Trade_CloseTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_DeclineTrade += MessageHandler_Trade_DeclineTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_OpenTrade += MessageHandler_Trade_OpenTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_RegisterTrade += MessageHandler_Trade_RegisterTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_RemoveFromTrade += MessageHandler_Trade_RemoveFromTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_ResetTrade += MessageHandler_Trade_ResetTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_TradeFailure += MessageHandler_Trade_TradeFailure_S2C;
        }

        private void ResetTradeState() {
            IsOpen = false;
            PartnerId = 0;
            YouAccepted = false;
            PartnerAccepted = false;
            YourItemIds.Clear();
            PartnerItemIds.Clear();
        }

        private void MessageHandler_Trade_TradeFailure_S2C(object sender, Trade_TradeFailure_S2C_EventArgs e) {
            OnFailedToAddObject?.Invoke(this, new ObjectFailedToAddToTradeEventArgs(e.Data.ObjectId, (WeenieError)e.Data.Reason));
        }

        private void MessageHandler_Trade_ResetTrade_S2C(object sender, Trade_ResetTrade_S2C_EventArgs e) {
            //_log.Log($"MessageHandler_Trade_ResetTrade_S2C: {e.Data.sourceID:X8}");

            if (YouAccepted && PartnerAccepted) {
                // trade is completed
                OnCompleted?.Invoke(this, new TradeCompletedEventArgs(PartnerId, YourItemIds, PartnerItemIds));
            }

            YourItemIds.Clear();
            PartnerItemIds.Clear();
            YouAccepted = false;
            PartnerAccepted = false;
            OnReset?.Invoke(this, new TradeResetEventArgs(e.Data.ObjectId));
        }

        private void MessageHandler_Trade_RemoveFromTrade_S2C(object sender, Trade_RemoveFromTrade_S2C_EventArgs e) {
            // todo: is this used?
            _log.LogError($"MessageHandler_Trade_RemoveFromTrade_S2C: {e.Data.ObjectId:X8} // {e.Data.Side}");
        }

        private void MessageHandler_Trade_RegisterTrade_S2C(object sender, Trade_RegisterTrade_S2C_EventArgs e) {
            //_log.Log($"MessageHandler_Trade_RegisterTrade_S2C: Initiator: {e.Data.initiator:X8} Partner: {e.Data.partner:X8}");
            IsOpen = true;
            if (e.Data.InitiatorId == _manager.GameState.CharacterId) {
                PartnerId = e.Data.PartnerId;
            }
            else {
                PartnerId = e.Data.InitiatorId;
            }
            OnStarted?.Invoke(this, new TradeStartedEventArgs(e.Data.InitiatorId, e.Data.PartnerId));
        }

        private void MessageHandler_Trade_OpenTrade_S2C(object sender, Trade_OpenTrade_S2C_EventArgs e) {
            // todo: is this used?
            _log.LogError($"MessageHandler_Trade_OpenTrade_S2C: {e.Data.ObjectId:X8}");
        }

        // this is called when someone unaccepts thier trade (the green trade arrow, trade is still open)
        private void MessageHandler_Trade_DeclineTrade_S2C(object sender, Trade_DeclineTrade_S2C_EventArgs e) {
            if (e.Data.ObjectId == _manager.GameState.CharacterId) {
                YouAccepted = false;
            }
            else {
                PartnerAccepted = false;
            }

            OnDeclined?.Invoke(this, new TradeDeclinedEventArgs(e.Data.ObjectId));
        }

        private void MessageHandler_Trade_CloseTrade_S2C(object sender, Trade_CloseTrade_S2C_EventArgs e) {
            ResetTradeState();
            OnEnded?.Invoke(this, new TradeEndedEventArgs(e.Data.Reason));
        }

        private void MessageHandler_Trade_ClearTradeAcceptance_S2C(object sender, Trade_ClearTradeAcceptance_S2C_EventArgs e) {
            YourItemIds.Clear();
            PartnerItemIds.Clear();
            YouAccepted = false;
            PartnerAccepted = false;

            OnFailed?.Invoke(this, new TradeFailedEventArgs());
        }

        private void MessageHandler_Trade_AddToTrade_S2C(object sender, Trade_AddToTrade_S2C_EventArgs e) {
            //_log.Log($"MessageHandler_Trade_AddToTrade_S2C: {e.Data.itemID:X8} // {e.Data.side}");

            if (e.Data.Side == UtilityBelt.Common.Enums.TradeSide.Self) {
                YourItemIds.Add(e.Data.ObjectId);
            }
            else {
                PartnerItemIds.Add(e.Data.ObjectId);
            }

            // when the items change both players decline any accepted trade
            YouAccepted = false;
            PartnerAccepted = false;

            OnObjectAdded?.Invoke(this, new ObjectAddedToTradeEventArgs(e.Data.ObjectId, e.Data.Side));
        }

        private void MessageHandler_Trade_AcceptTrade_S2C(object sender, Trade_AcceptTrade_S2C_EventArgs e) {
            //_log.Log($"MessageHandler_Trade_AcceptTrade_S2C: {e.Data.sourceID:X8}");
            if (e.Data.ObjectId == _manager.GameState.CharacterId) {
                YouAccepted = true;
            }
            else {
                PartnerAccepted = true;
            }

            OnAccepted?.Invoke(this, new TradeAcceptedEventArgs(e.Data.ObjectId));
        }

        public void Dispose() {
            _manager.MessageHandler.Incoming.Trade_AcceptTrade -= MessageHandler_Trade_AcceptTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_AddToTrade -= MessageHandler_Trade_AddToTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_ClearTradeAcceptance -= MessageHandler_Trade_ClearTradeAcceptance_S2C;
            _manager.MessageHandler.Incoming.Trade_CloseTrade -= MessageHandler_Trade_CloseTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_DeclineTrade -= MessageHandler_Trade_DeclineTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_OpenTrade -= MessageHandler_Trade_OpenTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_RegisterTrade -= MessageHandler_Trade_RegisterTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_RemoveFromTrade -= MessageHandler_Trade_RemoveFromTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_ResetTrade -= MessageHandler_Trade_ResetTrade_S2C;
            _manager.MessageHandler.Incoming.Trade_TradeFailure -= MessageHandler_Trade_TradeFailure_S2C;
        }
    }
}
