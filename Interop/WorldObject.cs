﻿using ACE.Entity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Lib;
using ObjDesc = UtilityBelt.Common.Messages.Types.ObjDesc;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Represents an ingame weenie object.  All selectable (and some non-selectable) objects are weenies.
    /// </summary>
    public class WorldObject {
        private static readonly InstanceId[] _parentInstanceIds = {
            InstanceId.Container,
            InstanceId.Wielder
        };
        private ILogger _log;
        private ScriptManager _manager;
        private GameState _gameState;
        private ClientActions _actions => _gameState.Actions;
        private List<LayeredSpellId> _spellIds = new List<LayeredSpellId>();
        private List<LayeredSpellId> _enchantmentIds = new List<LayeredSpellId>();
        private DateTime _lastAccessTime = DateTime.UtcNow;
        private ObjectClass _objectClass = ObjectClass.Unknown;
        private MotionStance _stance = MotionStance.NonCombat;

        internal event EventHandler<VitalChangedEventArgs> OnVitalChanged;

        /// <summary>
        /// Fired when this weenie's position is updated by the server
        /// </summary>
        public event EventHandler<ServerPositionChangedEventArgs> OnPositionChanged;

        /// <summary>
        /// Fired when this WorldObject is being destroyed, like when the client forgets about it.
        /// </summary>
        public event EventHandler<ObjectReleasedEventArgs> OnDestroyed;

        /// <summary>
        /// The id of this weenie object
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// The name of this weenie object.
        /// </summary>
        public string Name => Value(StringId.Name);

        /// <summary>
        /// The class id for this weenie
        /// </summary>
        public uint WeenieClassId { get; private set; }

        /// <summary>
        /// ObjectClass, for decal compatibility / familiarity (also a bit more granular than ObjectType)
        /// </summary>
        public ObjectClass ObjectClass {
            get {
                if (_objectClass == ObjectClass.Unknown) {
                    ObjectClass objectClass = ObjectClass.Unknown;
                    int _type = (int)ObjectType;
                    int _bools = (int)ObjectDescriptionFlag;
                    int num3 = (int)CreateFlags;

                    if ((_type & 1) > 0) objectClass = ObjectClass.MeleeWeapon;
                    else if ((_type & 2) > 0) objectClass = ObjectClass.Armor;
                    else if ((_type & 4) > 0) objectClass = ObjectClass.Clothing;
                    else if ((_type & 8) > 0) objectClass = ObjectClass.Jewelry;
                    else if ((_type & 16) > 0) objectClass = ObjectClass.Monster;
                    else if ((_type & 32) > 0) objectClass = ObjectClass.Food;
                    else if ((_type & 64) > 0) objectClass = ObjectClass.Money;
                    else if ((_type & 128) > 0) objectClass = ObjectClass.Misc;
                    else if ((_type & 256) > 0) objectClass = ObjectClass.MissileWeapon;
                    else if ((_type & 512) > 0) objectClass = ObjectClass.Container;
                    else if ((_type & 1024) > 0) objectClass = ObjectClass.Bundle;
                    else if ((_type & 2048) > 0) objectClass = ObjectClass.Gem;
                    else if ((_type & 4096) > 0) objectClass = ObjectClass.SpellComponent;
                    else if ((_type & 16384) > 0) objectClass = ObjectClass.Key;
                    else if ((_type & 32768) > 0) objectClass = ObjectClass.WandStaffOrb;
                    else if ((_type & 65536) > 0) objectClass = ObjectClass.Portal;
                    else if ((_type & 262144) > 0) objectClass = ObjectClass.TradeNote;
                    else if ((_type & 524288) > 0) objectClass = ObjectClass.ManaStone;
                    else if ((_type & 1048576) > 0) objectClass = ObjectClass.Services;
                    else if ((_type & 2097152) > 0) objectClass = ObjectClass.Plant;
                    else if ((_type & 4194304) > 0) objectClass = ObjectClass.BaseCooking;
                    else if ((_type & 8388608) > 0) objectClass = ObjectClass.BaseAlchemy;
                    else if ((_type & 16777216) > 0) objectClass = ObjectClass.BaseFletching;
                    else if ((_type & 33554432) > 0) objectClass = ObjectClass.CraftedCooking;
                    else if ((_type & 67108864) > 0) objectClass = ObjectClass.CraftedAlchemy;
                    else if ((_type & 134217728) > 0) objectClass = ObjectClass.CraftedFletching;
                    else if ((_type & 536870912) > 0) objectClass = ObjectClass.Ust;
                    else if ((_type & 1073741824) > 0) objectClass = ObjectClass.Salvage;
                    if ((_bools & 8) > 0) objectClass = ObjectClass.Player;
                    else if ((_bools & 512) > 0) objectClass = ObjectClass.Vendor;
                    else if ((_bools & 4096) > 0) objectClass = ObjectClass.Door;
                    else if ((_bools & 8192) > 0) objectClass = ObjectClass.Corpse;
                    else if ((_bools & 16384) > 0) objectClass = ObjectClass.Lifestone;
                    else if ((_bools & 32768) > 0) objectClass = ObjectClass.Food;
                    else if ((_bools & 65536) > 0) objectClass = ObjectClass.HealingKit;
                    else if ((_bools & 131072) > 0) objectClass = ObjectClass.Lockpick;
                    else if ((_bools & 262144) > 0) objectClass = ObjectClass.Portal;
                    else if ((_bools & 8388608) > 0) objectClass = ObjectClass.Foci;
                    else if ((_bools & 1) > 0) objectClass = ObjectClass.Container;
                    if ((_type & 8192) > 0 && (_bools & 256) > 0 && objectClass == ObjectClass.Unknown) {
                        if ((_bools & 2) > 0) objectClass = ObjectClass.Journal;
                        else if ((_bools & 4) > 0) objectClass = ObjectClass.Sign;
                        else if ((_bools & 15) > 0) objectClass = ObjectClass.Book;
                    }
                    if ((_type & 8192) > 0 && (num3 & 4194304) > 0) objectClass = ObjectClass.Scroll;
                    if (objectClass == ObjectClass.Monster && (_bools & 16) == 0) objectClass = ObjectClass.Npc;
                    if (objectClass == ObjectClass.Monster && (_bools & 67108864) != 0) objectClass = ObjectClass.Npc;

                    _objectClass = objectClass;
                }

                return _objectClass;
            }
        }

        public ObjectDescription ObjectDescription { get; } = new ObjectDescription();

        public PhysicsDescription PhysicsDesc { get; } = new PhysicsDescription();

        public Dictionary<IntId, int> IntValues { get; } = new Dictionary<IntId, int>();
        public Dictionary<Int64Id, long> Int64Values { get; } = new Dictionary<Int64Id, long>();
        public Dictionary<StringId, string> StringValues { get; } = new Dictionary<StringId, string>();
        public Dictionary<BoolId, bool> BoolValues { get; } = new Dictionary<BoolId, bool>();
        public Dictionary<FloatId, float> FloatValues { get; } = new Dictionary<FloatId, float>();
        public Dictionary<InstanceId, uint> InstanceValues { get; } = new Dictionary<InstanceId, uint>();
        public Dictionary<DataId, uint> DataValues { get; } = new Dictionary<DataId, uint>();

        public Dictionary<SkillId, Skill> Skills { get; } = new Dictionary<SkillId, Skill>();

        public Dictionary<AttributeId, Attribute> Attributes { get; } = new Dictionary<AttributeId, Attribute>();

        public Dictionary<VitalId, Vital> Vitals { get; } = new Dictionary<VitalId, Vital>();

        /// <summary>
        /// List of layered spell ids this item has. (this is not active spells, these are spells that the item contains)
        /// </summary>
        public IList<LayeredSpellId> SpellIds => _spellIds;

        /// <summary>
        /// List of layered spell ids of active enchantments this item has.
        /// </summary>
        public IList<LayeredSpellId> EnchantmentIds => _enchantmentIds;

        /// <summary>
        /// The spell id of this item, like for built-in wand spells
        /// </summary>
        public uint SpellId { get; private set; }

        /// <summary>
        /// Last time this weenie received data from the server
        /// </summary>
        public DateTime LastAccessTime {
            get => _lastAccessTime;
            set {
                _lastAccessTime = value;

                foreach (var child in Equipment) {
                    child.LastAccessTime = value;
                }
                foreach (var child in Containers) {
                    child.LastAccessTime = value;
                }
            }
        }

        /// <summary>
        /// The date this weenie was created at, in memory.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// child item ids, excluding containers // equipped (assuming this item is a container) (not recursive,
        /// meaning it doesn't include child container items, just non-container items contained directly in this container)
        /// </summary>
        public ObservableCollection<uint> ItemIds { get; } = new ObservableCollection<uint>();

        /// <summary>
        /// child item weenies, excluding containers // equipped (assuming this item is a container) (not recursive,
        /// meaning it doesn't include child container items, just non-container items contained directly in this container)
        /// </summary>
        public IList<WorldObject> Items => ItemIds.Select(id => _gameState.WorldState.GetWeenie(id)).ToList();

        /// <summary>
        /// List of weenie ids contained with this item. It also returns item ids contained within containers within this container.
        /// </summary>
        public IEnumerable<uint> AllItemIds {
            get {
                var itemIds = new List<uint>();

                itemIds.AddRange(ItemIds);
                foreach (var container in Containers) {
                    itemIds.AddRange(container.ItemIds);
                }

                return itemIds;
            }
        }

        /// <summary>
        /// List of weenies contained with this item. It also returns items contained within containers within this container.
        /// </summary>
        public IList<WorldObject> AllItems => AllItemIds.Select(id => _gameState.WorldState.GetWeenie(id)).ToList();

        /// <summary>
        /// child container ids, including foci (assuming this item is a container)
        /// </summary>
        public List<uint> ContainerIds { get; } = new List<uint>();

        /// <summary>
        /// List of child container weenies, including foci
        /// </summary>
        public IList<WorldObject> Containers => ContainerIds.Select(id => _gameState.WorldState.GetWeenie(id)).ToList();

        /// <summary>
        /// item ids equipped to this weenie
        /// </summary>
        public List<uint> EquipmentIds { get; } = new List<uint>();

        /// <summary>
        /// List of weenies equipped to this weenie.
        /// </summary>
        public IList<WorldObject> Equipment => EquipmentIds.Select(id => _gameState.WorldState.GetWeenie(id)).ToList();

        /// <summary>
        /// the container id this is inside, 0 if none.
        /// </summary>
        public uint ContainerId => Value(InstanceId.Container);

        /// <summary>
        /// The container weenie this weenie is inside. null if none
        /// </summary>
        public WorldObject Container => _gameState.WorldState.GetWeenie(ContainerId);

        /// <summary>
        /// the id of the object wielding this item, 0 if none
        /// </summary>
        public uint WielderId => Value(InstanceId.Wielder);

        /// <summary>
        /// The weenie wielding this weenie, null if none
        /// </summary>
        public WorldObject Wielder => _gameState.WorldState.GetWeenie(WielderId);

        /// <summary>
        /// Equipment coverage priority
        /// </summary>
        public CoverageMask ClothingPriority => (CoverageMask)Value(IntId.ClothingPriority);

        /// <summary>
        /// Radar blip color
        /// </summary>
        public RadarColor RadarColor => (RadarColor)Value(IntId.RadarBlipColor);

        /// <summary>
        /// Radar Behavior
        /// </summary>
        public RadarBehavior RadarBehavior => (RadarBehavior)Value(IntId.ShowableOnRadar);

        /// <summary>
        /// Material
        /// </summary>
        public MaterialType Material => (MaterialType)Value(IntId.MaterialType);

        /// <summary>
        /// If this item is wielded, the slots it currently covers
        /// </summary>
        public EquipMask CurrentWieldedLocation => (EquipMask)Value(IntId.CurrentWieldedLocation);

        /// <summary>
        /// The valid locations this item can be wielded to
        /// </summary>
        public EquipMask ValidWieldedLocations => (EquipMask)Value(IntId.ValidLocations);

        /// <summary>
        /// The type of ammo this is, only applicable if its projectile ammo
        /// todo: what defines something being projectile ammo?
        /// </summary>
        public AmmoType AmmoType => (AmmoType)Value(IntId.AmmoType);

        /// <summary>
        /// Type of object this is
        /// </summary>
        public ObjectType ObjectType => (ObjectType)Value(IntId.ObjectType);

        /// <summary>
        /// Type of items that this can be used on?
        /// </summary>
        public ObjectType TargetType => (ObjectType)Value(IntId.TargetType);

        /// <summary>
        /// Icon effects, like border highlights
        /// </summary>
        public IconHighlight IconEffects => (IconHighlight)Value(DataId.IconOverlaySecondary);

        /// <summary>
        /// The type of wieldable item this is
        /// </summary>
        public WieldType CombatUse => (WieldType)Value(IntId.CombatUse);

        /// <summary>
        /// Object description
        /// </summary>
        public ObjectDescriptionFlag ObjectDescriptionFlag { get; internal set; }

        /// <summary>
        /// Burden
        /// </summary>
        public int Burden => Value(IntId.EncumbranceVal);

        /// <summary>
        /// Weenie ClassId
        /// </summary>
        public uint ClassId { get; internal set; }

        /// <summary>
        /// Type of container
        /// </summary>
        public ContainerProperties ContainerProperties { get; internal set; }

        /// <summary>
        /// Physics State
        /// </summary>
        public PhysicsState PhysicsState => (PhysicsState)Value(IntId.PhysicsState);

        /// <summary>
        /// Wether or not this weenie has appraisal data from the server.
        /// </summary>
        public bool HasAppraisalData { get; internal set; }

        /// <summary>
        /// The last time we got appraisal data from the server
        /// </summary>
        public DateTime LastAppraisalResponse { get; internal set; } = DateTime.MinValue;

        /// <summary>
        /// Last position the server reported this weenie at, only applies to items in the landscape
        /// </summary>
        public Position ServerPosition { get; } = new Position();

        /// <summary>
        /// The number of milliseconds until the shared cooldown for this item is expired, or 0 if it is not on cooldown.
        /// </summary>
        public double CooldownTimeoutMilliseconds {
            get {
                var cooldown = _gameState.Character.SharedCooldowns.FirstOrDefault(c => c.Id == Value(IntId.SharedCooldown, -1));
                if (cooldown == null)
                    return 0;

                return (cooldown.ExpiresAt - DateTime.UtcNow).TotalMilliseconds;
            }
        }

        /// <summary>
        /// The stance the weenie is in, only applies to creatures.
        /// </summary>
        public MotionStance Stance {
            get => _stance == 0 ? MotionStance.NonCombat : _stance;
            set {
                if (value != 0)
                    _stance = value;
            }
        }

        /// <summary>
        /// The combat mode the weenie is in, only applies to creatures.
        /// </summary>
        public CombatMode CombatMode {
            get {
                switch (Stance) {
                    case MotionStance.HandCombat:
                    case MotionStance.DualWieldCombat:
                    case MotionStance.SwordCombat:
                    case MotionStance.SwordShieldCombat:
                    case MotionStance.TwoHandedStaffCombat:
                    case MotionStance.TwoHandedSwordCombat:
                        return CombatMode.Melee;
                    case MotionStance.BowCombat:
                    case MotionStance.AtlatlCombat:
                    case MotionStance.CrossbowCombat:
                    case MotionStance.CrossBowNoAmmo:
                    case MotionStance.ThrownShieldCombat:
                    case MotionStance.ThrownWeaponCombat:
                        return CombatMode.Missile;
                    case MotionStance.Magic:
                        return CombatMode.Magic;
                    default:
                        return CombatMode.NonCombat;
                }
            }
        }

        /// <summary>
        /// Weenie creation header flags. These are flags for if data fields are present during a weenie description
        /// </summary>
        public WeenieHeaderFlag CreateFlags { get; private set; }

        /// <summary>
        /// More weenie creation header flags. These are flags for if data fields are present during a weenie description
        /// </summary>
        public WeenieHeaderFlag2 CreateFlags2 { get; private set; }

        internal WorldObject(ILogger logger, ScriptManager manager, GameState gameState) {
            _log = logger;
            _manager = manager;
            _gameState = gameState;
        }

        internal int GetGearMaxHealth() {
            if (_gameState.Character?.Id != Id) {
                return 0;
            }

            return Value(IntId.GearMaxHealth);
        }

        internal void Dispose(ObjectReleasedEventArgs eventArgs) {
            OnDestroyed?.InvokeSafely(this, eventArgs);
        }

        #region Public API
        /// <inheritdoc cref="ClientActions.ObjectWield(uint, EquipMask, ActionOptions, Action{ObjectWieldAction})"/>
        public ObjectWieldAction Wield(EquipMask slot = EquipMask.None, ActionOptions options = null, Action<ObjectWieldAction> callback = null) {
            return _manager.GameState.Actions.ObjectWield(Id, slot, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectDrop(uint, ActionOptions, Action{ObjectDropAction})"/>
        public ObjectDropAction Drop(ActionOptions options = null, Action<ObjectDropAction> callback = null) {
            return _manager.GameState.Actions.ObjectDrop(Id, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectGive(uint, uint, ActionOptions, Action{ObjectGiveAction})"/>
        public ObjectGiveAction Give(uint targetId, ActionOptions options = null, Action<ObjectGiveAction> callback = null) {
            return _manager.GameState.Actions.ObjectGive(Id, targetId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectMove(uint, uint, uint, bool, ActionOptions, Action{ObjectMoveAction})"/>
        public ObjectMoveAction Move(uint targetId, uint slot = 0, bool stack = true, ActionOptions options = null, Action<ObjectMoveAction> callback = null) {
            return _manager.GameState.Actions.ObjectMove(Id, targetId, slot, stack, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectAppraise(uint, ActionOptions, Action{ObjectAppraiseAction})"/>
        public ObjectAppraiseAction Appraise(ActionOptions options = null, Action<ObjectAppraiseAction> callback = null) {
            return _manager.GameState.Actions.ObjectAppraise(Id, options, callback);
        }

        /// <inheritdoc cref="ClientActions.SalvageAdd(uint, ActionOptions, Action{SalvageAddAction})"/>
        public SalvageAddAction AddToSalvage(ActionOptions options = null, Action<SalvageAddAction> callback = null) {
            return _manager.GameState.Actions.SalvageAdd(Id, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectSelect(uint, ActionOptions, Action{ObjectSelectAction})"/>
        public ObjectSelectAction Select(ActionOptions options = null, Action<ObjectSelectAction> callback = null) {
            return _manager.GameState.Actions.ObjectSelect(Id, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectUse(uint, uint, ActionOptions, Action{ObjectUseAction})"/>
        public ObjectUseAction Use(ActionOptions options = null, Action<ObjectUseAction> callback = null) {
            return _manager.GameState.Actions.ObjectUse(Id, 0, options, callback);
        }

        /// <inheritdoc cref="ClientActions.ObjectUse(uint, uint, ActionOptions, Action{ObjectUseAction})"/>
        public ObjectUseAction UseOn(uint targetId, ActionOptions options = null, Action<ObjectUseAction> callback = null) {
            return _manager.GameState.Actions.ObjectUse(Id, targetId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.TradeAdd(uint, ActionOptions, Action{TradeAddAction})"/>
        public TradeAddAction AddToTrade(ActionOptions options = null, Action<TradeAddAction> callback = null) {
            return _manager.GameState.Actions.TradeAdd(Id, options, callback);
        }

        /// <inheritdoc cref="ClientActions.Inscribe(uint, string, ActionOptions, Action{ObjectInscribeAction})"/>
        public ObjectInscribeAction Inscribe(string inscription, ActionOptions options = null, Action<ObjectInscribeAction> callback = null) {
            return _manager.GameState.Actions.Inscribe(Id, inscription, options, callback);
        }

        // /ub lexec game.world[2154427904].Salvage().finally(function(res) print(res) end)

        /// <summary>
        /// Immediately salvage this item.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public RunAllOrderedAction Salvage(ActionOptions options = null, Action<RunAllOrderedAction> callback = null) {
            if (options == null) {
                options = new ActionOptions() {
                    Priority = (int)ActionType.Immediate
                };
            }

            return _manager.GameState.Actions.RunAllOrdered(new QueueAction[] {
                new SalvageAddAction(Id, options),
                new SalvageAction(options)
            }, options, callback);
        }

        /// <summary>
        /// Check if this world object has an active shared cooldown timer
        /// </summary>
        /// <returns>True if this has an active shared cooldown timer, false otherwise</returns>
        public bool IsOnSharedCooldown() {
            var sharedCooldownId = Value(IntId.SharedCooldown, -1);

            if (sharedCooldownId == -1) {
                return false;
            }
            return _gameState.Character.SharedCooldowns.Any(c => c.Id == sharedCooldownId);
        }

        /// <summary>
        /// Gets the topmost parent of a weenie. (ie, the wielder / container, recursively).
        /// This can return the same weenie if there is no parent.
        /// </summary>
        /// <returns>The topmost weenie this weenie belongs to</returns>
        public WorldObject GetTopmostParent() {
            var finalParent = this;

            while (true) {
                var foundInstance = false;
                foreach (var instancePropertyID in _parentInstanceIds) {
                    var instanceId = finalParent.Value(instancePropertyID);
                    if (instanceId != 0) {
                        if (_gameState.WorldState.TryGetWeenie(instanceId, out WorldObject instance)) {
                            finalParent = instance;
                            foundInstance = true;
                            continue;
                        }
                    }
                }

                if (!foundInstance)
                    break;
            }

            return finalParent;
        }

        /// <summary>
        /// Get the 3d distance from one weenie to another weenie Id
        /// </summary>
        /// <param name="otherId">The id of the other weenie to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo3D(uint otherId) {
            var other = _manager.GameState.WorldState.GetWeenie(otherId);

            if (other == null)
                return -1f;

            return DistanceTo3D(other);
        }

        /// <summary>
        /// Get the 3d distance from one weenie to another weenie
        /// </summary>
        /// <param name="other">The weenie to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo3D(WorldObject other) {
            return ServerPosition.DistanceTo3D(other.ServerPosition);
        }

        /// <summary>
        /// Get the 2d distance from one weenie to another weenie Id
        /// </summary>
        /// <param name="otherId">The id of the other weenie to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo2d(uint otherId) {
            var other = _manager.GameState.WorldState.GetWeenie(otherId);

            if (other == null)
                return -1f;

            return DistanceTo2D(other);
        }

        /// <summary>
        /// Get the 2d distance from one weenie to another weenie
        /// </summary>
        /// <param name="other">The weenie to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo2D(WorldObject other) {
            return ServerPosition.DistanceTo2D(other.ServerPosition);
        }

        /// <summary>
        /// Check if this weenie has the specified IntPropertyID defined.
        /// </summary>
        /// <param name="key">The IntPropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(IntId key) {
            return IntValues.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified Int64PropertyID defined.
        /// </summary>
        /// <param name="key">The Int64PropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(Int64Id key) {
            return Int64Values.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified StringPropertyID defined.
        /// </summary>
        /// <param name="key">The StringPropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(StringId key) {
            return StringValues.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified BoolPropertyID defined.
        /// </summary>
        /// <param name="key">The BoolPropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(BoolId key) {
            return BoolValues.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified FloatPropertyID defined.
        /// </summary>
        /// <param name="key">The FloatPropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(FloatId key) {
            return FloatValues.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified InstancePropertyID defined.
        /// </summary>
        /// <param name="key">The InstancePropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(InstanceId key) {
            return InstanceValues.ContainsKey(key);
        }

        /// <summary>
        /// Check if this weenie has the specified DataPropertyID defined.
        /// </summary>
        /// <param name="key">The DataPropertyID to check</param>
        /// <returns>true if set, false if not set</returns>
        public bool HasValue(DataId key) {
            return DataValues.ContainsKey(key);
        }

        /// <summary>
        /// Gets the value of the specified IntPropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The IntPropertyID value to get</param>
        /// <param name="default">The default value to return if no IntPropertyID is defined</param>
        /// <returns>The value of the specified IntPropertyID key, otherwise returns the specified default</returns>
        public int Value(IntId key, int @default = 0) {
            if (IntValues.TryGetValue(key, out int value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified Int64PropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The Int64PropertyID value to get</param>
        /// <param name="default">The default value to return if no Int64PropertyID is defined</param>
        /// <returns>The value of the specified Int64PropertyID key, otherwise returns the specified default</returns>
        public long Value(Int64Id key, long @default = 0) {
            if (Int64Values.TryGetValue(key, out long value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified StringPropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The StringPropertyID value to get</param>
        /// <param name="default">The default value to return if no StringPropertyID is defined</param>
        /// <returns>The value of the specified StringPropertyID key, otherwise returns the specified default</returns>
        public string Value(StringId key, string @default = "") {
            if (StringValues.TryGetValue(key, out string value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified BoolPropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The BoolPropertyID value to get</param>
        /// <param name="default">The default value to return if no BoolPropertyID is defined</param>
        /// <returns>The value of the specified BoolPropertyID key, otherwise returns the specified default</returns>
        public bool Value(BoolId key, bool @default = false) {
            if (BoolValues.TryGetValue(key, out bool value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified FloatPropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The FloatPropertyID value to get</param>
        /// <param name="default">The default value to return if no FloatPropertyID is defined</param>
        /// <returns>The value of the specified FloatPropertyID key, otherwise returns the specified default</returns>
        public float Value(FloatId key, float @default = 0f) {
            if (FloatValues.TryGetValue(key, out float value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified InstancePropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The InstancePropertyID value to get</param>
        /// <param name="default">The default value to return if no InstancePropertyID is defined</param>
        /// <returns>The value of the specified InstancePropertyID key, otherwise returns the specified default</returns>
        public uint Value(InstanceId key, uint @default = 0) {
            if (InstanceValues.TryGetValue(key, out uint value)) {
                return value;
            }

            return @default;
        }

        /// <summary>
        /// Gets the value of the specified DataPropertyID if it exists, or returns a default value.
        /// </summary>
        /// <param name="key">The DataPropertyID value to get</param>
        /// <param name="default">The default value to return if no DataPropertyID is defined</param>
        /// <returns>The value of the specified DataPropertyID key, otherwise returns the specified default</returns>
        public uint Value(DataId key, uint @default = 0) {
            if (DataValues.TryGetValue(key, out uint value)) {
                return value;
            }

            return @default;
        }
        #endregion // Public API

        #region Property Methods
        internal void AddOrUpdateValue(IntId key, int value) {
            if (IntValues.ContainsKey(key)) {
                IntValues[key] = value;
            }
            else {
                IntValues.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(Int64Id key, long value) {
            if (Int64Values.ContainsKey(key)) {
                Int64Values[key] = value;
            }
            else {
                Int64Values.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(FloatId key, float value) {
            if (FloatValues.ContainsKey(key)) {
                FloatValues[key] = value;
            }
            else {
                FloatValues.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(StringId key, string value) {
            if (StringValues.ContainsKey(key)) {
                StringValues[key] = value;
            }
            else {
                StringValues.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(BoolId key, bool value) {
            if (BoolValues.ContainsKey(key)) {
                BoolValues[key] = value;
            }
            else {
                BoolValues.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(InstanceId key, uint value) {
            if (InstanceValues.TryGetValue(key, out uint oldValue) && oldValue == value) {
                return;
            }

            //_log.Log($"Setting 0x{Id:X8} {Name} {key} to 0x{value:X8} (was: 0x{Value(key):X8})");

            // not sure we should be doing this here... this should probably be in WorldState?
            // its just convenient to do it here so we know we will catch when certain properties
            // are updated...
            switch (key) {
                case InstanceId.Container:
                    // check for old container
                    if (InstanceValues.TryGetValue(key, out uint oldContainerId)) {
                        // try and remove this from old container
                        if(_gameState.WorldState.TryGetWeenie(oldContainerId, out WorldObject oldContainer)) {
                            switch (ContainerProperties) {
                                case ContainerProperties.None:
                                    oldContainer.ItemIds.Remove(Id);
                                    break;
                                case ContainerProperties.Foci:
                                case ContainerProperties.Container:
                                    oldContainer.ContainerIds.Remove(Id);
                                    break;
                            }
                        }
                    }
                    // check for new container
                    if (value != 0 && _gameState.WorldState.TryGetWeenie(value, out WorldObject newContainer)) {
                        // add to parent container items
                        switch (ContainerProperties) {
                            case ContainerProperties.None:
                                if (!newContainer.ItemIds.Contains(Id)) {
                                    newContainer.ItemIds.Add(Id);
                                }
                                break;
                            case ContainerProperties.Foci:
                            case ContainerProperties.Container:
                                if (!newContainer.ContainerIds.Contains(Id)) {
                                    newContainer.ContainerIds.Add(Id);
                                }
                                break;
                        }
                    }
                    break;
                case InstanceId.Wielder:
                    //_log.Log($"Setting {Name} 0x{Id:X8} wielder to 0x{value:X8}");

                    // check for old wielder
                    if (InstanceValues.TryGetValue(key, out uint oldWielderId)) {
                        // try and remove this from old wielder equipment
                        if(_gameState.WorldState.TryGetWeenie(oldWielderId, out WorldObject oldWielder)) {
                            oldWielder.EquipmentIds.Remove(Id);
                        }
                    }
                    // check for new wielder
                    if (value != 0 && _gameState.WorldState.TryGetWeenie(value, out WorldObject newWielder)) {
                        // add to parent equipment
                        if (!newWielder.EquipmentIds.Contains(Id)) {
                            newWielder.EquipmentIds.Add(Id);
                        }
                    }
                    break;
            }


            if (InstanceValues.ContainsKey(key)) {
                InstanceValues[key] = value;
            }
            else {
                InstanceValues.Add(key, value);
            }
        }

        internal void AddOrUpdateValue(DataId key, uint value) {
            if (DataValues.ContainsKey(key)) {
                DataValues[key] = value;
            }
            else {
                DataValues.Add(key, value);
            }
        }

        internal void RemoveValue(BoolId key) {
            BoolValues.Remove(key);
        }

        internal void RemoveValue(FloatId key) {
            FloatValues.Remove(key);
        }

        internal void RemoveValue(IntId key) {
            IntValues.Remove(key);
        }

        internal void RemoveValue(Int64Id key) {
            Int64Values.Remove(key);
        }

        internal void RemoveValue(InstanceId key) {
            InstanceValues.Remove(key);
        }

        internal void RemoveValue(StringId key) {
            StringValues.Remove(key);
        }

        internal void RemoveValue(DataId key) {
            DataValues.Remove(key);
        }
        #endregion Property Methods

        #region MessageDef converters
        private Vital AddOrCreateVital(VitalId key) {
            Vital vital = null;
            if (((int)key % 2) == 0)
                key -= 1;

            if (Vitals.TryGetValue(key, out Vital iVital)) {
                vital = iVital;
            }
            else {
                vital = new Vital(key, this);
                Vitals.Add(key, vital);
            }

            return vital;
        }

        internal void UpdateVital(VitalId key, SecondaryAttribute value, bool isInitialUpdate = true) {
            if (value == null)
                return;
            Vital vital = AddOrCreateVital(key);
            vital.InitLevel = value.Attribute.InitPoints;
            vital.PointsRaised = value.Attribute.PointsRaised;
            vital.Experience = value.Attribute.ExperienceSpent;

            if ((int)value.Level != vital.Current && (isInitialUpdate || ((int)key % 2) == 0)) {
                key -= 1;
                var old = vital.Current;
                vital.Current = (int)value.Level;
                if (!isInitialUpdate) {
                    OnVitalChanged?.Invoke(this, new VitalChangedEventArgs(key, vital.Current, old));
                }
            }
        }

        internal void UpdateVitalPointsRaised(VitalId key, uint value) {
            Vital vital = AddOrCreateVital(key);
            vital.PointsRaised = value;
        }

        internal void UpdateVitalCurrent(VitalId key, uint value) {
            Vital vital = AddOrCreateVital(key);

            if ((int)value != vital.Current && ((int)key % 2) == 0) {
                key -= 1;
                var old = vital.Current;
                vital.Current = (int)value;
                OnVitalChanged?.Invoke(this, new VitalChangedEventArgs(key, vital.Current, old));
            }
        }

        private Attribute AddOrCreateAttribute(AttributeId key) {
            Attribute attribute = null;
            if (Attributes.TryGetValue(key, out Interop.Attribute iAttribute)) {
                attribute = (Attribute)iAttribute;
            }
            else {
                attribute = new Attribute(key, this);
                Attributes.Add(key, attribute);
            }

            return attribute;
        }

        internal void UpdateAttribute(AttributeId key, UtilityBelt.Common.Messages.Types.Attribute value) {
            if (value == null)
                return;
            Attribute attribute = AddOrCreateAttribute(key);
            attribute.InitLevel = value.InitPoints;
            attribute.PointsRaised = value.PointsRaised;
            attribute.Experience = value.ExperienceSpent;
        }

        internal void UpdateAttributePointsRaised(AttributeId key, uint value) {
            Attribute attribute = AddOrCreateAttribute(key);

            attribute.PointsRaised = value;
        }

        private Skill AddOrCreateSkill(SkillId key) {
            Skill skill = null;
            if (Skills.TryGetValue(key, out Skill iSkill)) {
                skill = (Skill)iSkill;
            }
            else {
                skill = new Skill(key, this);
                Skills.Add(key, skill);
            }

            return skill;
        }

        internal void UpdateSkillTraining(SkillId key, SkillState value) {
            Skill skill = AddOrCreateSkill(key);

            skill.Training = (SkillTrainingType)value;
        }

        internal void UpdateSkillPointsRaised(SkillId key, uint value) {
            Skill skill = AddOrCreateSkill(key);

            skill.PointsRaised = value;
        }

        internal void UpdateSkill(SkillId key, UtilityBelt.Common.Messages.Types.Skill value) {
            if (value == null)
                return;
            Skill skill = AddOrCreateSkill(key);

            skill.AdjustXP = value.AdjustExperience;
            skill.InitLevel = value.InitPoints;
            skill.LastUsedTime = (float)value.LastUsedTime;
            skill.PointsRaised = value.PointsRaised;
            skill.ResistanceOfLastCheck = value.ResistanceOfLastCheck;
            skill.Training = (SkillTrainingType)value.SkillState;
            skill.Type = key;
            skill.Experience = value.ExperienceSpent;
        }

        internal void UpdateObjDesc(ObjDesc objDesc) {
            if (objDesc == null)
                return;
            ObjectDescription.Version = objDesc.Version;
            ObjectDescription.APChanges.Clear();
            ObjectDescription.APChanges.AddRange(objDesc.AnimPartChanges.Select(a => new Interop.AnimPartChange() {
                PartID = a.PartId,
                PartIndex = a.PartIndex
            }));
            ObjectDescription.ModelCount = objDesc.ModelCount;
            ObjectDescription.Palette = objDesc.Palette;
            ObjectDescription.PaletteCount = objDesc.PaletteCount;
            ObjectDescription.Subpallettes.Clear();
            ObjectDescription.Subpallettes.AddRange(objDesc.Subpalettes.Select(s => new Interop.Subpalette() {
                NumColors = s.NumColors,
                Offset = s.Offset,
                Palette = s.Palette
            }));
            ObjectDescription.TextureCount = objDesc.TextureCount;
            ObjectDescription.TMChanges.Clear();
            ObjectDescription.TMChanges.AddRange(objDesc.TextureMapChanges.Select(t => new Interop.TextureMapChange() {
                NewTexID = t.NewTextureId,
                OldTexID = t.OldTextureId,
                PartIndex = t.PartIndex
            }));
        }

        internal void UpdatePhysicsDesc(UtilityBelt.Common.Messages.Types.PhysicsDesc physicsDesc) {
            if (physicsDesc == null)
                return;

            // todo: flags

            //PhysicsDesc.Acceleration.X = physicsDesc.AccelerationX;
            //PhysicsDesc.Acceleration.Y = physicsDesc.AccelerationY;
            //PhysicsDesc.Acceleration.Z = physicsDesc.AccelerationZ;
            PhysicsDesc.AnimationFrame = physicsDesc.AnimationFrame;
            PhysicsDesc.Autonomous = physicsDesc.Autonomous;
            ((List<byte>)PhysicsDesc.Bytes).Clear();
            if (physicsDesc.Bytes != null) {
                ((List<byte>)PhysicsDesc.Bytes).AddRange(physicsDesc.Bytes);
            }
            /*
            _PhysicsDesc._Children.Clear();
            if (physicsDesc.children != null) {
                _PhysicsDesc._Children.AddRange(physicsDesc.children.Select(c => new PhysicsChild() {
                    ItemID = c.itemID,
                    LocationID = c.locationID
                }));
            }
            */
            if (physicsDesc.Position != null) {
                UpdatePosition(PositionPropertyID.Location, physicsDesc.Position);
            }
            PhysicsDesc.DefaultScript = physicsDesc.DefaultScript;
            PhysicsDesc.DefaultScriptIntensity = physicsDesc.DefaultScriptIntensity;
            PhysicsDesc.Elasticity = physicsDesc.Elasticity;
            PhysicsDesc.Flags = (PhysicsDescriptionFlag)physicsDesc.Flags;
            PhysicsDesc.Friction = physicsDesc.Friction;
            PhysicsDesc.LocationID = physicsDesc.LocationId;
            PhysicsDesc.MotionTableID = physicsDesc.MotionTableId;
            PhysicsDesc.MovementByteCount = physicsDesc.MovementByteCount;
            PhysicsDesc.NumChildren = physicsDesc.NumChildren;
            PhysicsDesc.ObjectForcePositionSequence = physicsDesc.ObjectForcePositionSequence;
            PhysicsDesc.ObjectId = Id;
            PhysicsDesc.ObjectInstanceSequence = physicsDesc.ObjectInstanceSequence;
            PhysicsDesc.ObjectMovementSequence = physicsDesc.ObjectMovementSequence;
            PhysicsDesc.ObjectPositionSequence = physicsDesc.ObjectPositionSequence;
            PhysicsDesc.ObjectServerControlSequence = physicsDesc.ObjectServerControlSequence;
            PhysicsDesc.ObjectStateSequence = physicsDesc.ObjectStateSequence;
            PhysicsDesc.ObjectTeleportSequence = physicsDesc.ObjectTeleportSequence;
            PhysicsDesc.ObjectVectorSequence = physicsDesc.ObjectVectorSequence;
            PhysicsDesc.ObjectVisualDescSequence = physicsDesc.ObjectVisualDescSequence;
            //PhysicsDesc.Omega.X = physicsDesc.OmegaX;
            //PhysicsDesc.Omega.Y = physicsDesc.OmegaY;
            //PhysicsDesc.Omega.Z = physicsDesc.OmegaZ;
            PhysicsDesc.Parent = physicsDesc.ParentId;
            PhysicsDesc.PhysicsScriptTableID = physicsDesc.PhysicsScriptTableId;
            PhysicsDesc.Scale = physicsDesc.Scale;
            PhysicsDesc.SetupTableID = physicsDesc.SetupTableId;
            PhysicsDesc.SoundTableID = physicsDesc.SoundTableId;
            PhysicsDesc.State = (PhysicsState)physicsDesc.State;
            PhysicsDesc.Translucency = physicsDesc.Translucency;
            //PhysicsDesc.Velocity.X = physicsDesc.VelocityX;
            //PhysicsDesc.Velocity.Y = physicsDesc.VelocityY;
            //PhysicsDesc.Velocity.Z = physicsDesc.VelocityZ;

            if ((physicsDesc.Flags & 0x00000020) != 0)
                AddOrUpdateValue(InstanceId.Wielder, physicsDesc.ParentId);
        }

        internal void UpdateWeenieDesc(PublicWeenieDesc wdesc) {
            if (wdesc == null)
                return;

            WeenieClassId = wdesc.WeenieClassId;
            AddOrUpdateValue(StringId.Name, wdesc.Name);
            AddOrUpdateValue(DataId.Icon, wdesc.Icon);
            AddOrUpdateValue(IntId.ObjectType, (int)wdesc.Type);
            ObjectDescriptionFlag = wdesc.ObjectDescription;
            ClassId = wdesc.WeenieClassId;
            SpellId = wdesc.SpellId;
            var flag1 = (WeenieHeaderFlag)wdesc.Flags;
            var flag2 = (WeenieHeaderFlag2)wdesc.Header2;
            CreateFlags = flag1;
            CreateFlags2 = flag2;

            if ((flag1 & WeenieHeaderFlag.AmmoType) != 0)
                AddOrUpdateValue(IntId.AmmoType, (int)wdesc.AmmunitionType);

            if ((flag1 & WeenieHeaderFlag.RadarBlipColor) != 0)
                AddOrUpdateValue(IntId.RadarBlipColor, wdesc.RadarColor);

            if ((flag1 & WeenieHeaderFlag.CombatUse) != 0)
                AddOrUpdateValue(IntId.CombatUse, (int)wdesc.CombatUse);

            if ((flag1 & WeenieHeaderFlag.ContainersCapacity) != 0)
                AddOrUpdateValue(IntId.ContainersCapacity, wdesc.ContainerCapacity);

            if ((flag1 & WeenieHeaderFlag.Container) != 0)
                AddOrUpdateValue(InstanceId.Container, wdesc.ContainerId);

            if ((flag2 & WeenieHeaderFlag2.CooldownDuration) != 0)
                AddOrUpdateValue(FloatId.CooldownDuration, wdesc.CooldownDuration);

            if ((flag2 & WeenieHeaderFlag2.Cooldown) != 0)
                AddOrUpdateValue(IntId.SharedCooldown, (int)wdesc.CooldownID);

            if ((flag1 & WeenieHeaderFlag.HookType) != 0)
                AddOrUpdateValue(IntId.HookType, (int)wdesc.HookType);

            if ((flag1 & WeenieHeaderFlag.HookableOn) != 0)
                AddOrUpdateValue(IntId.HookObjectType, (int)wdesc.HookObjectTypes);

            if ((flag1 & WeenieHeaderFlag.IconOverlay) != 0)
                AddOrUpdateValue(DataId.IconOverlay, wdesc.IconOverlay);

            if ((flag2 & WeenieHeaderFlag2.IconUnderlay) != 0)
                AddOrUpdateValue(DataId.IconUnderlay, wdesc.IconUnderlay);

            if ((flag1 & WeenieHeaderFlag.ItemsCapacity) != 0)
                AddOrUpdateValue(IntId.ItemsCapacity, wdesc.ItemsCapacity);

            if ((flag1 & WeenieHeaderFlag.CurrentlyWieldedLocation) != 0)
                AddOrUpdateValue(IntId.CurrentWieldedLocation, (int)wdesc.Location);

            if ((flag1 & WeenieHeaderFlag.MaterialType) != 0)
                AddOrUpdateValue(IntId.MaterialType, (int)wdesc.Material);

            if ((flag1 & WeenieHeaderFlag.MaxStackSize) != 0)
                AddOrUpdateValue(IntId.MaxStackSize, wdesc.MaxStackSize);

            if ((flag1 & WeenieHeaderFlag.MaxUses) != 0)
                AddOrUpdateValue(IntId.MaxStructure, wdesc.MaxUses);

            if ((flag1 & WeenieHeaderFlag.Monarch) != 0)
                AddOrUpdateValue(InstanceId.Monarch, wdesc.Monarch);

            if ((flag1 & WeenieHeaderFlag.PluralName) != 0)
                AddOrUpdateValue(StringId.PluralName, wdesc.PluralName);

            if ((flag1 & WeenieHeaderFlag.Owner) != 0)
                AddOrUpdateValue(InstanceId.Owner, wdesc.OwnerId);

            if ((flag2 & WeenieHeaderFlag2.PetOwner) != 0)
                AddOrUpdateValue(InstanceId.PetOwner, wdesc.PetOwnerId);

            if ((flag1 & WeenieHeaderFlag.PhysicsScript) != 0)
                AddOrUpdateValue(DataId.PhysicsScript, wdesc.PhysicsScript);

            if ((flag1 & WeenieHeaderFlag.Coverage) != 0)
                AddOrUpdateValue(IntId.ClothingPriority, (int)wdesc.Priority);

            if ((flag1 & WeenieHeaderFlag.RadarBehavior) != 0)
                AddOrUpdateValue(IntId.ShowableOnRadar, (int)wdesc.RadarType);

            if ((flag1 & WeenieHeaderFlag.Spell) != 0)
                AddOrUpdateValue(DataId.Spell, wdesc.SpellId);

            if ((flag1 & WeenieHeaderFlag.StackSize) != 0)
                AddOrUpdateValue(IntId.StackSize, wdesc.StackSize);

            if ((flag1 & WeenieHeaderFlag.Uses) != 0)
                AddOrUpdateValue(IntId.Structure, wdesc.UsesRemaining);

            if ((flag1 & WeenieHeaderFlag.TargetType) != 0)
                AddOrUpdateValue(IntId.TargetType, (int)wdesc.TargetType);

            if ((flag1 & WeenieHeaderFlag.Usable) != 0)
                AddOrUpdateValue(IntId.ItemUseable, (int)wdesc.Useability);

            if ((flag1 & WeenieHeaderFlag.UseRadius) != 0)
                AddOrUpdateValue(FloatId.UseRadius, wdesc.UseRadius);

            if ((flag1 & WeenieHeaderFlag.ValidEquipLocations) != 0)
                AddOrUpdateValue(IntId.ValidLocations, (int)wdesc.ValidLocations);

            if ((flag1 & WeenieHeaderFlag.Value) != 0)
                AddOrUpdateValue(IntId.Value, (int)wdesc.Value);

            if ((flag1 & WeenieHeaderFlag.Wielder) != 0)
                AddOrUpdateValue(InstanceId.Wielder, wdesc.WielderId);

            if ((flag1 & WeenieHeaderFlag.Workmanship) != 0)
                AddOrUpdateValue(IntId.ItemWorkmanship, (int)wdesc.Workmanship);

            if ((flag1 & WeenieHeaderFlag.Burden) != 0)
                AddOrUpdateValue(IntId.EncumbranceVal, (int)wdesc.Burden);

            if ((flag1 & WeenieHeaderFlag.UiEffects) != 0)
                AddOrUpdateValue(DataId.IconOverlaySecondary, (uint)wdesc.Effects);


            /*
            if (wdesc.restrictions != null) {
                _WeenieDesc._Restrictions.Bitmask = wdesc.restrictions.bitmask;
                _WeenieDesc._Restrictions.MonarchID = wdesc.restrictions.monarchID;
                _WeenieDesc._Restrictions._Table.Clear();
                foreach (var entry in wdesc.restrictions.table.table) {
                    _WeenieDesc._Restrictions._Table.Add(entry.Key, entry.Value);
                }
                _WeenieDesc._Restrictions.Version = wdesc.restrictions.version;
            }
            */
        }

        internal void UpdateStatsTable(PackableHashTable<InstanceId, uint> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<PositionPropertyID, UtilityBelt.Common.Messages.Types.Position> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                UpdatePosition(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<BoolId, bool> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<FloatId, double> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, (float)kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<IntId, int> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<Int64Id, long> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<StringId, string> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdateStatsTable(PackableHashTable<DataId, uint> didStatsTable) {
            if (didStatsTable == null || didStatsTable.Table == null)
                return;
            foreach (var kv in didStatsTable.Table) {
                AddOrUpdateValue(kv.Key, kv.Value);
            }
        }

        internal void UpdatePosition(PositionPropertyID key, MovementData movementData) {
            if (movementData == null)
                return;
            switch (key) {
                case PositionPropertyID.Location:
                    Stance = movementData.Stance;
                    if (movementData.Origin == null)
                        return;
                    ServerPosition.Landcell = movementData.Origin.Landcell;
                    ServerPosition.Frame.Origin = new System.Numerics.Vector3(movementData.Origin.Position.X, movementData.Origin.Position.Y, movementData.Origin.Position.Z);

                    OnPositionChanged?.Invoke(this, new ServerPositionChangedEventArgs(this, ServerPosition));
                    break;
                default:
                    //_log.Log($"Attempted to update unknown location: {key}");
                    break;
            }
        }

        internal void UpdatePosition(PositionPropertyID key, PositionPack position) {
            if (position == null)
                return;
            switch (key) {
                case PositionPropertyID.Location:
                    ServerPosition.Landcell = position.Origin.Landcell;
                    ServerPosition.Frame.Origin = new System.Numerics.Vector3(position.Origin.Position.X, position.Origin.Position.Y, position.Origin.Position.Z);
                    ServerPosition.Frame.Orientation.X = position.XQuat;
                    ServerPosition.Frame.Orientation.Y = position.YQuat;
                    ServerPosition.Frame.Orientation.Z = position.ZQuat;
                    ServerPosition.Frame.Orientation.W = position.WQuat;

                    //_log.Log($"0x{Id:X8} {Name}: Update position 2: {key} to {Position}");

                    OnPositionChanged?.Invoke(this, new ServerPositionChangedEventArgs(this, ServerPosition));
                    break;
                default:
                    _log.LogDebug($"Attempted to update unknown location: {key}");
                    break;
            }
        }

        internal void UpdatePosition(PositionPropertyID key, UtilityBelt.Common.Messages.Types.Position position) {
            switch (key) {
                case PositionPropertyID.Location:
                    ServerPosition.Landcell = position.Landcell;
                    ServerPosition.Frame.Origin = new System.Numerics.Vector3(position.Frame.Origin.X, position.Frame.Origin.Y, position.Frame.Origin.Z);
                    ServerPosition.Frame.Orientation.X = position.Frame.Orientation.X;
                    ServerPosition.Frame.Orientation.Y = position.Frame.Orientation.Y;
                    ServerPosition.Frame.Orientation.Z = position.Frame.Orientation.Z;
                    ServerPosition.Frame.Orientation.W = position.Frame.Orientation.W;
                    //_log.Log($"0x{Id:X8} {Name}: Update position 3: {key} to {Position}");

                    OnPositionChanged?.Invoke(this, new ServerPositionChangedEventArgs(this, ServerPosition));
                    break;
                default:
                    _log.LogDebug($"Attempted to update unknown location: {key}");
                    break;
            }
        }

        internal void UpdateSpells(PSmartArray<LayeredSpellId> spellBook) {
            if (spellBook == null || spellBook.Items == null)
                return;

            _spellIds.Clear();
            _enchantmentIds.Clear();

            foreach (var spellId in spellBook.Items) {
                if (spellId.Layer == 0x8000) {
                    _enchantmentIds.Add(spellId);
                }
                else {
                    _spellIds.Add(spellId);
                }
            }
        }
        #endregion // MessageDef converters

        public static implicit operator uint(WorldObject weenie) {
            return weenie.Id;
        }

        public static implicit operator WorldObject(uint weenieId) {
            return ScriptManager.Instance?.GameState?.WorldState?.GetWeenie(weenieId);
        }

        public override string ToString() {
            return $"WorldObject {Name} (0x{Id:X8})";
        }
    }
}