﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// A position represents a specific place in the game world. This includes a landblock, plus a [Frame](xref:UBScript.Interop.Frame) to determine its offset.
    /// </summary>
    public class Position {
        public static float MaxCoord = 102.15f;
        public static double C = (255.0 * 192.0) / (MaxCoord * 2.0);

        /// <summary>
        /// The landcell
        /// </summary>
        public uint Landcell { get; set; }

        /// <summary>
        /// The frame position
        /// </summary>
        public Frame Frame { get; } = new Frame();

        public Position() {

        }

        /// <summary>
        /// Get the 2D distance between two positions
        /// </summary>
        /// <param name="position">The position to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo2D(Position position) {
            var ax = LandblockToEW(Landcell >> 16 << 16, Frame.Origin.X);
            var bx = LandblockToEW(position.Landcell >> 16 << 16, position.Frame.Origin.X);
            var ay = LandblockToNS(Landcell >> 16 << 16, Frame.Origin.Y);
            var by = LandblockToNS(position.Landcell >> 16 << 16, position.Frame.Origin.Y);
            var dist = (float)Math.Sqrt(Math.Pow(Math.Abs(ax - bx), 2) + Math.Pow(Math.Abs(ay - by), 2));

            return (float)(dist * C);
        }


        /// <summary>
        /// Get the 3D distance between two positions
        /// </summary>
        /// <param name="position">The position to get the distance to from this one</param>
        /// <returns>The distance</returns>
        public float DistanceTo3D(Position position) {
            var ax = LandblockToEW(Landcell >> 16 << 16, Frame.Origin.X);
            var bx = LandblockToEW(position.Landcell >> 16 << 16, position.Frame.Origin.X);
            var ay = LandblockToNS(Landcell >> 16 << 16, Frame.Origin.Y);
            var by = LandblockToNS(position.Landcell >> 16 << 16, position.Frame.Origin.Y);
            var az = Frame.Origin.Z;
            var bz = position.Frame.Origin.Z;
            var dist = (float)Math.Sqrt(Math.Pow(Math.Abs(ax - bx), 2) + Math.Pow(Math.Abs(ay - by), 2) + Math.Pow(Math.Abs(az - bz), 2));

            return (float)(dist * C);
        }

        public static uint GetLandblockFromCoordinates(float EW, float NS) {
            return GetLandblockFromCoordinates((double)EW, (double)NS);
        }

        public static uint GetLandblockFromCoordinates(double EW, double NS) {
            uint x = (uint)Math.Floor((EW + MaxCoord) * C / 192f);
            uint y = (uint)Math.Floor((NS + MaxCoord) * C / 192f);
            return ((x << 8) + y) << 16;
        }

        public static int LandblockXDifference(uint originLandblock, uint landblock) {
            var olbx = originLandblock >> 24;
            var lbx = landblock >> 24;

            return (int)(lbx - olbx) * 192;
        }

        public static int LandblockYDifference(uint originLandblock, uint landblock) {
            var olby = originLandblock << 8 >> 24;
            var lby = landblock << 8 >> 24;

            return (int)(lby - olby) * 192;
        }

        public static float NSToLandblock(uint landcell, float ns) {
            return (float)(((ns + MaxCoord) * C) % 192f);
        }

        public static float EWToLandblock(uint landcell, float ew) {
            return (float)(((ew + MaxCoord) * C) % 192f);
        }

        internal static double RelativeAngleOffset(double originAngle, double offsetAngle) {
            var diff = (originAngle - offsetAngle) - 180.0;
            diff /= 360.0;
            return ((diff - Math.Floor(diff)) * 360.0) - 180.0;
        }

        public static float LandblockToNS(uint landcell, float yOffset) {
            var y = ((landcell >> 16) & 0xFF) * 192f + yOffset;
            return (float)((y / C) - MaxCoord);
        }

        public static float LandblockToEW(uint landcell, float xOffset) {
            var x = (landcell >> 24) * 192f + xOffset;
            return (float)((x / C) - MaxCoord);
        }

        public override string ToString() {
            return $"0x{Landcell:X8} [{Frame.Origin.X} {Frame.Origin.Y} {Frame.Origin.Z}] {Frame.Orientation.W} {Frame.Orientation.X} {Frame.Orientation.Y} {Frame.Orientation.Z}";
        }
    }
}
