﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Some skills use a formula to boost the level based on attributes. If `UseFormula` is
    /// false the skill has no formula boost. These formulas are read from the portal.dat
    /// 
    /// The formula is `(Attribute1 + Attribute2) / Divisor`.
    /// 
    /// For example, the formula for Melee Defense is `(Coordination + Quickness) / 3`
    /// 
    /// If HasAttribute2 is false, only Attribute1 should be used.
    /// </summary>
    public class SkillFormula {
        /// <summary>
        /// Wether or not to use this formula when calculating the skill total
        /// </summary>
        public bool UseFormula { get; }

        /// <summary>
        /// Used for dividing the results of the attribute additions
        /// </summary>
        public float Divisor { get; }
        
        /// <summary>
        /// The first attribute this formula uses
        /// </summary>
        public AttributeId Attribute1 { get; }

        /// <summary>
        /// The second attribute this formula uses. Check HasAttribute2 to see if this should be used
        /// </summary>
        public AttributeId Attribute2 { get; }

        /// <summary>
        /// True if this formula uses Attribute2
        /// </summary>
        public bool HasAttribute2 => Attribute2 == 0;

        /// <summary>
        /// Initialize a new formula
        /// </summary>
        /// <param name="useFormula">Wether or not to use this formula when calculating the skill total</param>
        /// <param name="divisor">Used for dividing the results of the attribute additions</param>
        /// <param name="attribute1">The first attribute this formula uses</param>
        /// <param name="attribute2">The second attribute this formula uses. This is added to the first if it is not 0</param>
        internal SkillFormula(bool useFormula, uint divisor, AttributeId attribute1, AttributeId attribute2) {
            UseFormula = useFormula;
            Divisor = divisor;
            Attribute1 = attribute1;
            Attribute2 = attribute2;
        }
    }
}
