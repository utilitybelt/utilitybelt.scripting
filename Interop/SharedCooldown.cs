﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Types;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Item use cooldowns
    /// </summary>
    public class SharedCooldown {
        /// <summary>
        /// The full layered id of this cooldown
        /// </summary>
        public LayeredSpellId LayeredId { get; }

        /// <summary>
        /// The shared id of this cooldown. Get an objects SharedCooldownId with IntId.SharedCooldown
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// The id of the world object that triggered this cooldown
        /// </summary>
        public uint ObjectId { get; internal set; }

        /// <summary>
        /// The time in seconds that the cooldown has been active before we heard about
        /// it from the server.
        /// </summary>
        public double StartTime { get; internal set; }

        /// <summary>
        /// The total duration in seconds this cooldown should last for
        /// </summary>
        public double Duration { get; internal set; }

        /// <summary>
        /// The datetime that the client recieved this cooldown from the server.
        /// </summary>
        public DateTime ClientReceivedAt { get; internal set; }

        /// <summary>
        /// The datetime this cooldown will expire. All times are in UTC.
        /// Formula is: (Duration &lt; 0) ? DateTime.MaxValue : ClientReceivedAt + TimeSpan.FromSeconds(Duration) - TimeSpan.FromSeconds(StartTime)
        /// </summary>
        public DateTime ExpiresAt => (Duration < 0) ? DateTime.MaxValue : ClientReceivedAt + TimeSpan.FromSeconds(Duration) - TimeSpan.FromSeconds(StartTime);

        internal SharedCooldown(LayeredSpellId layeredId, uint objectId, double duration, double startTime) {
            Id = (int)(layeredId.FullId << 20 >> 20);
            LayeredId = layeredId;
            ObjectId = objectId;
            ClientReceivedAt = DateTime.UtcNow;
            Duration = duration;
            StartTime = startTime;
        }

        internal static SharedCooldown FromMessage(UtilityBelt.Common.Messages.Types.Enchantment enchantmentData) {
            return new SharedCooldown(enchantmentData.Id, enchantmentData.Caster, enchantmentData.Duration, enchantmentData.StartTime);
        }
    }
}
