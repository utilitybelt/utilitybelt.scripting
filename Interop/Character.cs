﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about the current character. This is the class that scripts get access to at `game.character`.
    /// </summary>
    public class Character {
        private ScriptManager _manager;

        #region public events
        /// <summary>
        /// Fired when portal space is entered (not on login, so the first event is [OnPortalSpaceExited](xref:UBScript.Interop.Character#UBScript_Interop_Character_OnPortalSpaceExited))
        /// </summary>
        public event EventHandler<EventArgs> OnPortalSpaceEntered;

        /// <summary>
        /// Fired when exiting portal space. (this fires for the first time after logging in, while exiting login portal space)
        /// </summary>
        public event EventHandler<EventArgs> OnPortalSpaceExited;

        /// <summary>
        /// Fired when object cooldowns change
        /// </summary>
        public event EventHandler<SharedCooldownsChangedEventArgs> OnSharedCooldownsChanged;

        /// <summary>
        /// Fired when active enchantments change
        /// </summary>
        public event EventHandler<EnchantmentsChangedEventArgs> OnEnchantmentsChanged;

        /// <summary>
        /// Fired when one of your vitals current value changes.
        /// </summary>
        public event EventHandler<VitalChangedEventArgs> OnVitalChanged;

        /// <summary>
        /// Fired when your total experience changes
        /// </summary>
        public event EventHandler<TotalExperienceChangedEventArgs> OnTotalExperienceChanged;

        /// <summary>
        /// Fired when your total luminance
        /// </summary>
        public event EventHandler<AvailableLuminanceChangedEventArgs> OnAvailableLuminanceChanged;

        /// <summary>
        /// Fired when your character levels up
        /// </summary>
        public event EventHandler<LevelChangedEventArgs> OnLevelChanged;

        /// <summary>
        /// Fired when your character's vitae changes
        /// </summary>
        public event EventHandler<VitaeChangedEventArgs> OnVitaeChanged;

        /// <summary>
        /// Fired any time your character attempts to cast a spell
        /// </summary>
        public event EventHandler<CastSpellAttemptEventArgs> OnCastSpellAttempt;

        /// <summary>
        /// Fired when your character dies
        /// </summary>
        public event EventHandler<DeathEventArgs> OnDeath;
        #endregion // public events

        #region public properties
        /// <summary>
        /// The id of the currently logged in character
        /// </summary>
        public uint Id => _manager.GameState.CharacterId;

        /// <summary>
        /// Character Options
        /// </summary>
        public CharacterOptions1 Options1 => _manager.GameState.Character.Options1;

        /// <summary>
        /// Information about the currently logged in character's fellowship state
        /// </summary>
        public Fellowship Fellowship => _manager.GameState.Character.Fellowship;

        /// <summary>
        /// Wether or not the currently logged in character is in portal space
        /// </summary>
        public bool InPortalSpace => _manager.GameState.Character.InPortalSpace;

        /// <summary>
        /// Information about the currently logged in character's spellbook. (known spells, spell helpers, etc)
        /// </summary>
        public SpellBook SpellBook => _manager.GameState.Character.SpellBook;

        /// <summary>
        /// Information about the currently logged in character's trade state
        /// </summary>
        public Trade Trade => _manager.GameState.Character.Trade;

        /// <summary>
        /// Information about the currently logged in character's animation state
        /// </summary>
        public AnimTracker AnimTracker => _manager.GameState.Character?.AnimTracker;

        /// <summary>
        /// Information about the currently logged in character's busy state
        /// </summary>
        public BusyTracker BusyTracker => _manager.GameState.Character?.BusyTracker;

        /// <summary>
        /// Information about the currently logged in character's allegiance
        /// </summary>
        public Allegiance Allegiance => _manager.GameState.Character?.Allegiance;

        /// <summary>
        /// Character's current vitae
        /// 1 = no vitea, 0.95 = 5% vitae
        /// </summary>
        public float Vitae => _manager.GameState.Character?.Vitae ?? 0;

        /// <summary>
        /// The weenie of the currently logged in character
        /// </summary>
        public WorldObject Weenie => _manager.GameState.Character?.Weenie;

        /// <summary>
        /// A list of all objects in your inventory, pulls items from containers as well. (This excludes equipped items and actual containers)
        /// </summary>
        public IList<WorldObject> Inventory => Weenie.AllItems.ToList();

        /// <summary>
        /// List of child container objects, including foci
        /// </summary>
        public IList<WorldObject> Containers => Weenie.Containers.ToList();

        /// <summary>
        /// List of objects equipped to this character.
        /// </summary>
        public IList<WorldObject> Equipment => Weenie.Equipment.ToList();

        /// <summary>
        /// The stance the character is in
        /// </summary>
        public MotionStance Stance => Weenie.Stance;

        /// <summary>
        /// The combat mode the character is in
        /// </summary>
        public CombatMode CombatMode => Weenie.CombatMode;

        /// <summary>
        /// Your current burden units. Your max burden units (for 100% burden) uses the formula Strength * 150.
        /// </summary>
        public int BurdenUnits => Weenie.Burden;

        /// <summary>
        /// The maximum burden units you can hold before becoming overburdened (100% burden).
        /// </summary>
        public int MaxBurdenUnits {
            get {
                var burdenCapacity = Weenie.Attributes[AttributeId.Strength].Current * 150f;
                burdenCapacity = burdenCapacity + (burdenCapacity * (Weenie.Value(IntId.AugmentationIncreasedCarryingCapacity) * 0.2f));
                return (int)burdenCapacity;
            }
        }
        #endregion // public properties

        /// <summary>
        /// Create a new character instance for a script. Scripts should not use this, they should use `game.character` to
        /// access the current character object for that script.
        /// </summary>
        /// <param name="manager"></param>
        internal Character(ScriptManager manager) {
            _manager = manager;

            _manager.GameState.Character.OnSharedCooldownsChanged += Character_OnSharedCooldownsChanged;
            _manager.GameState.Character.OnEnchantmentsChanged += Character_OnEnchantmentsChanged;
            _manager.GameState.Character.OnPortalSpaceEntered += Character_OnPortalSpaceEntered;
            _manager.GameState.Character.OnPortalSpaceExited += Character_OnPortalSpaceExited;
            _manager.GameState.Character.OnAvailableLuminanceChanged += Character_OnAvailableLuminanceChanged;
            _manager.GameState.Character.OnCastSpellAttempt += Character_OnCastSpellAttempt;
            _manager.GameState.Character.OnDeath += Character_OnDeath;
            _manager.GameState.Character.OnLevelChanged += Character_OnLevelChanged;
            _manager.GameState.Character.OnTotalExperienceChanged += Character_OnTotalExperienceChanged;
            _manager.GameState.Character.OnVitaeChanged += Character_OnVitaeChanged;
            _manager.GameState.Character.OnVitalChanged += Character_OnVitalChanged;
        }

        #region event forwarders
        private void Character_OnVitalChanged(object sender, VitalChangedEventArgs e) {
            OnVitalChanged?.InvokeSafely(this, e);
        }

        private void Character_OnVitaeChanged(object sender, VitaeChangedEventArgs e) {
            OnVitaeChanged?.InvokeSafely(this, e);
        }

        private void Character_OnTotalExperienceChanged(object sender, TotalExperienceChangedEventArgs e) {
            OnTotalExperienceChanged?.InvokeSafely(this, e);
        }

        private void Character_OnLevelChanged(object sender, LevelChangedEventArgs e) {
            OnLevelChanged?.InvokeSafely(this, e);
        }

        private void Character_OnDeath(object sender, DeathEventArgs e) {
            OnDeath?.InvokeSafely(this, e);
        }

        private void Character_OnCastSpellAttempt(object sender, CastSpellAttemptEventArgs e) {
            OnCastSpellAttempt?.InvokeSafely(this, e);
        }

        private void Character_OnAvailableLuminanceChanged(object sender, AvailableLuminanceChangedEventArgs e) {
            OnAvailableLuminanceChanged?.InvokeSafely(this, e);
        }

        private void Character_OnPortalSpaceExited(object sender, EventArgs e) {
            OnPortalSpaceExited?.InvokeSafely(this, e);
        }

        private void Character_OnPortalSpaceEntered(object sender, EventArgs e) {
            OnPortalSpaceEntered?.InvokeSafely(this, e);
        }

        private void Character_OnEnchantmentsChanged(object sender, EnchantmentsChangedEventArgs e) {
            OnEnchantmentsChanged?.InvokeSafely(this, e);
        }

        private void Character_OnSharedCooldownsChanged(object sender, SharedCooldownsChangedEventArgs e) {
            OnSharedCooldownsChanged?.InvokeSafely(this, e);
        }
        #endregion // event forwarders

        #region public api
        /// <summary>
        /// A list of all shared cooldowns on this character.
        /// </summary>
        /// <returns>A list of enchantments</returns>
        public IList<SharedCooldown> SharedCooldowns() => _manager.GameState.Character.SharedCooldowns;

        /// <summary>
        /// A list of *all* enchantments on this character. This includes enchantments that may be overriden by other enchantments.
        /// </summary>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> AllEnchantments() => _manager.GameState.Character.AllEnchantments;

        /// <summary>
        /// A list of active enchantments on this character. Does not include overlapping enchantments
        /// </summary>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> ActiveEnchantments() => _manager.GameState.Character.GetActiveEnchantments();

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified attribute type.
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(AttributeId attributeId) => _manager.GameState.Character.GetActiveEnchantments(attributeId);

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified skill type.
        /// </summary>
        /// <param name="skillId"></param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(SkillId skillId) => _manager.GameState.Character.GetActiveEnchantments(skillId);

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified vital
        /// </summary>
        /// <param name="vitalId"></param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(VitalId vitalId) => _manager.GameState.Character.GetActiveEnchantments(vitalId);

        /// <summary>
        /// Get the enchantments additive modifier for the specified attribute based on the characters active enchantments
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(AttributeId attributeId) {
            return _manager.GameState.Character.GetEnchantmentsAdditiveModifier(attributeId);
        }

        /// <summary>
        /// Get the enchantments additive modifier for the specified skill based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(SkillId type) {
            return _manager.GameState.Character.GetEnchantmentsAdditiveModifier(type);
        }

        /// <summary>
        /// Get the enchantments additive modifier for the specified vital based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(VitalId type) {
            return _manager.GameState.Character.GetEnchantmentsAdditiveModifier(type);
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified attribute based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(AttributeId type) {
            return _manager.GameState.Character.GetEnchantmentsMultiplierModifier(type);
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified skill based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(SkillId type) {
            return _manager.GameState.Character.GetEnchantmentsMultiplierModifier(type);
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified vital based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(VitalId type) {
            return _manager.GameState.Character.GetEnchantmentsMultiplierModifier(type);
        }

        /// <summary>
        /// Get the character's current skill level in the specified magic school. Includes active enchaments / vitae / augmentations / etc.
        /// </summary>
        /// <param name="school"></param>
        /// <returns></returns>
        public int GetMagicSkill(MagicSchool school) {
            return _manager.GameState.Character.GetMagicSkill(school);
        }

        /// <summary>
        /// Count the number of items in your inventory that match the specified object class. Stack sizes are used in this count.
        /// This includes items in containers as well as the main backpack, but not equipment.
        /// </summary>
        /// <param name="objectClass">The object class to filter by</param>
        /// <returns>The total item count (with stack sizes)</returns>
        public int GetInventoryCount(ObjectClass objectClass) {
            return Inventory.Where(wo => wo.ObjectClass == objectClass).Sum(wo => wo.Value(IntId.StackSize, 1));
        }

        /// <summary>
        /// Count the number of items in your inventory that match the specified name. Stack sizes are used in this count.
        /// This includes items in containers as well as the main backpack, but not equipment.
        /// </summary>
        /// <param name="name">The name to filter by</param>
        /// <returns>The total item count (with stack sizes)</returns>
        public int GetInventoryCount(string name) {
            return Inventory.Where(wo => wo.Name == name).Sum(wo => wo.Value(IntId.StackSize, 1));
        }

        /// <summary>
        /// Count the number of items in your inventory that match the specified filter function. Stack sizes are used in this count.
        /// This includes items in containers as well as the main backpack, but not equipment.
        /// </summary>
        /// <param name="matchFunction">The function to filter by</param>
        /// <returns>The total item count (with stack sizes)</returns>
        public int GetInventoryCount(Func<WorldObject, bool> matchFunction) {
            return Inventory.Where(matchFunction).Sum(wo => wo.Value(IntId.StackSize, 1));
        }

        /// <summary>
        /// Returns the first inventory item matching the specified ObjectClass. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="objectClass">The ObjectClass to look for</param>
        /// <returns>The first WorldObject in your inventory matching the specified ObjectClass, or null if none were found</returns>
        public WorldObject GetFirstInventory(ObjectClass objectClass) {
            return Inventory.FirstOrDefault(wo => wo.ObjectClass == objectClass);
        }

        /// <summary>
        /// Returns the first inventory item matching the specified name. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="name">The item name to look for</param>
        /// <returns>The first WorldObject in your inventory matching the specified name, or null if none were found</returns>
        public WorldObject GetFirstInventory(string name) {
            return Inventory.FirstOrDefault(wo => wo.Name == name);
        }

        /// <summary>
        /// Returns the first inventory item matching the specified filter function. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="matchFunction">The function to filter by</param>
        /// <returns>The first WorldObject in your inventory matching the specified filter function, or null if none were found</returns>
        public WorldObject GetFirstInventory(Func<WorldObject, bool> matchFunction) {
            return Inventory.FirstOrDefault(matchFunction);
        }

        /// <summary>
        /// Returns all inventory items matching the specified ObjectClass. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="objectClass">The ObjectClass to look for</param>
        /// <returns>All WorldObjects in your inventory matching the specified ObjectClass, or null if none were found</returns>
        public IList<WorldObject> GetInventory(ObjectClass objectClass) {
            return Inventory.Where(wo => wo.ObjectClass == objectClass).ToList();
        }

        /// <summary>
        /// Returns all inventory items matching the specified name. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="name">The item name to look for</param>
        /// <returns>All WorldObjects in your inventory matching the specified name, or null if none were found</returns>
        public IList<WorldObject> GetInventory(string name) {
            return Inventory.Where(wo => wo.Name == name).ToList();
        }

        /// <summary>
        /// Returns all inventory items matching the specified filter function. This searches the main backpack and all sub containers
        /// for items, but not containers themselves or equipment.
        /// </summary>
        /// <param name="matchFunction">The function to filter by</param>
        /// <returns>All WorldObjects in your inventory matching the specified filter function, or null if none were found</returns>
        public IList<WorldObject> GetInventory(Func<WorldObject, bool> matchFunction) {
            return Inventory.Where(matchFunction).ToList();
        }
        #endregion // public api

        internal void Dispose() {
            if (_manager?.GameState?.Character == null)
                return;

            _manager.GameState.Character.OnSharedCooldownsChanged -= Character_OnSharedCooldownsChanged;
            _manager.GameState.Character.OnEnchantmentsChanged -= Character_OnEnchantmentsChanged;
            _manager.GameState.Character.OnPortalSpaceEntered -= Character_OnPortalSpaceEntered;
            _manager.GameState.Character.OnPortalSpaceExited -= Character_OnPortalSpaceExited;
            _manager.GameState.Character.OnAvailableLuminanceChanged -= Character_OnAvailableLuminanceChanged;
            _manager.GameState.Character.OnCastSpellAttempt -= Character_OnCastSpellAttempt;
            _manager.GameState.Character.OnDeath -= Character_OnDeath;
            _manager.GameState.Character.OnLevelChanged -= Character_OnLevelChanged;
            _manager.GameState.Character.OnTotalExperienceChanged -= Character_OnTotalExperienceChanged;
            _manager.GameState.Character.OnVitaeChanged -= Character_OnVitaeChanged;
            _manager.GameState.Character.OnVitalChanged -= Character_OnVitalChanged;
        }
    }
}