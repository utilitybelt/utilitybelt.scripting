﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using static System.Collections.Specialized.BitVector32;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// The action queue handles queue and running actions based on priority. Using the queue system
    /// allows for multiple scripts to coexist.
    /// </summary>
    public class ActionQueue : IDisposable {
        private List<QueueAction> _queue = new List<QueueAction>();
        private List<QueueAction> _immediateQueue = new List<QueueAction>();
        private readonly ScriptManager _manager;
        private readonly GameState _gamestate;
        private bool isRunningAction = false;
        private bool isRunningImmediateAction = false;
        private DateTime lastAction = DateTime.UtcNow;
        private object _lock = new();

        /// <summary>
        /// The current queue. This includes all [QueueAction](xref:UBScript.Interop.QueueAction)'s that are not of type
        /// [ActionType.Immediate](xref:UBScript.Enums.ActionType).
        /// </summary>
        public IEnumerable<QueueAction> Queue => _queue;

        /// <summary>
        /// The current immediate queue. This includes all [QueueAction](xref:UBScript.Interop.QueueAction)'s that are of type
        /// [ActionType.Immediate](xref:UBScript.Enums.ActionType).
        /// </summary>
        public IEnumerable<QueueAction> ImmediateQueue => _immediateQueue;

        internal ActionQueue(ScriptManager manager, GameState gamestate) {
            _manager = manager;
            _gamestate = gamestate;
        }

        /// <summary>
        /// Check if the specified action is in the queue. This checks both the normal queue and the immediate queue.
        /// </summary>
        /// <param name="action">The action to check</param>
        /// <returns>True if the action is already queued</returns>
        public bool Contains(QueueAction action) {
            return _queue.Contains(action) || _immediateQueue.Contains(action);
        }

        /// <summary>
        /// Add the specified action to the queue. Actions that already exist in the queue will not be added.
        /// </summary>
        /// <param name="action">The action to add</param>
        /// <returns>True if the action was added</returns>
        public bool Add(QueueAction action) {
            if (action.ActionType == Enums.ActionType.Immediate) {
                return InsertInQueue(_immediateQueue, action);
            }
            else {
                return InsertInQueue(_queue, action);
            }
        }

        /// <summary>
        /// Remove the specified action from the queue. This will cause it to fail with ActionError.Cancelled
        /// </summary>
        /// <param name="action">The action to remove</param>
        public void Remove(QueueAction action) {
            action.SetPermanentResult(Enums.ActionError.Cancelled);
            lock (_lock) {
                _queue.Remove(action);
                _immediateQueue.Remove(action);
            }
        }

        public void Clear() {
            var items = _queue.ToArray();
            foreach (var item in items) {
                item.SetPermanentResult(Enums.ActionError.Cancelled);
            }
            items = _immediateQueue.ToArray();
            foreach (var item in items) {
                item.SetPermanentResult(Enums.ActionError.Cancelled);
            }
            _queue.Clear();
            _immediateQueue.Clear();
        }

        private bool InsertInQueue(List<QueueAction> queue, QueueAction action) {
            var i = 0;
            var insertIndex = -1;

            lock (_lock) {
                foreach (var qAction in queue) {
                    if (action.Equals(qAction)) {
                        _manager.Resolve<ILogger>()?.LogWarning($"Action already exists in queue: {qAction.Name}");
                        return false;
                    }
                    else if (insertIndex == -1 && action.Priority > qAction.Priority) {
                        insertIndex = i;
                    }
                    i++;
                }

                queue.Insert(insertIndex >= 0 ? insertIndex : queue.Count, action);
            }

            _manager.Resolve<ILogger>()?.LogDebug($"Added action to queue@{(insertIndex >= 0 ? insertIndex : queue.Count)}: {(action.ActionType == Enums.ActionType.Immediate ? "(Immediate)" : "")} {action.Name}");

            return true;
        }

        internal void Run() {
            while (true) {
                var immediateActions = _immediateQueue.ToArray();
                foreach (var action in immediateActions) {
                    if (action != null) {
                        if (action.NeedsInit) {
                            action.Init(_manager);
                        }

                        action.Update();

                        if (action.IsFinished) {
                            lock (_lock) {
                                _immediateQueue.Remove(action);
                            }
                            continue;
                        }
                    }
                }

                if (!isRunningAction && (_gamestate.Character != null && _gamestate.Character.IsBusy())) {
                    return;
                }

                if (_queue.Count > 0 && DateTime.UtcNow - lastAction > TimeSpan.FromMilliseconds(1)) {
                    var action = _queue[0];
                    if (action != null) {
                        if (action.NeedsInit) {
                            action.Init(_manager);
                            isRunningAction = true;
                        }

                        action.Update();

                        if (action.IsFinished) {
                            isRunningAction = false;
                            lock (_lock) {
                                _queue.Remove(action);
                            }
                            lastAction = DateTime.UtcNow;
                            continue;
                        }
                    }
                }

                break;
            }
        }

        public void Dispose() {

        }
    }
}
