﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Lib;
using ACE.DatLoader;
using ACE.DatLoader.Entity;

namespace UtilityBelt.Scripting.Interop {
    public class Skill {
        private ScriptManager _manager => ScriptManager.Instance;

        private WorldObject _weenie;
        private CharacterState _characterState => _manager.GameState.Character;
        private PortalDatDatabase _portalDat;

        /// <summary>
        /// The skill type this is
        /// </summary>
        public virtual SkillId Type { get; set; }

        /// <summary>
        /// How many points this skill has been raised.
        /// </summary>
        public virtual uint PointsRaised { get; set; }

        public virtual uint AdjustXP { get; set; }

        /// <summary>
        /// Total experience already spent on this skill
        /// </summary>
        public virtual uint Experience { get; set; }

        /// <summary>
        /// The level this skill intialized at.
        /// </summary>
        public virtual uint InitLevel { get; set; }

        public virtual uint ResistanceOfLastCheck { get; set; }

        public virtual float LastUsedTime { get; set; }

        private SkillTrainingType _training = SkillTrainingType.Unusable;
        /// <summary>
        /// The training type of this skill.
        /// </summary>
        public virtual SkillTrainingType Training {
            get => _training;
            set {
                if (_portalDat.SkillTable.SkillBaseHash.TryGetValue((uint)Type, out SkillBase skillBase)) {
                    if ((uint)value >= skillBase.MinLevel) {
                        _training = value;
                    }
                    else {
                        _training = SkillTrainingType.Unusable;
                    }
                }
                else {
                    _training = SkillTrainingType.Unusable;
                }
            }
        }

        /// <summary>
        /// The formula for calculating this skill total
        /// </summary>
        public virtual SkillFormula Formula { get; }

        /// <summary>
        /// The base skill value. (no buffs, includes formula bonus ie missile weapons gets base coordination/2)
        /// </summary>
        public virtual int Base {
            get {
                // logic from ACE
                var _base = (int)(InitLevel + PointsRaised);

                if (Training > SkillTrainingType.Unusable && Formula.UseFormula) {
                    var attrBonus = _weenie.Attributes[Formula.Attribute1].Base;
                    if (Formula.Attribute2 != 0) {
                        attrBonus += _weenie.Attributes[Formula.Attribute2].Base;
                    }

                    _base += (int)Math.Round(((float)attrBonus / Formula.Divisor));
                }

                _base += _weenie.Value(IntId.LumAugAllSkills);

                if (MeleeSkills.Contains(Type))
                    _base += _weenie.Value(IntId.AugmentationSkilledMelee) * 10;
                else if (MissileSkills.Contains(Type))
                    _base += _weenie.Value(IntId.AugmentationSkilledMissile) * 10;
                else if (MagicSkills.Contains(Type))
                    _base += _weenie.Value(IntId.AugmentationSkilledMagic) * 10;

                if (Training >= SkillTrainingType.Trained)
                    _base += _weenie.Value(IntId.Enlightenment);
                return _base;
            }
        }

        /// <summary>
        /// Effective skill. Includes buffs/debuffs/vitae/augmentations.
        /// </summary>
        public virtual int Current {
            get {
                // logic from ACE
                var effectiveBase = (int)(InitLevel + PointsRaised);
                if (Training > SkillTrainingType.Unusable && Formula.UseFormula) {
                    var attrBonus = _weenie.Attributes[Formula.Attribute1].Current;
                    if (Formula.Attribute2 != 0) {
                        attrBonus += _weenie.Attributes[Formula.Attribute2].Current;
                    }

                    effectiveBase += (int)Math.Round(((float)attrBonus / Formula.Divisor));
                }

                effectiveBase += _weenie.Value(IntId.LumAugAllSkills);

                if (MeleeSkills.Contains(Type))
                    effectiveBase += _weenie.Value(IntId.AugmentationSkilledMelee) * 10;
                else if (MissileSkills.Contains(Type))
                    effectiveBase += _weenie.Value(IntId.AugmentationSkilledMissile) * 10;
                else if (MagicSkills.Contains(Type))
                    effectiveBase += _weenie.Value(IntId.AugmentationSkilledMagic) * 10;

                var multiplier = _characterState.GetEnchantmentsMultiplierModifier(Type);
                var fTotal = effectiveBase * multiplier;

                if (_characterState.Vitae < 1.0f) {
                    fTotal *= _characterState.Vitae;
                }

                fTotal += _weenie.Value(IntId.AugmentationJackOfAllTrades) * 5;

                if (Training == SkillTrainingType.Specialized)
                    fTotal += _weenie.Value(IntId.LumAugSkilledSpec) * 2;

                var additives = _characterState.GetEnchantmentsAdditiveModifier(Type);
                return (int)Math.Max(Math.Round(fTotal + additives), 0);
            }
        }

        public SkillBase Dat { get; set; }

        public SkillTrainingType MinTraining => AlwaysTrained.Contains(Type) ? SkillTrainingType.Trained : SkillTrainingType.Unusable;

        public SkillTrainingType MaxTraining => Type == SkillId.Salvaging ? SkillTrainingType.Trained : SkillTrainingType.Specialized;
        
        public uint CostToIncreaseTraining {
            get {
                if (Type == SkillId.Salvaging)
                    return 0;
                else if (Training == SkillTrainingType.Specialized)
                    return 0;
                else if (Training == SkillTrainingType.Trained)
                    return (uint)Dat.UpgradeCostFromTrainedToSpecialized;
                else
                    return (uint)Dat.TrainedCost;
            }
        }
        protected SkillCG HeritageSkill { get; set; } = null;

        public uint TrainedCost => (uint)(HeritageSkill == null ? Dat.TrainedCost : HeritageSkill.NormalCost);
        public uint SpecializedCost => (uint)(HeritageSkill == null ? Dat.UpgradeCostFromTrainedToSpecialized : HeritageSkill.PrimaryCost);

        /// <summary>
        /// True if this skill can be lowered
        /// </summary>
        public bool CanLower => _weenie.Id == _manager.GameState.CharacterId && Training > MinTraining;

        /// <summary>
        /// True if this skill can be raised
        /// </summary>
        public bool CanRaise => _weenie.Id == _manager.GameState.CharacterId && Training < MaxTraining;

        public uint CreditsSpent {
            get {
                if (Training == MinTraining)
                    return 0;
                else if (Training == SkillTrainingType.Specialized)
                    return TrainedCost + SpecializedCost;
                else
                    return TrainedCost;
            }
        }


        internal Skill(SkillId skillId, WorldObject weenie) {
            Type = skillId;
            _weenie = weenie;
            _portalDat = _manager.Resolve<PortalDatDatabase>();

            if (_portalDat.SkillTable.SkillBaseHash.TryGetValue((uint)skillId, out SkillBase skillBase)) {
                Dat = skillBase;
                var formula = skillBase.Formula;
                Formula = new SkillFormula(formula.X > 0 ? true : false, formula.Z, (AttributeId)formula.Attr1, (AttributeId)formula.Attr2);
            }
            else {
                Formula = new SkillFormula(false, 1, 0, 0);
            }
        }

        /// <summary>
        /// A list of melee skills, including legacy skills
        /// </summary>
        public static List<SkillId> MeleeSkills = new List<SkillId>()
        {
            SkillId.LightWeapons,
            SkillId.HeavyWeapons,
            SkillId.FinesseWeapons,
            SkillId.DualWield,
            SkillId.TwoHandedCombat,

            // legacy
            SkillId.Axe,
            SkillId.Dagger,
            SkillId.Mace,
            SkillId.Spear,
            SkillId.Staff,
            SkillId.Sword,
            SkillId.UnarmedCombat
        };

        /// <summary>
        /// A list of missile skills, including legacy skills
        /// </summary>
        public static List<SkillId> MissileSkills = new List<SkillId>()
        {
            SkillId.MissleWeapons,

            // legacy
            SkillId.Bow,
            SkillId.Crossbow,
            SkillId.Sling,
            SkillId.ThrownWeapons
        };

        /// <summary>
        /// A list of magic skills
        /// </summary>
        public static List<SkillId> MagicSkills = new List<SkillId>()
        {
            SkillId.CreatureEnchantment,
            SkillId.ItemEnchantment,
            SkillId.LifeMagic,
            SkillId.VoidMagic,
            SkillId.WarMagic
        };

        /// <summary>
        /// A list of skills that are always trained
        /// </summary>
        public static List<SkillId> AlwaysTrained = new List<SkillId>()
        {
            SkillId.ArcaneLore,
            SkillId.Jump,
            SkillId.Loyalty,
            SkillId.MagicDefense,
            SkillId.Run,
            SkillId.Salvaging
        };

        /// <summary>
        /// Skills that require augmentation to specialize.
        /// </summary>
        public static List<SkillId> AugSpecSkills = new List<SkillId>()
        {
            SkillId.ArmorTinkering,
            SkillId.ItemTinkering,
            SkillId.MagicItemTinkering,
            SkillId.WeaponTinkering,
            SkillId.Salvaging
        };
    }
}
