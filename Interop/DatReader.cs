﻿using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Environment = ACE.DatLoader.FileTypes.Environment;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Access information in the game .dat files
    /// </summary>
    /*
    public class DatReader {
        private readonly ScriptManager _manager;
        private readonly PortalDatDatabase _portalDat;
        private readonly CellDatDatabase _cellDat;
        private readonly LanguageDatDatabase _languageDat;

        public DatReader() {
            _manager = ScriptManager.Instance;
            _portalDat = _manager.Resolve<PortalDatDatabase>();
            _cellDat = _manager.Resolve<CellDatDatabase>(); ;
            _languageDat = _manager.Resolve<LanguageDatDatabase>();
        }

        public Animation GetAnimation(uint fileId) {
            return _portalDat.ReadFromDat<Animation>(fileId);
        }

        public BadData GetBadData(uint fileId) {
            return _portalDat.ReadFromDat<BadData>(fileId);
        }

        public CellLandblock GetCellLandblock(uint fileId) {
            return _cellDat.ReadFromDat<CellLandblock>(fileId);
        }

        public CharGen GetCharGen(uint fileId) {
            return _portalDat.ReadFromDat<CharGen>(fileId);
        }

        public ChatPoseTable GetChatPoseTable(uint fileId) {
            return _portalDat.ReadFromDat<ChatPoseTable>(fileId);
        }

        public ClothingTable GetClothingTable(uint fileId) {
            return _portalDat.ReadFromDat<ClothingTable>(fileId);
        }

        public CombatManeuverTable GetCombatManeuverTable(uint fileId) {
            return _portalDat.ReadFromDat<CombatManeuverTable>(fileId);
        }

        public ContractTable GetContractTable(uint fileId) {
            return _portalDat.ReadFromDat<ContractTable>(fileId);
        }

        public DidMapper GetDidMapper(uint fileId) {
            return _portalDat.ReadFromDat<DidMapper>(fileId);
        }

        public DualDidMapper GetDualDidMapper(uint fileId) {
            return _portalDat.ReadFromDat<DualDidMapper>(fileId);
        }

        public EnumMapper GetEnumMapper(uint fileId) {
            return _portalDat.ReadFromDat<EnumMapper>(fileId);
        }

        public EnvCell GetEnvCell(uint fileId) {
            return _cellDat.ReadFromDat<EnvCell>(fileId);
        }

        public Environment GetEnvironment(uint fileId) {
            return _portalDat.ReadFromDat<Environment>(fileId);
        }

        public Font GetFont(uint fileId) {
            return _portalDat.ReadFromDat<Font>(fileId);
        }

        public GeneratorTable GetGeneratorTable(uint fileId) {
            return _portalDat.ReadFromDat<GeneratorTable>(fileId);
        }

        public GfxObj GetGfxObj(uint fileId) {
            return _portalDat.ReadFromDat<GfxObj>(fileId);
        }

        public GfxObjDegradeInfo GetGfxObjDegradeInfo(uint fileId) {
            return _portalDat.ReadFromDat<GfxObjDegradeInfo>(fileId);
        }

        public Iteration GetPortalDatIteration(uint fileId) {
            return _portalDat.ReadFromDat<Iteration>(fileId);
        }

        public Iteration GetCellDatIteration(uint fileId) {
            return _portalDat.ReadFromDat<Iteration>(fileId);
        }

        public LandblockInfo GetLandblockInfo(uint fileId) {
            return _cellDat.ReadFromDat<LandblockInfo>(fileId);
        }

        public LanguageInfo GetLanguageInfo(uint fileId) {
            return _languageDat.ReadFromDat<LanguageInfo>(fileId);
        }

        public LanguageString GetLanguageString(uint fileId) {
            return _portalDat.ReadFromDat<LanguageString>(fileId);
        }

        public MotionTable GetMotionTable(uint fileId) {
            return _portalDat.ReadFromDat<MotionTable>(fileId);
        }

        public NameFilterTable GetNameFilterTable(uint fileId) {
            return _portalDat.ReadFromDat<NameFilterTable>(fileId);
        }

        public Palette GetPalette(uint fileId) {
            return _portalDat.ReadFromDat<Palette>(fileId);
        }

        public PaletteSet GetPaletteSet(uint fileId) {
            return _portalDat.ReadFromDat<PaletteSet>(fileId);
        }

        public ParticleEmitterInfo GetParticleEmitterInfo(uint fileId) {
            return _portalDat.ReadFromDat<ParticleEmitterInfo>(fileId);
        }

        public PhysicsScript GetPhysicsScript(uint fileId) {
            return _portalDat.ReadFromDat<PhysicsScript>(fileId);
        }

        public PhysicsScriptTable GetPhysicsScriptTable(uint fileId) {
            return _portalDat.ReadFromDat<PhysicsScriptTable>(fileId);
        }

        public QualityFilter GetQualityFilter(uint fileId) {
            return _portalDat.ReadFromDat<QualityFilter>(fileId);
        }

        public RegionDesc GetRegionDesc(uint fileId = 0x13000000) {
            return _portalDat.ReadFromDat<RegionDesc>(fileId);
        }

        public RenderTexture GetRenderTexture(uint fileId) {
            return _portalDat.ReadFromDat<RenderTexture>(fileId);
        }

        public Scene GetScene(uint fileId) {
            return _portalDat.ReadFromDat<Scene>(fileId);
        }

        public SecondaryAttributeTable GetSecondaryAttributeTable(uint fileId = 0x0E000003) {
            return _portalDat.ReadFromDat<SecondaryAttributeTable>(fileId);
        }

        public SetupModel GetSetupModel(uint fileId) {
            return _portalDat.ReadFromDat<SetupModel>(fileId);
        }

        public SkillTable GetSkillTable(uint fileId = 0x0E000004) {
            return _portalDat.ReadFromDat<SkillTable>(fileId);
        }

        public SoundTable GetSoundTable(uint fileId) {
            return _portalDat.ReadFromDat<SoundTable>(fileId);
        }

        public SpellComponentsTable GetSpellComponentsTable(uint fileId) {
            return _portalDat.ReadFromDat<SpellComponentsTable>(fileId);
        }

        public SpellTable GetSpellTable(uint fileId = 0x0E00000E) {
            return _portalDat.ReadFromDat<SpellTable>(fileId);
        }

        public StringTable GetStringTable(uint fileId = 0x2300000E) {
            return _languageDat.ReadFromDat<StringTable>(fileId);
        }

        public Surface GetSurface(uint fileId) {
            return _portalDat.ReadFromDat<Surface>(fileId);
        }

        public SurfaceTexture GetSurfaceTexture(uint fileId) {
            return _portalDat.ReadFromDat<SurfaceTexture>(fileId);
        }

        public TabooTable GetTabooTable(uint fileId = 0x0E00001E) {
            return _portalDat.ReadFromDat<TabooTable>(fileId);
        }

        public Texture GetTexture(uint fileId) {
            return _portalDat.ReadFromDat<Texture>(fileId);
        }

        public Wave GetWave(uint fileId) {
            return _portalDat.ReadFromDat<Wave>(fileId);
        }

        public XpTable GetXpTable(uint fileId) {
            return _portalDat.ReadFromDat<XpTable>(fileId);
        }
    }
    */
}
