﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Interop {
    public class Capabilities {
        internal static ClientCapability ClientCapabilities;

        /// <summary>
        /// Check if the current game client has the specified capability flag.
        /// </summary>
        /// <param name="capability">The capability to check</param>
        /// <returns>True if the client supports this capability</returns>
        public bool Has(ClientCapability capability) {
            return ClientCapabilities.HasFlag(capability);
        }

        /// <summary>
        /// Get a flag enum of all supported game client capabilities.
        /// </summary>
        /// <returns></returns>
        public ClientCapability Get() {
            return ClientCapabilities;
        }
    }
}
