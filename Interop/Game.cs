﻿using ACE.DatLoader;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using UtilityBelt.Common.Messages;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Lib;
using WattleScript.Interpreter;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Game interface exposed to scripting environments as global `game`. A new game instance is created for each script to
    /// make event / hud cleanup easier in the case that a script doesn't clean up after itself.
    /// </summary>
    public class Game {
        private readonly ScriptManager _manager;
        private World world;

        #region public events
        /// <summary>
        /// Fired when client state changes. See [ClientState](xref:UBScript.Enums.ClientState) for a list of valid states.
        /// </summary>
        public event EventHandler<StateChangedEventArgs> OnStateChanged;

        /// <summary>
        /// Fired when the gamestate ticks. This is roughly once per second
        /// </summary>
        public event EventHandler<EventArgs> OnTick;

        /// <summary>
        /// Fired every frame, during 2D rendering
        /// </summary>
        public virtual event EventHandler<EventArgs> OnRender2D;

        /// <summary>
        /// Fired every frame, during 3D rendering
        /// </summary>
        public virtual event EventHandler<EventArgs> OnRender3D;

        /// <summary>
        /// Fired when the script tied to this game instance is stopped.
        /// </summary>
        public event EventHandler<EventArgs> OnScriptEnd;

        /// <summary>
        /// Fired for every window message. You can optionally eat them to prevent them from hitting imgui / game client.
        /// </summary>
        public virtual event EventHandler<WindowMessageEventArgs> OnWindowMessage;
        #endregion // public events

        #region public properties
        /// <summary>
        /// Access to ac network message events
        /// </summary>
        public MessageHandler Messages { get; private set; }
        /// <summary>
        /// The name of the account this client is connected as
        /// </summary>
        public string AccountName => _manager.GameState.AccountName;

        /// <summary>
        /// Client actions
        /// </summary>
        public ClientActions Actions { get; private set; }

        /// <summary>
        /// Action Queue
        /// </summary>
        public ActionQueue ActionQueue => _manager.GameState.ActionQueue;

        /// <summary>
        /// Information about the currently logged in character. Only valid while logged in, specifically when
        /// `game.State == ClientState.Entering_Game`
        /// </summary>
        public Character Character { get; private set; }

        /// <summary>
        /// The weenie id of the currently logged in character. 0 if not logged in.
        /// </summary>
        public uint CharacterId => _manager.GameState.CharacterId;

        /// <summary>
        /// A list of characters available on the character select screen. This includes characters
        /// pending deletion.
        /// </summary>
        /// <example>
        /// # [lua](#tab/lua)
        /// 
        /// <pre><code language="lua"><![CDATA[
        /// -- loop through available characters on this account, printing their name and id
        /// for charIdentity in game.CharacterSet do
        ///   print("Found available character: ", charIdentity.name, charIdentity.id);
        /// end
        /// ]]></code></pre>
        /// 
        /// # [csharp](#tab/csharp)
        /// 
        /// <pre><code language="csharp"><![CDATA[
        /// // loop through available characters on this account, printing their name and id
        /// foreach (var charIdentity in GameState.CharacterSet) {
        ///    Console.WriteLine("Found available character: {charIdentity.name} {charIdentity.id}");
        /// }
        /// ]]></code></pre>
        /// 
        /// ***
        /// </example>
        public IList<CharacterIdentity> Characters => _manager.GameState.Characters;

        /// <summary>
        /// The datetime (UTC) of when the last tick event was fired.
        /// </summary>
        public DateTime LastTick { get; private set; } = DateTime.MinValue;

        /// <summary>
        /// Maximum number of characters allowed to be created per account on this server.
        /// </summary>
        public uint MaxAllowedCharacters => _manager.GameState.MaxAllowedCharacters;

        /// <summary>
        /// The name of the server this client is connected to
        /// </summary>
        public string ServerName => _manager.GameState.ServerName;

        /// <summary>
        /// The current state of the client. Use this to check if you are logged in, at character select screen, etc.
        /// </summary>
        public ClientState State => _manager.GameState.State;

        /// <summary>
        /// Information about the world around you. Only valid while logged in, specifically when
        /// `game.State == ClientState.Entering_Game`
        /// </summary>
        public World World {
            get {
                //_manager.Resolve<ILogger>()?.Log($"{ScriptName}:GET World IS {(world == null ? "null" : world)}");
                return world;
            }
            private set {
                world = value;

                //_manager.Resolve<ILogger>()?.Log($"{ScriptName}:SET World IS {(world == null ? "null" : world)}");
            }
        }

        /// <summary>
        /// The name of the script this game instance is tied to.
        /// </summary>
        private string ScriptName { get; set; }

        /// <summary>
        /// The capabilities of this game client
        /// </summary>
        public Capabilities Capabilities { get; private set; } = new Capabilities();
        #endregion // public properties

        /// <summary>
        /// Create a new game instance for a script. Scripts should not use this, they should use the `game` global to
        /// access the current game object for that script.
        /// </summary>
        [WattleScript.Interpreter.WattleScriptHidden]
        public Game() {
            _manager = ScriptManager.Instance;
            Actions = _manager.Resolve<ClientActions>();

            // we create a new message handler for each script, which is a little inefficient because it means double
            // parsing... but it ensures we can clean up events when a script is ended.
            // todo: fix this so we are only parsing messages in one place
            Messages = _manager.Resolve<MessageHandler>();

            _manager.OnIncomingData += Manager_OnIncomingData;
            _manager.OnOutgoingData += Manager_OnOutgoingData;
            _manager.GameState.OnTick += GameState_OnTick;
            _manager.GameState.OnRender2D += GameState_OnRender2D;
            _manager.GameState.OnRender3D += GameState_OnRender3D;
            _manager.GameState.OnStateChanged += GameState_OnStateChanged;
            _manager.GameState.OnWindowMessage += GameState_OnWindowMessage;

            switch (_manager.GameState.State) {
                case ClientState.Entering_Game:
                case ClientState.PlayerDesc_Received:
                case ClientState.In_Game:
                case ClientState.Logging_Out:
                    World = new World(_manager);
                    Character = new Character(_manager);
                    break;
                default:
                    break;
            }
        }

        #region event handlers
        private void GameState_OnWindowMessage(object sender, WindowMessageEventArgs e) {
            OnWindowMessage?.InvokeSafely(this, e);
        }

        private void Manager_OnOutgoingData(object sender, MessageBytesEventArgs e) {
            try {
                Messages.HandleOutgoingMessageData(e.Data);
            }
            catch (Exception ex) {
                _manager.Resolve<ILogger>().LogError(ex.ToString());
            }
        }

        private void Manager_OnIncomingData(object sender, MessageBytesEventArgs e) {
            try {
                Messages.HandleIncomingMessageData(e.Data);
            }
            catch (Exception ex) {
                _manager.Resolve<ILogger>().LogError(ex.ToString());
            }
        }

        private void GameState_OnStateChanged(object sender, StateChangedEventArgs e) {
            switch (e.NewState) {
                case ClientState.Entering_Game:
                    World = new World(_manager);
                    Character = new Character(_manager);
                    break;
                case ClientState.Logging_Out:
                    Character?.Dispose();
                    World?.Dispose();
                    Character = null;
                    World = null;
                    break;
            }

            OnStateChanged?.InvokeSafely(this, e);
        }

        private void GameState_OnTick(object sender, EventArgs e) {
            LastTick = DateTime.UtcNow;
            OnTick?.InvokeSafely(this, e);

            Character?.BusyTracker?.Tick();
        }

        private void GameState_OnRender3D(object sender, EventArgs e) {
            OnRender3D?.InvokeSafely(this, e);
        }

        private void GameState_OnRender2D(object sender, EventArgs e) {
            OnRender2D?.InvokeSafely(this, e);
        }
        #endregion // event handlers

        internal void End() {
            OnScriptEnd?.InvokeSafely(this, EventArgs.Empty);
        }

        internal void Dispose() {
            // force clear all event delegates
            OnScriptEnd = delegate { };
            OnStateChanged = delegate { };
            OnTick = delegate { };

            _manager.OnIncomingData -= Manager_OnIncomingData;
            _manager.OnOutgoingData -= Manager_OnOutgoingData;
            _manager.GameState.OnTick -= GameState_OnTick;
            _manager.GameState.OnRender2D -= GameState_OnRender2D;
            _manager.GameState.OnRender3D -= GameState_OnRender3D;
            _manager.GameState.OnStateChanged -= GameState_OnStateChanged;
            _manager.GameState.OnWindowMessage -= GameState_OnWindowMessage;

            Character?.Dispose();
            Character = null;
            World?.Dispose();
            World = null;

            //Messages = null;
        }
    }
}