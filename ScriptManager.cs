﻿using ACE.DatLoader;
using WattleScript.Interpreter;
using WattleScript.Interpreter.Interop;
using WattleScript.Interpreter.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Common;
using UtilityBelt.Common.Lib;
using UtilityBelt.Common.Messages;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using UtilityBelt.Scripting.ScriptEnvs.Lua;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using static WattleScript.Interpreter.Debugging.DebuggerAction;

// https://marketplace.visualstudio.com/items?itemName=xanathar.moonsharp-debug
namespace UtilityBelt.Scripting {

    public class ScriptManagerOptions {
        public bool StartVSCodeDebugServer { get; set; } = false;
        public string ScriptDirectory { get; set; } = "C:\\Games\\Decal Plugins\\UtilityBelt\\scripts\\";
        public string ScriptDataDirectory { get; set; } = "C:\\Games\\Decal Plugins\\UtilityBelt\\scriptdata\\";

        public ClientCapability ClientCapabilities { get; set; }
    }

    public class ScriptManager : IDisposable {
        private enum MessageDirection {
            IN,
            OUT
        }
        private class PendingMessage {
            public MessageDirection Direction { get; set; }
            public byte[] Message { get; set; }
            public PendingMessage(MessageDirection direction, byte[] rawData) {
                Direction = direction;
                Message = rawData;
            }
        }

        private readonly ConcurrentQueue<PendingMessage> _pendingMessages = new ConcurrentQueue<PendingMessage>();

        private readonly List<Type> _userScriptableTypes = new List<Type>();
        public static ScriptManager Instance { get; private set; }

        private readonly Dictionary<Type, ScriptComponent> _registeredComponents = new Dictionary<Type, ScriptComponent>();

        private Dictionary<string, UBScript> _activeScripts = new Dictionary<string, UBScript>();
        private List<UBScript> _pendingScripts = new List<UBScript>();

        public event EventHandler<EventArgs> OnInitialized;

        public event EventHandler<ScriptEventArgs> OnScriptStarted;
        public event EventHandler<ScriptEventArgs> OnScriptPaused;
        public event EventHandler<ScriptEventArgs> OnScriptUnpaused;
        public event EventHandler<ScriptEventArgs> OnScriptStopped;

        public event EventHandler<MessageBytesEventArgs> OnIncomingData;
        public event EventHandler<MessageBytesEventArgs> OnOutgoingData;

        public event EventHandler<DirectoryAccessRequestEventArgs> OnDirectoryAccessRequest;

        public ScriptManagerOptions Options { get; }

        public UBScript GlobalScriptContext { get; private set; }

        /// <summary>
        /// Reference to the MessageHandler implementation being used.
        /// </summary>
        public MessageHandler MessageHandler { get; private set; }

        /// <summary>
        /// Reference to the ILogger implementation being used.
        /// </summary>
        private ILogger Logger => Resolve<ILogger>();

        private List<Callback> _callbacks = new List<Callback>();
        private bool isLoaded;

        public GameState GameState { get; private set; }
        public bool IsInitialized { get; private set; }

        public ScriptManager(ScriptManagerOptions options) {
            Instance = this;
            Options = options;

            Script.GlobalOptions.RethrowExceptionNested = true;
        }

        private void RegisterConverters() {
            // /ub lexec return game.actions.castSpell(2, 0, nil, function(res) print(tostring(res)) end)
            //Task.Run(() => {

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.Function,
                typeof(Func<bool>),
                val => {
                    return (Func<bool>)delegate () {
                        try {
                            var r = val.Function.Call();
                            if (!r.IsNil()) {
                                return r.CastToBool();
                            }
                        }
                        catch (ScriptRuntimeException ex) {
                            Logger.LogError($"An error occured! {ex.DecoratedMessage}");
                        }
                        catch (Exception ex) { Logger.LogError(ex.ToString()); }
                        return false;
                    };
                }
            );

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.Function,
                typeof(Func<QueueAction>),
                val => {
                    return (Func<QueueAction>)delegate () {
                        try {
                            var r = val.Function.Call();
                            if (r.IsNotNil()) {
                                if (r.UserData.Object is QueueAction action) {
                                    return action;
                                }
                            }
                            return null;
                        }
                        catch (ScriptRuntimeException ex) {
                            Logger.LogError($"An error occured! {ex.DecoratedMessage}");
                        }
                        catch (Exception ex) { Logger.LogError(ex.ToString()); }
                        return null;
                    };
                }
            );

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.Function,
                typeof(Func<object, object>),
                val => {
                    return (Func<object, object>)delegate (object e) {
                        try {
                            return val.Function.Call(e);
                        }
                        catch (ScriptRuntimeException ex) {
                            Logger.LogError($"An error occured! {ex.DecoratedMessage}");
                        }
                        catch (Exception ex) { Logger.LogError(ex.ToString()); }
                        return null;
                    };
                }
            );

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.Function,
                typeof(Func<WorldObject, bool>),
                val => {
                    return (Func<WorldObject, bool>)delegate (WorldObject e) {
                        try {
                            var res = val.Function.Call(e);
                            return res.Boolean;
                        }
                        catch (ScriptRuntimeException ex) {
                            Logger.LogError($"An error occured! {ex.DecoratedMessage}");
                        }
                        catch (Exception ex) { Logger.LogError(ex.ToString()); }
                        return false;
                    };
                }
            );

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.Function,
                typeof(Action<string, string>),
                val => {
                    return delegate (string a, string b) {
                        try {
                            var script = GetScript(val.Function.OwnerScript);
                            if (script != null) {
                                val.Function.CallAsync(a, b);
                            }
                        }
                        catch (ScriptRuntimeException ex) {
                            Instance?.Resolve<ILogger>()?.LogError($"An error occured! {ex.DecoratedMessage}");
                        }
                        catch (Exception ex) { Instance?.Resolve<ILogger>()?.LogError(ex.ToString()); }
                    };
                }
            );

            Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                DataType.UserData,
                typeof(Enum),
                val => (Enum)val.UserData.Object
            );
            //});
        }

        public void RegisterActionResultConversion(Type type) {
            try {
                var actionType = typeof(Action<>).MakeGenericType(type);
                Script.GlobalOptions.CustomConverters.SetScriptToClrCustomConversion(
                    DataType.Function,
                    actionType,
                    val => {
                        return delegate (object e) {
                            try {
                                var script = GetScript(val.Function.OwnerScript);
                                if (script != null) {
                                    val.Function.CallAsync(e);
                                }
                            }
                            catch (ScriptRuntimeException ex) {
                                Instance?.Resolve<ILogger>()?.LogError($"An error occured! {ex.DecoratedMessage}");
                            }
                            catch (Exception ex) { Instance?.Resolve<ILogger>()?.LogError(ex.ToString()); }
                        };
                    }
                );
            }
            catch (Exception ex) {
                Instance?.Resolve<ILogger>()?.LogError(ex, $"Failed to Register Action Result Conversion for {type}");
            }
        }

        public void SetClientCapabilities(ClientCapability capabilities) {
            Capabilities.ClientCapabilities = capabilities;
        }

        /// <summary>
        /// Initialize the script manager. Make sure custom components are registered before calling this.
        /// </summary>
        public async Task Initialize(Action<string> progressReport = null) {
            try {
                MessageHandler = new MessageHandler();

                await Task.Run(() => {
                    if (!Directory.Exists(Options.ScriptDirectory)) {
                        Directory.CreateDirectory(Options.ScriptDirectory);
                    }
                    if (!Directory.Exists(Options.ScriptDataDirectory)) {
                        Directory.CreateDirectory(Options.ScriptDataDirectory);
                    }

                    RegisterConverters();

                    if (!_registeredComponents.ContainsKey(typeof(ILogger)))
                        RegisterComponent(typeof(ILogger), typeof(DefaultLogger), true, new DefaultLogger());

                    // ScriptManagers should register themselves. This allows other components to have access
                    // to the IoC container.
                    RegisterComponent(typeof(ScriptManager), typeof(ScriptManager), true, this);
                    RegisterComponent(typeof(MessageHandler), typeof(MessageHandler));

                    if (!_registeredComponents.ContainsKey(typeof(ClientActions))) {
                        RegisterComponent(typeof(ClientActions), typeof(ClientActions), true, new ClientActions());
                    }

                    GameState = new GameState(MessageHandler, this, Resolve<ILogger>());
                    RegisterComponent(typeof(GameState), typeof(GameState), true, GameState);

                    // default interface implementations
                    if (!_registeredComponents.ContainsKey(typeof(Game)))
                        RegisterComponent(typeof(Game), typeof(Game));
                    if (!_registeredComponents.ContainsKey(typeof(World)))
                        RegisterComponent(typeof(World), typeof(World));
                    if (!_registeredComponents.ContainsKey(typeof(Character)))
                        RegisterComponent(typeof(Character), typeof(Character));
                    if (!_registeredComponents.ContainsKey(typeof(WorldState)))
                        RegisterComponent(typeof(WorldState), typeof(WorldState));
                    if (!_registeredComponents.ContainsKey(typeof(AnimTracker)))
                        RegisterComponent(typeof(AnimTracker), typeof(AnimTracker));
                    if (!_registeredComponents.ContainsKey(typeof(BusyTracker)))
                        RegisterComponent(typeof(BusyTracker), typeof(BusyTracker));
                    if (!_registeredComponents.ContainsKey(typeof(UBScript)))
                        RegisterComponent(typeof(UBScript), typeof(UBScript));
                    if (!_registeredComponents.ContainsKey(typeof(Fellowship)))
                        RegisterComponent(typeof(Fellowship), typeof(Fellowship));
                    if (!_registeredComponents.ContainsKey(typeof(Trade)))
                        RegisterComponent(typeof(Trade), typeof(Trade));
                    if (!_registeredComponents.ContainsKey(typeof(WorldObject)))
                        RegisterComponent(typeof(WorldObject), typeof(WorldObject));
                    if (!_registeredComponents.ContainsKey(typeof(Spell)))
                        RegisterComponent(typeof(Spell), typeof(Spell));
                    if (!_registeredComponents.ContainsKey(typeof(SpellBook)))
                        RegisterComponent(typeof(SpellBook), typeof(SpellBook));
                    if (!_registeredComponents.ContainsKey(typeof(Skill)))
                        RegisterComponent(typeof(Skill), typeof(Skill));
                    if (!_registeredComponents.ContainsKey(typeof(Vital)))
                        RegisterComponent(typeof(Vital), typeof(Vital));
                    if (!_registeredComponents.ContainsKey(typeof(SkillFormula)))
                        RegisterComponent(typeof(SkillFormula), typeof(SkillFormula));
                    if (!_registeredComponents.ContainsKey(typeof(Interop.Attribute)))
                        RegisterComponent(typeof(Interop.Attribute), typeof(Interop.Attribute));
                    if (!_registeredComponents.ContainsKey(typeof(Allegiance)))
                        RegisterComponent(typeof(Allegiance), typeof(Allegiance));
                    if (!_registeredComponents.ContainsKey(typeof(AllegianceMember)))
                        RegisterComponent(typeof(AllegianceMember), typeof(AllegianceMember));
                    //}
                    progressReport?.Invoke($"Registered IoC types");

                    //using (profiler?.Step("Starting vscode debug server")) {
                    if (Options.StartVSCodeDebugServer)
                        StartVSCodeDebugServer();
                    //}


                    progressReport?.Invoke($"Registered script interop types");
                    //using (profiler?.Step("Register script types")) {
                    SetupScriptData();
                    //}
                    isLoaded = true;
                });


                progressReport?.Invoke($"Initialized scripts....");
                //using (profiler?.Step("Start global script context")) {
                StartGlobalScriptContext();
                //}
                LoadPendingScripts();

                progressReport?.Invoke($"LoadPendingMessages ... {_pendingMessages.Count}");
                LoadPendingMessages();
                IsInitialized = true;

                OnInitialized?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex) { progressReport?.Invoke(ex.ToString()); }
        }

        private void LoadPendingMessages() {
            while (_pendingMessages.Count > 0) {
                if (_pendingMessages.TryDequeue(out PendingMessage result)) {
                    if (result.Direction == MessageDirection.IN) {
                        HandleIncoming(result.Message);
                    }
                    else {
                        HandleOutgoing(result.Message);
                    }
                }
            }
        }

        private void LoadPendingScripts() {
            foreach (var script in _pendingScripts) {
                script.Init();
                //vsCodeDebugServer?.AttachToScript(script._MoonScript, script.Name);
                OnScriptStarted?.Invoke(this, new ScriptEventArgs(script));
            }
            _pendingScripts.Clear();
        }

        public void HandlePluginsLoaded() {
            try {
                ScriptableTypes.LoadPluginScriptTypes();
            }
            catch (Exception ex) { Logger?.LogError(ex, "Error loading plugin scriptable types", null); }
        }

        public void HandlePluginsUnloaded() {
            try {
                ScriptableTypes.UnloadPluginScriptTypes();
            }
            catch (Exception ex) { Logger?.LogError(ex, "Error unloading plugin scriptable types", null); }
        }

        public void HandleIncoming(byte[] rawData) {
            try {
                if (!isLoaded) {
                    _pendingMessages.Enqueue(new PendingMessage(MessageDirection.IN, rawData));
                    return;
                }
                MessageHandler.HandleIncomingMessageData(rawData);
                OnIncomingData?.Invoke(this, new MessageBytesEventArgs(rawData));
            }
            catch (Exception ex) { Logger?.LogError(ex.ToString()); }
        }

        public void HandleOutgoing(byte[] rawData) {
            try {
                if (!isLoaded) {
                    _pendingMessages.Enqueue(new PendingMessage(MessageDirection.OUT, rawData));
                    return;
                }
                MessageHandler.HandleOutgoingMessageData(rawData);
                OnOutgoingData?.Invoke(this, new MessageBytesEventArgs(rawData));
            }
            catch (Exception ex) { Logger?.LogError(ex.ToString()); }
        }

        public Callback RegisterActionCallback<T>(Action<T> callback) where T : QueueAction {
            var cb = new Callback(Logger, this);
            cb.CallbackFunc = (e) => callback(e as T);
            _callbacks.Add(cb);
            return cb;
        }

        public Callback RegisterActionCallback(Action callback) {
            var cb = new Callback(Logger, this);
            cb.CallbackFunc = (e) => callback();
            _callbacks.Add(cb);
            return cb;
        }

        public Callback RegisterEventCallback<T>(Action<T> callback) where T : System.EventArgs {
            var cb = new Callback(Logger, this);
            cb.CallbackFunc = (e) => callback(e as T);
            _callbacks.Add(cb);
            return cb;
        }

        private void SetupScriptData() {
            UserData.RegistrationPolicy = InteropRegistrationPolicy.Default;
            UserData.RegisterType(typeof(System.EventArgs));
            UserData.RegisterType(typeof(System.Exception));
            UserData.RegisterType(new LuaDescriptor(typeof(System.DateTime)));
            UserData.RegisterType(new LuaDescriptor(typeof(System.TimeSpan)));
            UserData.RegisterType(new LuaDescriptor(typeof(System.Text.RegularExpressions.Regex)));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.Capture));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.CaptureCollection));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.Group));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.GroupCollection));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.Match));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.MatchCollection));
            UserData.RegisterType(typeof(System.Text.RegularExpressions.RegexMatchTimeoutException));
            UserData.RegisterType(new LuaEnumDescriptor(typeof(System.Text.RegularExpressions.RegexOptions)));
            UserData.RegisterType(typeof(System.Enum));
            UserData.RegisterType(typeof(System.Type));
            UserData.RegisterType(typeof(System.EventHandler));
            UserData.RegisterType(typeof(System.IntPtr));
            UserData.RegisterType(new LuaDescriptor(typeof(System.Numerics.Vector2)));
            UserData.RegisterType(new LuaDescriptor(typeof(System.Numerics.Vector3)));
            UserData.RegisterType(new LuaDescriptor(typeof(System.Numerics.Vector4)));
            UserData.RegisterType(typeof(LuaEventFacade));

            foreach (var module in ScriptableTypes.GetAllModules()) {
                foreach (var type in module.Value.GetRegisteredTypes()) {
                    if (type.IsEnum) {
                        UserData.RegisterType(new LuaEnumDescriptor(type));
                    }
                    else {
                        UserData.RegisterType(new LuaDescriptor(type));
                    }

                    if (typeof(QueueAction).IsAssignableFrom(type)) {
                        RegisterActionResultConversion(type);
                    }
                }
                foreach (var type in module.Value.GetExtensions()) {
                    UserData.RegisterExtensionType(type);
                }
            }
        }

        private void StartGlobalScriptContext() {
            var dataDirectory = Path.Combine(Options.ScriptDataDirectory, "global");
            GlobalScriptContext = new UBScript("Global", "", new ScriptContext(this, "Global", ScriptStartupType.Global, dataDirectory, false), Logger);
            GlobalScriptContext.Context.Script = GlobalScriptContext;
            GlobalScriptContext.Init();
            OnScriptStarted?.Invoke(this, new ScriptEventArgs(GlobalScriptContext));
        }

        private void StartVSCodeDebugServer() {
            //vsCodeDebugServer = new MoonSharpVsCodeDebugServer();
            //vsCodeDebugServer.Logger = Options.LogAction;

            //vsCodeDebugServer.Start();
        }

        #region Public API
        public void Tick() {
            if (!isLoaded)
                return;

            GameState?.Tick();

            var scripts = _activeScripts.Values.ToArray();
            foreach (var script in scripts) {
                script.RunCoroutines();
            }

            var outstandingCallbacks = _callbacks.ToArray();
            foreach (var callback in outstandingCallbacks) {
                if (callback.IsScheduled && DateTime.UtcNow >= callback.InvokeAt) {
                    callback.Invoke(callback.InvokeWith);
                }
                else if (!callback.IsScheduled && DateTime.UtcNow - callback.StartedAt > callback.Timeout) {
                    callback.HandleTimeout?.Invoke();
                }

                if (callback.WasCalled) {
                    _callbacks.Remove(callback);
                    continue;
                }
            }
        }

        public void Render2D() {
            if (!isLoaded)
                return;

            GameState?.Render2D();
        }

        public void Render3D() {
            if (!isLoaded)
                return;

            GameState?.Render3D();
        }

        public IEnumerable<UBScript> GetAll() {
            return _activeScripts.Values.ToList();
        }

        public string GetScriptDirectory(string scriptName) {
            var path = Path.Combine(Options.ScriptDirectory, scriptName);

            ScriptManager.Instance?.Resolve<ILogger>()?.LogDebug($"Path: {path} // {Options.ScriptDirectory} // {scriptName}");

            if (Directory.Exists(path)) {
                return path;
            }

            return null;
        }

        public IEnumerable<string> GetAvailable() {
            var scripts = Directory.GetDirectories(Options.ScriptDirectory, "*", SearchOption.TopDirectoryOnly)
                .Select(d => Path.Combine(d, "index.lua"))
                .Where(d => File.Exists(d))
                .Select(d => Directory.GetParent(d).Name)
                .Where(d => !d.Contains(" "))
                .ToList();

            scripts.Sort();

            return scripts;
        }

        public UBScript GetScript(Script script) {
            foreach (var checkScript in _activeScripts.Values) {
                if (checkScript.WattleScript == script) {
                    return checkScript;
                }
            }
            return null;
        }

        public UBScript GetScript(string name) {
            if (_activeScripts.TryGetValue(name, out UBScript script)) {
                return script;
            }
            return null;
        }

        public void StartScript(string name, bool isOnDisk = true, ScriptStartupType startupType = ScriptStartupType.Manual, bool enableDebugger = false, bool silent = false) {
            if (name.ToLower().Equals("global")) {
                throw new Exception("Can't create a UB script named 'global', that name is reserved for internal use.");
            }

            var existingScript = GetScript(name);
            if (existingScript != null)
                StopScript(name, silent);

            var directory = isOnDisk ? Path.Combine(Options.ScriptDirectory, name) : null;

            var dataDirectory = Path.Combine(Options.ScriptDataDirectory, name);
            var script = new UBScript(name, directory, new ScriptContext(this, name, startupType, dataDirectory, isOnDisk, directory), Logger);
            script.Context.Script = script;

            if (!silent)
                Logger.Log(LogLevel.Information, $"Created script: {name}");

            _activeScripts.Add(name, script);

            if (isLoaded) {
                script.Init();
                if (enableDebugger) {
                    //vsCodeDebugServer?.AttachToScript(script._MoonScript, name);
                }
                OnScriptStarted?.Invoke(this, new ScriptEventArgs(script));
            }
            else {
                if (!_pendingScripts.Contains(script))
                    _pendingScripts.Add(script);
            }

            return;
        }

        public void RestartScript(string name) {
            Logger.Log(LogLevel.Information, $"Restarting script: {name}");
            bool isOnDisk = true;
            ScriptStartupType startupType = ScriptStartupType.Manual;
            if (GetScript(name) != null) {
                startupType = GetScript(name).Context.StartupType;
                isOnDisk = !string.IsNullOrEmpty(GetScript(name).Directory);

                StopScript(name, true);

            }
            StartScript(name, isOnDisk, startupType, false, true);
        }

        public void PauseScript(string name) {
            Logger.Log(LogLevel.Information, $"Pausing script: {name}");
            var script = GetScript(name);
            if (script != null) {
                script.Pause();
                OnScriptPaused?.Invoke(this, new ScriptEventArgs(script));
            }
            else {
                Logger.LogError($"Unable to find script with name: {name}.");
            }
        }

        public void UnPauseScript(string name) {
            Logger.Log(LogLevel.Information, $"Unpausing script: {name}");
            var script = GetScript(name);
            if (script != null) {
                script.Unpause();
                OnScriptUnpaused?.Invoke(this, new ScriptEventArgs(script));
            }
            else {
                Logger.LogError($"Unable to find script with name: {name}.");
            }
        }

        public async void StopScript(string name, bool silent = false) {
            if (!silent)
                Logger.Log(LogLevel.Information, $"Stopping script: {name}");
            var script = GetScript(name);
            if (script != null) {
                script.Context.GetGame()?.End();
                // vsCodeDebugServer?.Detach(script._MoonScript);
                OnScriptStopped?.Invoke(this, new ScriptEventArgs(script));
                _activeScripts.Remove(name);
                script.Dispose();
            }
            else {
                Logger.LogError($"Unable to find script with name: {name}.");
            }
        }
        #endregion // Public API

        #region Dependency Injection
        /// <summary>
        /// Registering a registerType that already exists will overwrite the old implementation. This allows for selectively
        /// overriding different interfaces with a custom implementation where needed.
        /// </summary>
        /// <param name="registerType"></param>
        /// <param name="implementedType"></param>
        /// <param name="isSingleton"></param>
        /// <param name="instance"></param>
        public void RegisterComponent(Type registerType, Type implementedType, bool isSingleton = false, object instance = null) {
            //Logger?.Log($"Register {implementedType} as {registerType} isSingleton:{isSingleton} instance:{instance}", LogLevel.Verbose);
            // remove any existing implementations of registerType
            UnregisterComponent(registerType);

            _registeredComponents.Add(registerType, new ScriptComponent() {
                ImplementedType = implementedType,
                RegisteredType = registerType,
                IsSingleton = isSingleton,
                Instance = instance
            });
        }

        /// <summary>
        /// Unregister an engine component
        /// </summary>
        /// <param name="registerType"></param>
        public void UnregisterComponent(Type registerType) {
            if (_registeredComponents.ContainsKey(registerType))
                _registeredComponents.Remove(registerType);
        }

        /// <summary>
        /// Resolve engine component of type T
        /// </summary>
        /// <typeparam name="T">Type to resolve</typeparam>
        /// <returns></returns>
        public T Resolve<T>() where T : class {
            try {
                if (_registeredComponents.TryGetValue(typeof(T), out ScriptComponent component)) {
                    var instance = (T)GetInstance(component);
                    if (instance != null) {
                        return instance;
                    }
                }

                if (!(typeof(ILogger).IsAssignableFrom(typeof(T)))) {
                    Logger?.LogError((new Exception()).StackTrace);
                    throw new Exception($"Resolve {typeof(T)} WAS NULL");
                }
            }
            catch (Exception ex) {
                Logger?.LogError(ex, $"Error while resolving type {typeof(T)}");
            }

            return null;
        }

        private object GetInstance(ScriptComponent component) {
            if (component.IsSingleton && component.Instance != null)
                return component.Instance;

            return CreateInstance(component);
        }

        private object CreateInstance(ScriptComponent component, IDictionary<object, object> parameterOverloads = null) {
            //if (component.RegisteredType != typeof(ILogger))
            //    Logger?.Log($"CreateInstance: {component.ImplementedType}//{component.RegisteredType}: {component.IsSingleton} // {component.Instance}", LogLevel.Verbose);

            if (component.IsSingleton && component.Instance != null)
                return component.Instance;

            object newInstance;
            object[] args = BuildArgs(component, parameterOverloads);

            if (args == null) {
                newInstance = Activator.CreateInstance(component.ImplementedType, true);
            }
            else {
                if (component.RegisteredType != typeof(ILogger))
                    Logger?.LogDebug($"CreateInstance: {component.ImplementedType}//{component.RegisteredType}: {component.IsSingleton} // {component.Instance} args:{args} // {args.Length}");
                newInstance = Activator.CreateInstance(component.ImplementedType, args);
            }

            if (component.IsSingleton)
                component.Instance = newInstance;

            return newInstance;
        }

        private object[] BuildArgs(ScriptComponent component, IDictionary<object, object> parameterOverloads) {
            var constructors = component.ImplementedType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic).ToList();

            // sort constructors by largest number of params first
            constructors.Sort((a, b) => -a.GetParameters().Count().CompareTo(b.GetParameters().Count()));
            //Logger?.Log($"Constructors ({component.ImplementedType}) ({constructors.Count}): {string.Join(", ", constructors.Select(c => c.Name + "->args" + c.GetParameters().Length.ToString()).ToArray())}", LogLevel.Verbose);

            foreach (var constructor in constructors) {
                if (TrySatisfyConstructorParams(parameterOverloads, constructor, out List<object> constructorArgs)) {
                    return constructorArgs.ToArray();
                }
            }

            return null;
        }

        private bool TrySatisfyConstructorParams(IDictionary<object, object> parameterOverloads, ConstructorInfo constructor, out List<object> constructorArgs) {
            var args = new List<object>();
            bool foundAllParams = true;
            IEnumerable<string> namedParameterKeys = new string[] { };
            IEnumerable<Type> typedParameterKeys = new Type[] { };

            if (parameterOverloads != null) {
                namedParameterKeys = parameterOverloads.Keys.Where(k => k is string).Select(k => k.ToString());
                typedParameterKeys = parameterOverloads.Keys.Where(k => k is Type).Select(k => (Type)k);
            }

            foreach (var param in constructor.GetParameters()) {
                // check for a namedParameter match
                if (namedParameterKeys.Contains(param.Name)) {
                    args.Add(parameterOverloads[param.Name]);
                    continue;
                }
                // check for a typedParameter match
                else if (typedParameterKeys.Contains(param.ParameterType)) {
                    args.Add(parameterOverloads[param.ParameterType]);
                    continue;
                }
                // check out existing component registry
                else if (_registeredComponents.TryGetValue(param.ParameterType, out ScriptComponent paramComponent)) {
                    args.Add(GetInstance(paramComponent));
                    continue;
                }

                Logger?.LogError($"Could not satisfy type: {param.Name} ({param.ParameterType})");

                foundAllParams = false;
                break;
            }

            constructorArgs = foundAllParams ? args : new List<object>();
            return foundAllParams;
        }
        #endregion // Dependency Injection

        public void Dispose() {
            OnScriptStopped?.Invoke(this, new ScriptEventArgs(GlobalScriptContext));
            GlobalScriptContext?.Dispose();
            GlobalScriptContext = null;
            foreach (var script in _activeScripts.Values) {
                OnScriptStopped?.Invoke(this, new ScriptEventArgs(script));
                //vsCodeDebugServer?.Detach(script._MoonScript);
                script.Dispose();
            }
            _activeScripts.Clear();

            //vsCodeDebugServer?.Dispose();
            GameState?.Dispose();
        }

        internal void RequestDirectoryAccess(string scriptName, string storageDirectory, string reason, Action<bool, string> callback) {
            OnDirectoryAccessRequest?.Invoke(this, new DirectoryAccessRequestEventArgs(scriptName, storageDirectory, reason, callback));
        }
    }
}
